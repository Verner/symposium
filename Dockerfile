FROM python:3.6-alpine
LABEL maintainer="Jonathan Verner <jonathan@temno.eu>"
LABEL description="Symposium Web Site (DEV)"
LABEL license="MIT"
LABEL usage=""
LABEL version="0.1"

ENV APP_HOME /symposium
ENV LANG en_US.utf8
ENV PGDATA /var/lib/postgresql/data

COPY Pipfile $APP_HOME/Pipfile
COPY Pipfile.lock $APP_HOME/Pipfile.lock

# pinning pipenv & pip to specific versions
# until a bugfix for https://github.com/pypa/pipenv/issues/2956
# is released
RUN apk update && apk upgrade && \
    apk add --no-cache postgresql postgresql-contrib nginx redis \
            freetype libpng libjpeg-turbo jpeg tiff libwebp lcms2 && \
    apk add --no-cache --virtual build-deps \
            postgresql-dev \
            ca-certificates \
            zlib-dev \
            python3-dev \
            libjpeg-turbo-dev jpeg-dev tiff-dev freetype-dev lcms2-dev libwebp-dev \
		    gcc \
		    musl-dev \
		    make \
		    linux-headers \
		    git && \
    pip install pipenv==2018.7.1 pip==18.0 && \
    cd $APP_HOME && \
    pipenv --python /usr/local/bin/python install --dev && \
    rm -rf /var/cache/apk/* && \
    apk del build-deps && \
    rm -rf /root/.cache

RUN mkdir -p /run/nginx \
             $APP_HOME \
             $APP_HOME/logs \
             $APP_HOME/code

COPY docker/nginx.conf /etc/nginx/conf.d/default.conf
COPY docker/ $APP_HOME/

RUN sed -i /etc/nginx/conf.d/default.conf -e "s|%APP_HOME%|$APP_HOME|g" && \
    $APP_HOME/initdb.sh

VOLUME $APP_HOME/code

EXPOSE 80
EXPOSE 8080

# Debugger
EXPOSE 1984

# Live Reload
EXPOSE 35729

ENTRYPOINT $APP_HOME/run.sh
