General
========

For general guidelines see `Contributing to Open Source Projects <http://www.contribution-guide.org/>`_.

Setup Devel Environment
=========================

.. code-block:: shell-session

    # Clone the repo
    $ git clone https://gitlab.com/Verner/symposium

    ...

    # Change to the project dir
    $ cd symposium

    # Create a virtualenv & install dev dependencies
    $ pipenv --three && pipenv install --dev


Coding Style
==============

The code mostly follows `PEP 8 <https://www.python.org/dev/peps/pep-0008/>`_ (see :source:`pylintrc` for details).
Please make sure your code follows the guidelines by running

.. code-block:: shell-session

    $ ./manage.py test lint

Where possible, especially in public facing interface, use PEP 484-style type annotations to specify argument types & return
types.

Documentation
===============

All modules should have a docstring as should all user-facing classes and methods. Docstrings should be formatted
according to the `Google Style Guide <http://google.github.io/styleguide/pyguide.html#Comments>`_
(see also `Khan Academy Style Guide <https://github.com/Khan/style-guides/blob/master/style/python.md#docstrings>`_).
The Napoleon Sphinx extension has some `examples <http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html>`_.
These docstrings will be automatically extracted by Sphinx and included in the project documentation.

To build documentation run

.. code-block:: shell-session

    $ ./manage.py doc build

To see how the resulting documentation will look like do

.. code-block:: shell-session

    $ ./manage.py doc view

Other documentation (tutorials, ...) are located in the ``doc/en`` subdirectory as ``.rst`` files formatted using
`reStructuredText <http://docutils.sourceforge.net/rst.html>`_ (see `Sphinx ReSt primer <https://www.sphinx-doc.org/en/stable/rest.html>`_).
When writing a new file which should be included in the docs, add it to the ``contents.rst`` file.


Testing
=========

  - tests go into the ``tests/symposium`` subdirectory and should have a name starting with ``test_``
  - see files in the directory for examples of tests
  - to run all tests do

  .. code-block:: shell-session

        $ ./manage.py test run

  - to run a specific test do

  .. code-block:: shell-session

        $ ./manage.py test run name

  where ``name`` is the name of the test (e.g. if ``name`` is ``example`` the tests would be found in the file ``tests/symposium/test_example.py``)

  - to perform a static typecheck do

  .. code-block:: shell-session

        $ ./manage.py test check


