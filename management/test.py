import os
import pathlib
from plumbum import local, ProcessExecutionError
import sys
from webbrowser import open_new_tab

from .utils import M

test_runner = local['pytest']
linter = local['pylint']
type_checker = local['mypy']
safety = local['safety']


@M.command()
def safety():
    safety()

@M.command()
def run(tests=None, cov=None):
    with local.env(PYTHONPATH='./site:./'):
        with local.cwd('./site'):
            if tests is None:
                tests = ''
                args = [tests]
            else:
                tests = tests+'.py'
                args = [tests,'--fulltrace', '--pdb']
                if cov is not None:
                    args.append('--cov=./')
            args.extend(['--nomigrations', '--create-db'])
            try:
                test_runner.run(args, stdout=sys.stdout, stderr=sys.stderr)
            except:
                pass

@M.command()
def lint(report=False):
    with local.env(PYTHONPATH='./site'):
        p = pathlib.Path('./site')
        modules = [x for x in p.iterdir() if x.is_dir() and (x/'__init__.py').is_file() and not x.name == '..']
        try:
            if not report:
                linter("--reports=n", *modules, stdout=sys.stdout, stderr=sys.stderr)
            else:
                linter(*modules, stdout=sys.stdout, stderr=sys.stderr)
        except ProcessExecutionError:
            exit(1)


@M.command()
def check():
    with local.env(MYPYPATH='../'):
        with local.cwd('./site'):
            try:
                type_checker("--ignore-missing-imports", "-p", "symposium", stdout=sys.stdout, stderr=sys.stderr)
            except ProcessExecutionError:
                exit(1)


@M.command()
def ci():
    run(cov=True)
