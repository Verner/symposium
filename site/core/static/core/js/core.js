global.fs = {
    'name': '/',
    'children':[
        {'name':'dirA','children':[ {'name': 'fileA', children:[]}, {'name': 'fileB', children:[]}] },
        {'name':'dirB','children':[ {'name': 'fileC', children:[]}, {'name': 'fileD', children:[]}] },
        {'name':'fileE','children':[]},
        {'name':'fileF','children':[]},
    ]
}

import { Widget } from "./lib/widget"
import { evModelChanged, TreeModel, Model } from "./lib/models.js"
import { ModelView, GridView, ListView } from "./lib/views"
import { update } from "./lib/utils"

export class FSItemView extends ModelView {
    constructor(item) {
        super(item)

        this._icon_elt = document.createElement('span')
        this._title_elt = document.createElement('span')
        this._icon_elt.classList.add('item-icon')
        this._title_elt.classList.add('item-title')

        this.elt.appendChild(this._icon_elt)
        this.elt.appendChild(this._title_elt)

        this.registerEvent('activated', {bubbles:true})
        this.sub(this, 'click', () => this.pub('activated', this.model))

        this._update()

    }

    _update() {

        var self = this

        if ( this.isDir() ) {
            this.addCssClass('directory')
            this.removeCssClass('file')
        } else {
            this.addCssClass('file')
            this.removeCssClass('directory')
        }

        if (this.model.data.preview) {
            this._icon_elt.style.backgroundImage = item.data.preview
            this._icon_elt.classList.add('preview')
        } else {
            this._icon_elt.classList.remove('preview')
            this._icon_elt.style.backgroundImage = null;
        }

        this._title_elt.innerText = this.model.id

    }

    isDir() { return this.model.children.length > 0 }
}

export class FavItemView extends ModelView {
    constructor(item) {
        super(item, {tag:"li"})
        this.registerEvent('activated', {bubbles:true})
        this._update()
    }
    _update() {
        this.elt.innerText = this.model.data.title
        if (this.model.data.target) {
            this.addCssClass('w-link')
            this.sub(this, 'click', (evt) => this.pub('activated', this.model.data))
        } else {
            this.unsub(this)
            this.removeCssClass('w-link')
        }
    }
}


export class BreadCrumbItemView extends ModelView {
    constructor(item) {
        super(item, {tag:"a", cssClasses:["breadcrumb"]})
        this.registerEvent('activated', {bubbles:true})
        this._update()
    }
    _update() {
//         this.elt.innerText = this.model.data
        this.elt.innerHTML = '<span class="w-separator"></span>'+this.model.data
        this.addCssClass('w-link')
        this.sub(this, 'click', (evt) => this.pub('activated', this._path()))
    }
    _path() {
        let pos = this.parent.indexOfChild(this)
        return this.parent.model.data.slice(0,pos+1).map((md) => md.data)
    }
}


// var dlg = new Layout('horizontal', [favs, Layout('vertical', controls, breadcrumbs, grid)])
//
//
//


export class LayoutElt extends Widget {
    constructor(direction, widgets, options) {
        let opts = {
            cssClasses: ['w-layout'],
            style: {
                display: 'flex',
                flexDirection: direction
            }
        }
        if (options) {
            if (options.cssClasses) options.cssClasses = opts.cssClasses.concat(options.cssClasses)
            update(opts, options)
        }
        console.log("OPTIONS", options)
        console.log("OPTS:", opts)

        super(opts)

        this._direction = direction

        for(let ch of widgets) {
            this.appendChild(ch)
        }
    }

    get direction() {return this._direction}
    set direction(dir) {this._direction=dir; this.elt.style.flexDirection=this._direction}
}

export class Row extends LayoutElt {
    constructor(widgets, options) {super('row', widgets, options)}
}

export class Col extends LayoutElt {
    constructor(widgets, options) {super('col', widgets, options)}
}

export class Button extends Widget {
    constructor(clickHandler) {
        super({tag:'button'})
        this.addEventListener('click', clickHandler)
    }
}

export class Dialog extends Widget {
    constructor() {
        super()

        this.setStyle({
            position:'absolute',
            top:'50%',
            margin:'auto'
        })
        this.addCssClass('w-dialog')
        document.addEventListener('keyup', this.onKeyUp)

    }

    onKeyUp(evt) {
        switch( evt.keyCode) {
            case 27: this.reject('cancel'); break;
        }
    }

    resolve(val) {
        this.hide()
        this._resolve(val)
        this._resolve = undefined
        this._reject = undefined
    }

    reject(val) {
        this.hide()
        this._reject(val)
        this._reject = undefined
        this._resolve = undefined
    }

    exec() {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.show();
            self._resolve = resolve;
            self._reject = reject;
        })
    }

    show() {this.setStyle({display:''})}
    hide() {this.setStyle({display:'none'})}
}

export class FileDialog extends Dialog {
    constructor(favs, fsModel) {
        super()
        var self = this;
        this._history = {}
        this._favs = favs


        var upBtn = new Button(),
            uploadBtn = new Button(),
            createDirBtn = new Button()

        this.favs = new ListView(favs, FavItem, {cssClasses:['favs']});
        this.content = new GridView(fsModel, FSItemView, {cssClasses:['content']});
        this.toolbar = new Row([upBtn, uploadBtn, createDirBtn], {cssClasses:['toolbar']})
        this.layout = new Row([this.favs, new Col([
                                                    this.toolbar,
                                                    this.locationbar,
                                                    this.content
        ])])

        upBtn.addEventListener('click', ()=>{self.content.navigateUp()})
        this.content.addEventListener('itemSelect', function(ev){self.resolve(ev.data)})
    }
}


export class FileBrowser extends LayoutElt {
    constructor(favModel, fsModel) {
        super('row', [], {cssClasses:['w-file-browser']})
        this._history = []
        this._pathModel = new Model(fsModel.path.map(id => new Model(id)))

        this._breadCrumb = new ListView(this._pathModel, BreadCrumbItemView, {cssClasses:['w-breadcrumb']})
        this._favView = new ListView(favModel, FavItemView, {cssClasses:['w-favs'], style:{flexGrow:1}})
        this._content = new GridView(fsModel, FSItemView, {cssClasses:['w-content']})

        this._favView.sub(this, 'activated', (ev) => this._content.navigateTo(fsModel.subTreeAt(ev.data.target)))
        this._content.sub(this, 'navigate', (ev) => this._history.push(ev.data.path))
        this._content.sub(this, 'navigate', (ev) => this._pathModel.data = ev.data.path.map(id => new Model(id)) || this._pathModel.notifyChange())
        this._breadCrumb.sub(this, 'activated', (ev) => this._content.navigateTo(fsModel.subTreeAt(ev.data)))
        this.appendChildren([this._favView, new LayoutElt('row',[this._breadCrumb, this._content], {style:{flexGrow:5, flexWrap:"wrap"}})])
    }

    back() {
        this._content.navigateTo(fsModel.subTreeAt(this._history.pop()))
    }
}


var test_elt = document.getElementById('test')


global.fsModel = TreeModel.fromObj(fs, 'name', 'children')
global.favModel = new Model([new Model({title:'Directory A', target:['/', 'dirA']})])
global.fb = new FileBrowser(global.favModel, global.fsModel);
global.fg = new GridView(global.fsModel, FSItemView)
global.fg.addCssClass('w-file-browser')
// global.fg.renderInto(test_elt)
// global.TreeModel = TreeModel
global.fb.renderInto(test_elt)




