import { Publisher } from "./event"

export const evModelChanged = 'modelChanged';
export const evChildAppended = 'childAppended';
export const evChildReplaced = 'childReplaced';


export class Model extends Publisher {

    static fromObj(obj) {
        let ret;
        if (obj instanceof Array) {
            ret = new Model([])
            for(const item of obj) {
                ret.data.push(Model.fromObj(item))
            }
        } else if (obj instanceof Object) {
            ret = new Model({})
            for(const [key, val] in Object.entries(obj)) {
                ret.data[key] = Model.fromObj(val)
            }
        } else ret = new Model(obj)
        return ret
    }

    constructor(data = {}) {
        super()
        this.data = data
    }

    get data() {return this._data}
    set data(val) {
        if (this._data instanceof Publisher) this._data.unsub(this)
        this._data = val
        if (this._data instanceof Publisher) this._data.sub(this, 'change', () => this.pub(evModelChanged, {'new': this}))
        this.notifyChange()
    }

    notifyChange(){this.pub(evModelChanged, {'new': this})}

}

export class TreeModel extends Model {
    static fromObj(obj, id_key='id', children_key='children') {
        let data = {}
        for(let [key, val] in Object.entries(obj)) if (key !== children_key) data[key] = val

        let root = new TreeModel(obj[id_key], data)
        for(let ch of obj[children_key]) root.appendChild(TreeModel.fromObj(ch, id_key, children_key))

        return root
    }

    constructor(id, data = {}) {
        super(data)
        this._id = id
        this._children = []
        this._childrenById = {}
    }

    get id() {return this._id}
    get parent() {return this._parent}
    get root() {
        let ret = this;
        while (ret.parent) ret = ret.parent;
        return ret
    }

    get children() {return this._children}

    childById(id){return this._childrenById[id]}

    appendChild(ch) {
        this._childrenById[ch.id] = {ch:ch, index:this._children.length}
        this._children.push(ch)
        ch._parent = this
        this.pub(evChildAppended, {'new': ch})
        this.pub(evModelChanged, {'new': this})
    }

    replaceChild(old_child, new_child) {
        let old = this._childrenById[old_child]
        this._childrenById[new_child.id] = {ch:new_child, index:old.index}
        this._children[old.index] = new_child
        delete this._childrenById[old_child.id]
        this.pub(evChildReplaced, {'new': new_child, 'old': old_child, 'index': old.index})
        this.pub(evModelChanged, {'new': this})
    }

    subTreeAt(path) {
        let subtree = this
        for(let id of path.slice(1)) subtree = subtree._childrenById[id].ch
        return subtree
    }

    get path() {
        let ret = [this.id], node = this
        while(node.parent) {
            node = node.parent
            ret.unshift(node.id)
        }
        return ret
    }

}
