export class Publisher extends EventTarget {
    constructor() {
        super()
        this._subscribers = {}
        this._event_types = {}
        this._pub_proxy = this
    }

    _reAddListeners() {
        for(const [sub, subscriptions] of Object.entries(this._subscribers)) {
            for(const [ev_type, handlers] of Object.entries(subscriptions)) {
                for(const h of handlers)
                    this._pub_proxy.addEventListener(ev_type, h)
            }
        }
    }

    registerEvent(event_type, opts) {this._event_types[event_type] = opts}

    setPubProxy(src){this._pub_proxy = src}

    pub(event_type, data) {
        let opts = this._event_types[event_type]
        let ev = new Event(event_type, opts);
        ev.data = data
        this._pub_proxy.dispatchEvent(ev)
    }

    sub(owner, event_type, handler) {
        if (this._subscribers[owner] === undefined) this._subscribers[owner] = {}
        let subs = this._subscribers[owner]
        if (subs[event_type] === undefined) subs[event_type] = [];
        subs[event_type].push(handler)
        this._pub_proxy.addEventListener(event_type, handler)
    }

    unsub(owner, event_type='*', handler='*') {
        let subs = this._subscribers[owner]

        if (event_type === '*') {
            for(let ev in subs) this.unsub(owner, ev, handler)
        } else if ( handler === '*' ) {
            for(let h of subs[event_type]) this._pub_proxy.removeEventListener(event_type, h)
            subs[event_type] = []
        } else {
            this._pub_proxy.removeEventListener(event_type, handler)
            let index = subs[event_type].indexOf(handler)
            delete subs[event_type][index]
        }

    }
}


