import { update } from "./utils"
import { Widget } from "./widget"
import { evModelChanged } from "./models"

export class ModelView extends Widget {
    constructor(model, options) {
        super(options)
        this.setModel(model, false)
    }

    _update() {}

    get model() {return this._model}

    setModel(model, update=true){
        if (this._model) this._model.unsub(this)
        this._model = model
        this._model.sub(this, evModelChanged, () => this._update())
        if (update) this._update()
    }
}

export class ListView extends ModelView {
    constructor(model, ItemViewClass, options) {
        let opts = {
            tag: "ul",
            cssClasses: ["w-listview"]
        }
        if (options) {
            if (options.cssClasses) options.cssClasses = opts.cssClasses.concat(options.cssClasses)
            update(opts, options)
        }

        super(model, opts)

        this.ItemViewClass = ItemViewClass
        this._update()
    }

    _update() {
        this.clearChildren()
        for(let ch of this.model.data) {
            let item = new this.ItemViewClass(ch)
            this.appendChild(item)
        }
    }
}

export class GridView extends ModelView {
    constructor(model, ItemViewClass, options) {
        let opts = {
            tag: "ul",
            cssClasses: ["w-listview"],
            style: {
                flexWrap:"wrap"
            }
        }
        if (options) {
            if (options.cssClasses) options.cssClasses = opts.cssClasses.concat(options.cssClasses)
            update(opts, options)
        }

        super(model, options)
        this.ItemViewClass = ItemViewClass
        this._update()
        this.sub(this, 'activated', (ev) => this.navigateTo(ev.data))
    }

    _update() {
        this.clearChildren()
        for(let ch of this.model.children) {
            let item = new this.ItemViewClass(ch)
            this.appendChild(item)
        }
    }

    navigateUp() {
        if (this.model.parent) {
            this.pub('navigate', this.model.parent)
            this.setModel(this.model.parent)
        }
    }

    navigateToRoot() {
        this.pub('navigate', this.model.root)
        this.setModel(this.model.root)
    }

    navigateTo(ch) {
        this.pub('navigate', ch)
        this.setModel(ch)
        this._update()
    }

}
