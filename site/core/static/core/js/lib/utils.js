export function clearChildren(elt) {
    while (elt.firstChild) elt.firstChild.remove();
}

export function update(objWhat, objWith) {
    if (objWith instanceof Object)  for(const [k, v] of Object.entries(objWith)) {
        if (objWhat[k] && objWhat[k] instanceof Object && v instanceof Object) update(objWhat[k], v)
        else objWhat[k] = v
    }
}
