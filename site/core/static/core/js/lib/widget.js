import { update, clearChildren } from "./utils"
import { Publisher } from "./event"

export class Widget extends Publisher {
    constructor(options = {}) {
        let opts = {
            tag: "div",
            cssClasses: [],
            style: {}
        }
        if (options) {
            if (options.cssClasses) options.cssClasses = opts.cssClasses.concat(options.cssClasses)
            update(opts, options)

        }


        super()

        this._options = opts

        this.elt = this._options.elt ? this._options.elt : document.createElement(this._options.tag)
        for(const cls of this._options.cssClasses) this.addCssClass(cls)
        this.setStyle(this._options.style)
        this.setPubProxy(this.elt)

        this.child_widgets = []

    }

    renderInto(elt) {
        clearChildren(elt)
        this.appendTo(elt)
    }

    appendTo(elt) {
        elt.appendChild(this.elt)
    }

    addCssClass(cls) {
        this.elt.classList.add(cls)
    }
    removeCssClass(cls) {
        this.elt.classList.remove(cls)
    }

    setStyle(style) {
        for(const [prop, val] of Object.entries(style)) {
            this.elt.style[prop] = val
        }
    }

    indexOfChild(w) {
        for(let i=0; i< this.child_widgets.length; i++) if (w === this.child_widgets[i]) return i
        return -1
    }

    appendChild(w) {
        this.elt.appendChild(w.elt)
        this.child_widgets.push(w)
        w.parent = this
    }

    appendChildren(widgets) {
        for(const w of widgets) {
            this.elt.appendChild(w.elt)
            this.child_widgets.push(w)
            w.parent = this
        }
    }

    clearChildren() {
        clearChildren(this.elt)
        this.child_widgets = []
    }

}
