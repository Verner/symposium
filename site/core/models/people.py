from countries_plus.models import Country
from django.db import models
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField


class Institution(models.Model):
    name = models.CharField(max_length=500, blank=True, null=True)
    freeform_address = models.CharField(max_length=500, blank=True, null=True)
    street = models.CharField(max_length=200, blank=True, null=True)
    street_number = models.CharField(max_length=30, blank=True, null=True)
    city = models.CharField(max_length=200, blank=True, null=True)
    post_code = models.CharField(max_length=20, blank=True, null=True)
    state = models.CharField(max_length=200, blank=True, null=True)
    country = models.ForeignKey(Country, blank=True, null=True, on_delete=models.SET_NULL)
    meta_data = JSONField(default=dict)

    def __str__(self):
        return self.name+"("+self.city+", "+self.state+" "+self.country+")"


class Profile(models.Model):
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other'),
        ('0', 'Prefer not to say')
    )
    user = models.OneToOneField(User, blank=True, null=True, on_delete=models.CASCADE)
    avatar = models.ImageField(blank=True, null=True)
    affiliation = models.ForeignKey(Institution, blank=True, null=True, on_delete=models.SET_NULL)
    web = models.URLField(blank=True, null=True)
    gender = models.CharField(choices=GENDER_CHOICES, max_length=2, blank=True, null=True)
    meta_data = JSONField(default=dict)

    def __str__(self):
        return self.user.get_full_name()+" <"+self.user.email+">"
