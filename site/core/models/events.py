from django.db import models

from .people import Profile


class ConferenceSeries(models.Model):
    name = models.CharField(max_length=500)
    about = models.TextField(default='', blank=True, null=True)
    slug = models.SlugField()

    def __str__(self):
        return self.name


class ConferenceRun(models.Model):
    start = models.DateField()
    end = models.DateField()
    series = models.ForeignKey(ConferenceSeries, blank=True, null=True, on_delete=models.CASCADE)
    slug = models.SlugField()
    run = models.CharField(max_length=500, blank=True, null=True)
    organizers = models.ManyToManyField(Profile)
    about = models.TextField(default='', blank=True, null=True)

    def __str__(self):
        return self.run+" ("+str(self.start)+"-"+str(self.end)+")"



