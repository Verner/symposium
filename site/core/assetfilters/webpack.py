from assets.filters import register_filter
from assets.filters.utils import pipe
from assets.templatetags.asset import asset_url

import re

ASSET_PATTERN = re.compile(b'asset\(([^\)]*)\)')


@register_filter('replace_urls')
def replace_urls_filter(stream, cwd, config):
    """Replace urls of the form asset(bundle_id) with the correct static urls provided
       by the asset package."""
    def repl(m):
        return bytes(asset_url(str(m.group(1), encoding='utf-8')), encoding='utf-8')
    return ASSET_PATTERN.sub(repl, stream)


@register_filter('postcss')
def postcss_filter(stream, cwd, config):
    """
        Post css to automatically put prefixes
    """
    return pipe('postcss --use autoprefixer', stream, cwd=cwd)


import json
import os
import re
import subprocess
import sys
from tempfile import NamedTemporaryFile, gettempdir
from pathlib import Path



@register_filter('webpack')
def webpack_filter(input_bytes, cwd, config={}):
    """
    A filter for bundling javascript with webpack using the babel loader. Provide
    babel options under the "config.babel" key of a given item in manifest.json
    """

    # Create temporary files
    #    temp_in contains the source to transform
    #    temp_out will contain the transformed source
    temp_in = NamedTemporaryFile(suffix='.js', dir=cwd)
    temp_out = NamedTemporaryFile(suffix='.js')
    temp_cfg = NamedTemporaryFile(suffix='.js', dir=cwd)

    # Construct the configuration
    babel_options = {}
    if hasattr(webpack_filter, "_config"):
        babel_options.update(webpack_filter._config)
    babel_options.update(config.get('babel', {}))

    webpack_config = {
        "entry": temp_in.name,
        "output": {
            "path": gettempdir(),
            "filename": Path(temp_out.name).name
        },
        "module": {
            "rules": [{
                "test": "/\.js$/",
                "exclude": "/(node_modules|bower_components)/",
                "use": {
                    "loader": "babel-loader",
                    "options": babel_options,
                }
            }]
        }
    }
    webpack_config.update(config.get('webpack'))

    # Write the source & config to temporary files
    temp_in.write(input_bytes)
    temp_in.flush()
    temp_cfg.write(bytes("const config="+json.dumps(webpack_config, indent=4)+";\nmodule.exports=config;", encoding='utf-8'))
    temp_cfg.flush()

    # Run webpack
    try:
        p = subprocess.run(["webpack", "--config", temp_cfg.name], stdout=sys.stdout, stderr=sys.stderr, cwd=cwd, check=True)
    except:
        print("Error running webpack")
        print("Config:", Path(temp_cfg.name).read_text())
        raise

    # Read the output
    temp_out.seek(0)
    ret = temp_out.read()

    # Cleanup
    temp_out.close()
    temp_in.close()
    temp_cfg.close()

    return ret



