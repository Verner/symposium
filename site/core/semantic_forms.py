import json

from django.apps import apps
from django.db import models
from django.template.loader import get_template
from django.utils.safestring import mark_safe


from .utils.objdict import ObjDict


class SemanticWidget:
    DEFAULT_TEMPLATE = 'input'
    TEMPLATE_PATH = 'core/semantic_forms/fields/'
    INPUT_TYPE = 'text'

    def __init__(self, field, display_spec=None):
        self.spec = ObjDict(display_spec)
        self.field = field
        self.input_type = self.INPUT_TYPE
        self._template_base = self.TEMPLATE_PATH+self.DEFAULT_TEMPLATE

    def __getattr__(self, attr):
        if attr.startswith('_') or hasattr(self, attr):
            return super().__getattribute__(attr)
        return self.spec.get(attr)

    def render(self, format='html'):
        tpl = get_template(self._template_base+'.'+format)
        return mark_safe(tpl.render({'widget': self, 'field': self.field}))


class EmailWidget(SemanticWidget):
    INPUT_TYPE = 'email'


class PasswordWidget(SemanticWidget):
    INPUT_TYPE = 'password'


class SelectWidget(SemanticWidget):
    DEFAULT_TEMPLATE = 'select'


class ValidationError:
    TITLE = "Error"
    DETAIL = "The entered data is invalid."

    def __init__(self, fld, title=None, detail=None):
        self.field = fld
        self.title = title or self.TITLE.format()
        self.detail = detail or self.DETAIL.format()

    def render(self, format='html'):
        if format == 'html':
            return '<div class="error">\n  <h4>'+self.title+'</h4>\n  <p>'+self.detail+'</p>'
        return json.dumps({'title': self.title, 'detail': self.detail})

    def __str__(self):
        return self.render(format='html')


class RequiredError(ValidationError):
    TITLE = 'Value Required'
    DETAIL = 'This field is mandatory'


class InvalidChoiceError(ValidationError):
    TITLE = 'Invalid Choice'
    DETAIL = 'The selected choice is not valid.'


class FormError(BaseException):
    pass


class SemanticField:
    DEFAULT_WIDGET = SemanticWidget
    ALL_FIELDS = {}

    @classmethod
    def register_field(cls, name):
        def decorator(klass):
            cls.ALL_FIELDS[name] = klass
            return klass
        return decorator

    @classmethod
    def factory_create(cls, form, field_id, name, spec, **kwargs):
        if spec.type and ':' in spec.type:
            field_type, args = spec.type.split(':')
            for arg in args.split(','):
                arg_name, arg_val = arg.split('=')
                kwargs[arg_name] = arg_val
        else:
            field_type = spec.type or 'char'

        field_cls = cls.ALL_FIELDS.get(field_type, cls)
        return field_cls(form, field_id, name, spec, **kwargs)

    def __init__(self, form, field_id, name, spec, initial=None, widget=None):
        self._spec = ObjDict(spec)
        self._widget_cls = widget or self.DEFAULT_WIDGET
        self._initial = ObjDict(initial)
        self._value = self._initial.get(self._spec.default)
        self._bound_value = None
        self._clean_value = None
        self._bound = False
        self.form = form
        self.id = self.form.id+':'+str(field_id)
        self.title = self._spec.title
        self.name = name
        self.errors = None
        self.valid = None

    @property
    def value(self):
        return self._clean_value or self._bound_value or self._value

    def bind(self, data):
        self._bound_value = data.get(self.name, None)
        self._bound = True
        self.valid = None
        self.errors = None

    def validate(self):
        if self.valid is not None:
            return self.errors
        if not self._bound:
            raise FormError("Cannot validate unbound field.")
        self.errors = []
        if self._spec.required and self._bound_value is None:
            self.errors.append(RequiredError(self))
        self.valid = not self.errors
        return self.errors

    def clean(self):
        if not self.valid:
            raise FormError("Cannot clean invalid field.")
        self._clean_value = self._bound_value

    def render(self, format='html'):
        widget = self._widget_cls(self, display_spec=self._spec.display)
        return widget.render(format=format)

    def __str__(self):
        return self.render()


@SemanticField.register_field('choice')
class ChoiceField(SemanticField):
    DEFAULT_WIDGET = SelectWidget

    def _load_choices(self, choices):
        for ch in choices:
            if isinstance(ch, str):
                val = ch
                label = ch
            elif isinstance(ch, tuple):
                val, label = ch
            elif isinstance(models.Model):
                val, label = ch.id, str(ch)
            self.choices.append({'value': val, 'label': label})
            self._allowed_values.add(val)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.choices = []
        self._allowed_values = set()
        if self._spec.choices:
            self._load_choices(self._spec.choices)

    def validate(self):
        if self.valid is not None:
            return self.errors
        super().validate()
        if self.value not in self._allowed_values:
            self.errors.append(InvalidChoiceError(self))
        self.valid = not self.errors
        return self.errors


@SemanticField.register_field('reference')
class ReferenceField(ChoiceField):
    def __init__(self, *args, model, **kwargs):
        super().__init__(*args, **kwargs)
        self._model = apps.get_model('app_name', model)
        self._load_choices(self._model.objects.all())

    def clean(self):
        if not self.valid:
            raise FormError("Cannot clean invalid field.")
        self._clean_value = self._model.objects.get(id=self._bound_value)


class DynamicForm:
    DEFAULT_TEMPLATE_BASE = 'core/semantic_forms/form'

    def __init__(self, form_spec, form_id='form', data=None, initial=None):
        self._initial = initial or {}
        self._data = data or {}
        self._spec = ObjDict(form_spec)
        self._bound = False
        self._clean_data = {}
        self._template_base = self._spec.template_base or self.DEFAULT_TEMPLATE_BASE
        self.id = form_id
        self._last_field_id = 0
        self.title = self._spec.title
        self.display = self._spec.display
        self.fields = []
        self.errors = None
        self.field_errors = []
        self.valid = None
        for field_name, field_spec in self._spec.get('fields', {}).items():
            self.fields.append(SemanticField.factory_create(self, self._last_field_id, field_name, field_spec))
            self._last_field_id += 1
        if self._data:
            self.bind(data)

    def bind(self, data):
        for fld in self.fields:
            fld.bind(data)
        self._bound = True
        self.valid = None

    def validate(self):
        if not self._bound:
            raise FormError("Cannot validate unbound field.")
        if self.valid is not None:
            return self.errors, self.field_errors
        self._clean_data = {}
        self.errors = []
        for fld in self.fields:
            self.field_errors.extend(fld.validate())
            if fld.valid:
                fld.clean()
                self._clean_data[fld.name] = fld.value
        if self.errors or self.field_errors:
            self.valid = False
        else:
            self.valid = True
        return self.errors, self.field_errors

    @property
    def clean_data(self):
        return self._clean_data

    def render(self, format='html'):
        tpl = get_template(self._template_base+'.'+format)
        return mark_safe(tpl.render({'form': self}))
