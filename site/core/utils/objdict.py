def recursive_copy(obj, depth=-1):
    if depth == 0:
        return obj
    if isinstance(obj, list):
        return [recursive_copy(x, depth-1) for x in obj]
    elif isinstance(obj, dict):
        return ObjDict({k: recursive_copy(v, depth-1) for k, v in obj.items()})
    elif hasattr(obj, 'copy'):
        return obj.copy()
    else:
        return obj


def merge(where, what):
    for k, v in what.items():
        if k in where:
            if isinstance(v, dict):
                merge(where[k], ObjDict(v))
            else:
                where[k] = recursive_copy(v)
        else:
            where[k] = recursive_copy(v)


class ObjDict(dict):
    def __init__(self, items, default=None):
        super().__init__()
        self._default = default
        if items:
            for k, v in items.items():
                self[k] = recursive_copy(v)

    def __getattr__(self, attr):
        try:
            return super().__getattribute__(attr)
        except AttributeError:
            return self._get(attr, self._default)

    def __delattr__(self, attr):
        if attr.startswith('_') or hasattr(self, attr):
            super().__delattr__(attr)
        else:
            parent = self
            for part in attr.split('.')[:-1]:
                parent = self[part]
            del parent[attr]

    def _get(self, path, default):
        ret = self
        for part in path.split('.'):
            if part not in ret:
                return default
            else:
                ret = ret[part]
        return ret
