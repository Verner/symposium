from core import semantic_forms
from core.utils.objdict import ObjDict


TEST_FORM = {
    "title": "Personal Details",
    "fields": {
        "title": {
            "label": "Title",
            "type": "choice",
            "choices": ["Dr.", "Mr.", "Ms."]
        },
        "firstname": {
            "label": "First name",
            "default": "profile.user.firstname",
            "required": True,
            "type": "string",
            "display": {
                "width": "half"
            }
        },
        "last": {
            "label": "Last name",
            "default": "profile.user.lastname",
            "required": True,
            "type": "string",
        },
        "email": {
            "label": "Email",
            "type": "email",
            "display": {
                "width": "half"
            }
        }
    },
    "layout": [
        [("title", 1), ("firstname", 2), ("lastname", 3)],
    ]
}


def test_form_spec():
    profile = ObjDict({
        'user': {
            'firstname': 'Jonathan',
            'lastname': 'Verner',
            'email': 'jonathan@temno.eu',
        }
    })
    F = semantic_forms.DynamicForm(TEST_FORM, initial={'profile': profile})
    assert len(F.fields) == 4

