from core.utils.objdict import ObjDict, merge


def test_obj_dict():
    class DummyObject:
        b = 30
    SRC = {
        'a': {'b': 10},
        'c': {'b': 20},
        'd': DummyObject()
    }
    data = ObjDict(SRC)
    assert data.a.b == 10
    assert data.c.b == 20
    assert data.d.b == DummyObject.b
    assert data.e is None

    data = ObjDict({}, default=10)
    assert data.b == 10


def test_merge():
    A = {
        'common': {'common': 'a','a': 'a'},
        'a': {'a': 'a'},
        'overriden': {'a': 'a'}
    }
    B = {
        'common': {'common': 'b','b': 'b'},
        'b': {'b': 'b'},
        'overriden': 'b'
    }
    M = ObjDict(A)
    assert M.common.common == 'a'
    assert M.overriden.a == 'a'
    assert M.b is None
    assert ObjDict(B).b.b == 'b'

    merge(M, B)

    assert M.common.common == 'b'
    assert M.common.a == 'a'
    assert M.common.b == 'b'
    assert M.a.a == 'a'
    assert M.b.b == 'b'
    assert M.overriden == 'b'


def test_list():
    A = ObjDict({
        'a': 'a',
        'lst': [{'a': 'a'}, {'b': 'b'}, 0, 1]
    })

    assert A.a == 'a'
    assert A.lst[0].a == 'a'
    assert A.lst[1].b == 'b'
    assert A.lst[2] == 0
    assert A.lst[-1] == 1
