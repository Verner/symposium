from django.contrib import admin
from liveedit.models import register_model

from .models import events, people

models = [
    events.ConferenceSeries,
    events.ConferenceRun,
    people.Profile,
    people.Institution,
]

for m in models:
    admin.site.register(m)

live_editable = models

for m in live_editable:
    register_model(m)
