from django.urls import include, path
from rest_framework import routers

from . import views

app_name = 'core'

urlpatterns = []

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
# router.register('token', views.TokenViewSet)

rest_urls = [
    path('', include(router.urls)),
]

