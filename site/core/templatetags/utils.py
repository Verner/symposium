from django.utils.safestring import mark_safe

from django_jinja.library import global_function as jinja_tag
from assets.templatetags.asset import asset



@jinja_tag
def icon(icon_name):
    """
        {{ fa_icon("address-book") }} renders as <span class='icon fa fa-address-book'></span>
    """
    return mark_safe("<i class='fas fa-{name}'></i>".format(name=icon_name))
    #return mark_safe("<svg><use xlink:href={svg_path}#{icon}></use></svg>".format(icon=icon_name, svg_path=asset('core:icons:solid.svg')))
    #return mark_safe("<img src='{svg_path}' />".format(icon=icon_name, svg_path=asset('core:icons:'+icon_name+'.svg')))
