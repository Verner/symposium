import jinja2

from django.urls import reverse
from django_jinja.library import global_function

from ..utils import replace_with_slugs
from ..models import MenuItem, ContentFragment


@jinja2.contextfunction
@global_function
def render_fragment(context, page, label):
    try:
        return jinja2.Markup(page.fragments[label].render(format='html'))
    except:
        if context.template.engine.debug:
            raise jinja2.TemplateRuntimeError(message=_("Required fragment not found:")+str(label))
        else:
            return ''


@jinja2.contextfunction
@global_function
def declare_fragments(context, labels):
    if not hasattr(context,'__fragments'):
        setattr(context, '__fragments', [])
    context.__fragments.extend(labels.split(','))
    return ''


@global_function
def slugify(html):
    return jinja2.Markup(replace_with_slugs(html))

@global_function
def load_menu(menu_name, site_prefix=None):
    return MenuItem.objects.get(name=menu_name, site_prefix=site_prefix)

@global_function
def load_fragment(fragment_name, site_prefix=None):
    return ContentFragment.objects.get(name=fragment_name)



@jinja2.contextfunction
@global_function
def language_chooser(context):
    translations = [(lang[0], reverse('simplecms:set_lang', kwargs={'language_code': lang[0]})) for lang in settings.LANGUAGES]
    return translations

