from django.contrib import admin
from django.contrib.postgres.fields import JSONField as PG_JSONField

from jsoneditor.forms import JSONEditor
from jsonfield import JSONField as JSONField

from liveedit.models import register_model

from .models import ContentFragment, Page, PageTemplate, MenuItem, MediaElement


class MyAdmin(admin.ModelAdmin):
    formfield_overrides = {
        PG_JSONField: {'widget': JSONEditor},
        JSONField: {'widget': JSONEditor},
    }


models = [ContentFragment, Page, PageTemplate, MenuItem, MediaElement]
for m in models:
    admin.site.register(m, MyAdmin)

live_editable = [Page, ContentFragment]
for m in live_editable:
    register_model(m)
