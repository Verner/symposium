import os
from urllib.parse import urlparse

from django.conf import settings
from django.contrib.auth.decorators import permission_required
from django.http import Http404, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.template.response import TemplateResponse
from django.urls import reverse, reverse_lazy, resolve
from django.utils.http import is_safe_url, urlunquote
from django.utils.translation import get_language_from_request, check_for_language, LANGUAGE_SESSION_KEY, activate

from .models import Page, PageTemplate, MediaElement
from .forms import PageForm


def pageview(request, slug, site_prefix=None):
    try:
        lang_code = get_language_from_request(request)
        slug = '/'+slug.strip('/')+'/'
        page = Page.objects.get(slug=slug, language=lang_code)
        tpl = page.template.load()
        return TemplateResponse(request, tpl, {'page': page, 'slug': slug, 'site_prefix': site_prefix})
    except Exception as _:
        raise Http404("Page '"+slug+"' does not exist.")


def home_page(request, site_prefix=None):
    lang_code = get_language_from_request(request)
    try:
        slug = '//'
        page = Page.objects.get(slug=slug, language=lang_code)
    except Page.DoesNotExist:
        if request.user.has_perm('simplecms.add_page'):
            page = Page(title='Home Page',
                        template=PageTemplate.objects.get(path='simplecms/cms_default_template.html'),
                        author=request.user,
                        language=lang_code,
                        slug='//'
                       )
            page.save()
            page.add_fragment(label='main')
            page.save()
        else:
            raise Http404("HomePage does not exist.")
    tpl = page.template.load()
    return TemplateResponse(request, tpl, {'page': page, 'slug': '', 'site_prefix': site_prefix})


def set_lang(request, language_code):

    redirect_to = request.GET.get('next', None)
    if redirect_to is None or not is_safe_url(redirect_to):
        redirect_to = request.META.get('HTTP_REFERER')
        if redirect_to:
            redirect_to = urlunquote(redirect_to)  # HTTP_REFERER may be encoded.
        if not is_safe_url(url=redirect_to, host=request.get_host()):
            redirect_to = '/'
    redirect_path = urlparse(redirect_to).path

    # Get the current language (before the switch)
    old_lang_code = get_language_from_request(request)

    # If the language is the same, just redirect back
    if old_lang_code == language_code:
        return HttpResponseRedirect(redirect_to)

    # Determine the current view
    match = resolve(redirect_path)

    # Activate the new language
    activate(language_code)

    if match.view_name == 'simplecms:page_view':
        # Deal with CMS pages
        slug = '/'+match.kwargs['slug'].strip('/')+'/'

        # Get the original page
        page = Page.objects.get(slug=slug, language=old_lang_code)

        # Try to find a translation
        try:
            translated_page = page.translated(language=language_code)
            continue_to = reverse('simplecms:page_view', kwargs={
                'slug': translated_page.slug.strip('/'),
                'site_prefix': translated_page.site_prefix
            })
        except Exception as _:
            # Translated page does not exist, create one
            continue_to = reverse('simplecms:page_translate', kwargs={
                'slug': slug,
                'language_code': language_code,
                'site_prefix': page.site_prefix
            })
    elif match.view_name == 'simplecms:page_translate':
        continue_to = reverse('simplecms:page_view', kwargs={'slug': match.kwargs['slug'], 'site_prefix': match.kwargs.get('site_prefix',None)})
    elif match.view_name.startswith('simplecms:set_lang'):
        continue_to = reverse('simplecms:home_page', kwargs={'site_prefix': match.kwargs.get('site_prefix', None)})
    else:
        # Deal with non CMS pages
        continue_to = reverse(match.view_name, args=match.args, kwargs=match.kwargs)

    response = HttpResponseRedirect(continue_to)

    # Set cookies & session variables
    if language_code and check_for_language(language_code):
        if hasattr(request, 'session'):
            request.session[LANGUAGE_SESSION_KEY] = language_code
            response.set_cookie(
                settings.LANGUAGE_COOKIE_NAME, language_code,
                max_age=settings.LANGUAGE_COOKIE_AGE,
                path=settings.LANGUAGE_COOKIE_PATH,
                domain=settings.LANGUAGE_COOKIE_DOMAIN,
            )
        else:
            response.set_cookie(
                settings.LANGUAGE_COOKIE_NAME, language_code,
                max_age=settings.LANGUAGE_COOKIE_AGE,
                path=settings.LANGUAGE_COOKIE_PATH,
                domain=settings.LANGUAGE_COOKIE_DOMAIN,
            )
    return response


@permission_required('simplecms.add_page')
def page_translate(request, slug, language_code, site_prefix=None):
    original_page = Page.objects.get(slug=slug, site_prefix=site_prefix)
    translated_page = original_page.create_translation(request.user, language_code)
    tpl = translated_page.template.load()
    return TemplateResponse(request, tpl, {'page': translated_page, 'slug': slug, 'edit_slug': True, 'site_prefix': site_prefix})


@permission_required('simplecms.add_page')
def page_create(request, site_prefix=None):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = PageForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            page = form.save(commit=False)
            page.site_prefix=site_prefix
            if not page.language:
                page.language = 'en'
            page.author = request.user
            page.save()
            for label in page.template.fragment_labels:
                page.add_fragment(label)
            redir_url = "{scheme}://{host}{path}".format(scheme=request.scheme, host=request.get_host(), path=page.get_absolute_url())
            return HttpResponseRedirect(reverse('close-popup')+"?redirect_parent_url="+redir_url)
    # if a GET (or any other method) we'll create a blank form
    else:
        form = PageForm(initial={
            'language': 'en',
        })
    return render(request, 'simplecms/page_create.html', {'form': form, 'site_prefix': site_prefix})


@permission_required('simplecms.add_mediaelement')
def mediaelement_create(request):
    if request.method == 'POST':
        elt = MediaElement(
            author=request.user,
            language='en',
            title=request.FILES['upload'].name,
            content=request.FILES['upload'],
            site_prefix=site_prefix
        )
        elt.save()
        return JsonResponse({
            'uploaded': 1,
            'fileName': os.path.basename(elt.content.name),
            'url': elt.content.url
        })
    return Http404("Endpoint valid for uploads only")
