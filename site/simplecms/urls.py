from django.conf.urls import url, include

from . import models
from . import views

app_name = 'simplecms'

urlpatterns = [
    url(r'^cms_admin/page/create/$', views.page_create, name='page_create'),
    url(r'^cms_admin/page/translate/(?P<slug>[-\w/]+)/(?P<language_code>[\w/]+)/$', views.page_translate, name='page_translate'),
    url(r'^api/mediaelement/create/$', views.mediaelement_create, name='mediaelement_create'),
    url(r'^set_lang/(?P<language_code>\w\w)/$', views.set_lang, name='set_lang'),
    url(r'^(?P<slug>[-\w/]+)/$', views.pageview, name='page_view'),
    url(r'^$', views.home_page, name='home_page'),
]

