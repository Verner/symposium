from profile import Profile

prof_import = Profile()
prof_import.enable()

import json

from browser import document as doc, window, html, console
from brdj.ui.modelwidgets import CreateInstanceDialog, ModelListView
from models import Page


class PageView(ModelListView):
    class Meta:
        model = Page

import_stats = prof_import.create_stats()


def main():
    prof_main = Profile()
    from browser import window, document as doc
    from brdj.ui.console import Console
    cons = Console(doc["console"])
    page_view = PageView()
    doc['pages'] <= page_view.element
    cons.add_to_ns('pv', page_view)
    cons.add_to_ns('i_stats', import_stats)
    main_stats = prof_main.create_stats()
    cons.add_to_ns('main_stats', main_stats)

main()





