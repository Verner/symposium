import re

from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied, ValidationError
from django.template import Template, engines
from django.template.loader import get_template
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _

from jsonfield import JSONField


class MenuItem(models.Model):
    name = models.CharField(max_length=255)
    children = JSONField(default=[], blank=True, null=True)
    language = models.CharField(max_length=7, choices=settings.LANGUAGES)
    site_prefix = models.CharField(max_length=500, blank=True, null=True)

    @classmethod
    def _validate_menu_item(cls, item):
        if 'type' not in item:
            item['type'] = 'menuitem'
        tp = item['type']
        if tp == 'menuitem':
            if 'title' not in item:
                raise ValidationError('Title field required')
            if 'children' not in item:
                raise ValidationError('Children field required')
            if not item['children'] and 'url' not in item:
                raise ValidationError('URL field or at least one child required')
        elif tp == 'divider':
            return
        elif tp == 'header':
            if 'title' not in item:
                raise ValidationError

        for ch in item['children']:
            cls._validate_menu_item(ch)

    def save(self):
        for ch in self.children:
            self._validate_menu_item(ch)
        super().save()

    def __str__(self):
        return self.name +"("+self.language+")"


class PageTemplate(models.Model):
    title = models.CharField(max_length=255, blank=True)
    src = models.TextField(blank=True)
    path = models.CharField(max_length=255, blank=True)

    def clean(self):
        if '..' in self.path:
            raise ValidationError(_("Template paths may not contain '..'"))
        if (self.path and self.src) or (not self.path and not self.src):
            raise ValidationError(_("Template may specify ONLY ONE of 'path' or 'src'."))
        if self.path == '' and not settings.SIMPLE_CMS['enable_dynamic_templates']:
            raise PermissionDenied(_('User supplied templates not allowed.'))
        try:
            self.load()
        except Exception as ex:
            raise ValidationError("Invalid template path (not found or not able to parse): "+str(ex))

    def load(self):
        if len(self.path) > 0:
            return get_template(template_name=self.path)
        else:
            if not settings.SIMPLE_CMS['enable_dynamic_templates']:
                raise PermissionDenied(_('User supplied templates not allowed.'))
            return Template(self.src)

    @cached_property
    def fragment_labels(self):
        tpl = self.load()
        ctx = {}
        try:
            tpl.render(ctx)
        except:
            pass

        return set(ctx.getattr('__fragments', []))

    def __str__(self):
        ret = self.title
        if len(self.path) > 0:
            ret += ' (static)'
        else:
            ret += ' (dynamic)'
        return ret


PAGE_FORMATS = [
    ('html', 'HTML'),
    ('jinja2', 'Jinja2 Template')
]

SLUG_RE = re.compile('[a-z-_/]*', re.IGNORECASE)


class ContentFragment(models.Model):
    content = models.TextField(blank=True)
    content_format = models.CharField(default='html', choices=PAGE_FORMATS, max_length=20)
    name = models.CharField(max_length=255, blank=True, default='')

    def render(self, format='html', context=None):
        if self.content_format == 'jinja2':
            jinja2_engine = engines.all()[0]
            tpl = jinja2_engine.from_string(self.content)
            return tpl.render(context)
        return self.content

    def __str__(self):
        structures = self.pagestructure_set.all()
        if len(structures) == 1:
            return str(structures[0])
        else:
            return self.name
        return self.content


class Page(models.Model):
    title = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)
    author = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, blank=True)
    language = models.CharField(max_length=7, choices=settings.LANGUAGES)
    structure = models.ManyToManyField(ContentFragment, through='PageStructure')
    translation_of = models.ForeignKey('Page', null=True, on_delete=models.SET_NULL, blank=True)
    template = models.ForeignKey(PageTemplate, null=True, on_delete=models.SET_NULL, blank=True)
    site_prefix = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        unique_together = (("slug", "language"),)

    def clean(self):
        if not SLUG_RE.match(self.slug):
            raise ValidationError(_("Slug may only contain characters from the english alphabet, '-' and '_'."))
        self.slug = '/' + self.slug.strip('/') + '/'

    def save(self, *args, **kwargs):
        if not self.pk:
            create = True
        else:
            create = False
        super().save(*args, **kwargs)
        if create:
            for label in self.template.fragment_labels:
                self.add_fragment(label)

    def get_absolute_url(self):
        return reverse('simplecms:page_view', kwargs={'slug': self.slug})

    @cached_property
    def fragments(self):
        return {ps.fragment_label: ps.fragment for ps in PageStructure.objects.filter(page=self)}

    def add_fragment(self, label='', copy=None):
        if copy is not None:
            content = copy.content
            content_format = copy.content_format
        else:
            content = ''
            content_format = 'html'
        frag = ContentFragment(content=content, content_format=content_format)
        frag.save()
        structure = PageStructure(page=self, fragment=frag, fragment_label=label)
        structure.save()
        return frag

    def translated(self, language):
        if self.translation_of:
            return self.translation_of.translated(language) # pylint: disable=no-member
        else:
            if self.language == language:
                return self
            return Page.objects.get(translation_of=self, language=language)

    def create_translation(self, author, language_code):
        translation = Page(
            author=author,
            title=self.title,
            slug=self.slug,
            language=language_code,
            translation_of=self,
            template=self.template
        )
        translation.save()
        for label, frag in self.fragments.items():
            translation.add_fragment(label, frag)
        return translation

    def __str__(self):
        return self.title+"("+self.language+")"


class PageStructure(models.Model):
    page = models.ForeignKey(Page, on_delete=models.CASCADE)
    fragment = models.ForeignKey(ContentFragment, on_delete=models.CASCADE)
    fragment_label = models.CharField(max_length=255, blank=True)

    class Meta:
        unique_together = (("page", "fragment_label"),)

    def __str__(self):
        return str(self.page)+":"+self.fragment_label


class MediaElement(models.Model):
    title = models.CharField(max_length=255, blank=True, null=True)
    author = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, blank=True)
    language = models.CharField(max_length=7, choices=settings.LANGUAGES)
    content = models.FileField(upload_to='cms/media/%Y/%m/')

    def __str__(self):
        if self.title:
            return self.title+" ("+str(self.author)+")"
        return self.content.name+" ("+str(self.author)+")"

