import re
from .models import Page


INTERNAL_LINK_RE = re.compile(r"""{{\s*slug:(?P<page_id>[^ }]*)\s*}}""", re.IGNORECASE | re.DOTALL)


def _replacer(match):
    id = match.group('page_id')
    page = Page.get(pk=id)
    return page.get_absolute_url()


def replace_with_slugs(html):
    return INTERNAL_LINK_RE.sub(_replacer, html)
