from django.apps import AppConfig


class SimpleAuthConfig(AppConfig):
    name = 'simpleauth'

