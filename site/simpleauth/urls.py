from django.urls import include, path
from rest_framework import routers

from . import views

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register('token', views.TokenViewSet)

app_name = 'simpleauth'
urlpatterns = [
    path('<str:service>/start', views.oauth_login_start_view, name='orcid_login_start'),
    path('<str:service>/finish', views.oauth_login_finish_view, name='orcid_login_finish'),
]

rest_urls = [
    path('', include(router.urls)),
]


