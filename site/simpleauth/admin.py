from django.contrib import admin

# Register your models here.
from . import models

models = [models.Token]

for m in models:
    admin.site.register(m)
