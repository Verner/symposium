try:
    import simplepam
except:
    class simplepam:
        @classmethod
        def authenticate(cls, u, p):
            return False

from django.contrib.auth.models import User
from django.views.decorators.debug import sensitive_variables


class PAMBackend(object):

    @sensitive_variables('password')
    def authenticate(self, request=None, username=None, password=None):
        if simplepam.authenticate(username, password):
            try:
                user = User.objects.get(username=username)
            except User.DoesNotExist:
                user = User(username=username, password='')
                user.save()
            return user
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

