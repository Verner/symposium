from django.conf import settings
from django.contrib.auth import login
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from requests_oauthlib import OAuth2Session
from rest_framework import serializers, viewsets

from . import models


# Serializers define the API representation.
class TokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Token
        fields = ('service', 'user', 'oauth_id')
        depth = 1


# ViewSets define the view behavior.
class TokenViewSet(viewsets.ModelViewSet):
    queryset = models.Token.objects.all()
    serializer_class = TokenSerializer


def oauth_login_start_view(request, service):
    scope = ['/authenticate']
    api = OAuth2Session(settings.OAUTH[service]['client_id'], redirect_uri=request.build_absolute_uri(reverse('AUTH:oauth_login_finish')), scope=scope)
    auth_redirect_url, state = api.authorization_url(settings.OAUTH[service]['auth_url'])
    request.session['oauth:'+service+':state'] = state
    request.session['oauth:'+service+':next'] = request.GET.get('next', None)
    request.session['oauth:'+service+':popup'] = request.GET.get('popup', None)
    return HttpResponseRedirect(request.build_absolute_uri(auth_redirect_url))


def oauth_login_finish_view(request, service):
    try:
        api = OAuth2Session(settings.OAUTH[service]['client_id'], state=request.session['oauth:'+service+':state'])
        token = api.fetch_token(settings.OAUTH[service]['token_url'], client_secret=settings.OAUTH[service]['client_secret'], code=request.GET.get('code', ''))
    except Exception as ex:
        raise PermissionDenied(ex)
    oauth_id = token[settings.OAUTH[service]['oauth_id_attr']]
    next_url = request.session['oauth:'+service+':next']
    popup = request.session['oauth:'+service+':popup']
    try:
        tok = models.Token.objects.get(oauth_id=oauth_id)
        tok.token = token
        tok.save()
        login(request, tok.user, backend=settings.AUTHENTICATION_BACKENDS[0])
        if popup:
            if next_url:
                return HttpResponseRedirect(reverse('close-popup')+"?redirect_parent_url="+next_url)
            return HttpResponseRedirect(reverse('close-popup')+"?reload=1")
        if next_url:
            return HttpResponseRedirect(next_url)
        return HttpResponseRedirect('/')
    except:
        if request.user.is_authenticated():
            tok = models.Token(user=request.user, token=token, oauth_id=oauth_id)
            tok.save()
            if popup:
                if next_url:
                    return HttpResponseRedirect(reverse('close-popup')+"?redirect_parent_url="+next_url)
                return HttpResponseRedirect(reverse('close-popup')+"?reload=1")
            if next_url:
                return HttpResponseRedirect(next_url)
            return HttpResponseRedirect('/')

        # Register user
        return render(request, 'auth/oauth_error.html', {'error': 'No user corresponding to oauthid', 'oauth_id': oauth_id, 'service': service})
