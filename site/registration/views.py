from functools import reduce

from django.contrib.auth.models import User, login
from django.db.models import Q
from django.shortcuts import render

from core.semantic_forms import DynamicForm
from core.models.events import ConferenceRun, Profile
from core.signals import PROFILE_CREATED

from . import signals
from .models import RegistrationForm, Participant


class RegistrationStatus:
    def __init__(self, source, successful, message):
        self.source = source
        self.message = message
        self.successful = successful

    def is_successful(self):
        return self.successful


def register(request, conf_run):
    form_spec = RegistrationForm.objects.get(conf=conf_run)

    if request.user:
        initial = {'profile': request.user.profile}
    else:
        initial = {}

    statuses = []
    successful = None

    if not request.method == 'POST':
        form = DynamicForm(form_spec, initial=initial)
    else:
        # if this is a POST request we need to process the form data
        form = DynamicForm(form_spec, data=request.POST, initial=initial)
        successful = form.is_valid()

        if successful:
            participant = Participant()

            # Load user supplied data
            for fld in Participant.USER_PROVIDED_FIELDS:
                if fld in form.clean_data:
                    setattr(participant, fld, form.clean_data['fld'])

            # Provide registration generated data
            participant.status = 'received'
            participant.conference = ConferenceRun.objects.get(slug=conf_run)

            # Allow other apps to process registration data
            statuses = signals.REGISTRATION_RECEIVED.send_robust(sender=signals.SENDER, form=form)
            successful = reduce(lambda result, status: result and status.is_successful(), statuses, True)

        if successful:
            # Find user profile or create new one
            if request.user:
                participant.profile = request.user.profile
            else:
                try:
                    profile = Profile.object.get(
                        Q(email=form.clean_data['email']) |
                        (Q(firstname=form.clean_data['firstname']) & Q(lastname=form.clean_data['lastname']))
                    )
                except:
                    user = User(firstname=form.clean_data['firstname'],
                                lastname=form.clean_data['lastname'],
                                email=form.clean_data['email'])
                    user.save()
                    profile = Profile(form.clean_data)
                    profile.user = user
                    PROFILE_CREATED.send_robust(sender=signals.SENDER, profile=profile)
                login(request, profile.user)
                participant.profile = profile

            signals.PARTICIPANT_CREATED.send(sender=signals.SENDER, profile=profile)

    return render(request, 'registration/registration.html', {'form': form, 'statuses': statuses, 'successful': successful})
