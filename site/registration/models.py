from datetime import datetime
from django.db import models
from django.contrib.postgres.fields import JSONField

from core.models.events import ConferenceRun
from core.models.people import Profile, Institution

DEFAULT_FORM = {
    "personal": {
        "title": "Personal Details",
        "fields": {
            "firstname": {
                "label": "First name",
                "default": "profile.user.firstname",
                "required": True,
                "type": "string",
                "display": {
                    "width": "half"
                }
            },
            "avatar": {
                "label": "Photo",
                "help": {
                    "display": "tooltip",
                    "text": "The plan is to make a list of people with their photos available for the Winter School to help you recognize people you do not know."
                },
                "type": "photo:size=200x300",
                "display": {
                    "position": "float",
                    "width": "half"
                }
            },

            "lastname": {
                "label": "Last name",
                "default": "profile.user.firstname",
                "required": True,
                "type": "string",
                "display": {
                    "width": "half"
                }
            },
            "email": {
                "label": "Email",
                "default": "profile.user.email",
                "required": True,
                "type": "email",
                "display": {
                    "width": "half"
                }
            },
            "www": {
                "label": "www",
                "default": "profile.www",
                "required": False,
                "type": "url",
                "display": {
                    "width": "half"
                }
            },
        }
    },
    "affiliation": {
        "title": "Affiliation",
        "display": {
            "help": {
                "above": "Due to receiving support, we are required to collect basic contact information of registered participants."
            },
        },
        "fields": {
            "affiliation": {
                "label": "Institution",
                "default": "profile.affiliation.name",
                "required": "false",
                "type": "ref:model=core.models.people.Institution",
                "restrict_to_existing": False
            },
            "affiliation.freeform_address": {
                "label": "Address",
                "default": "affiliation.freeform_address"
            },
            "affiliation.country": {
                "label": "Country",
                "default": "affiliation.country",
                "type": "ref:countries_plus.models.Country",
                "restrict_to_existing": True
            }
        }
    },
    "conference": {
        "title": "Conference Details",
        "fields": {
            "diet": {
                "label": "Diet",
                "default": ["profile.meta_data.diet", "None"],
                "required": True,
                "type": "choice",
                "choices": ['none', 'vegetarian', 'vegan', 'halal', 'kosher', 'other'],
                "help": {
                    "tooltip": "If you choose 'other', please specify in the Notes to organizers",
                },
                "width": "third"
            },
            "notes": {
                "label": "Notes to the organizers",
                "required": False,
                "type": "text",
            }
        }


    }
}


class RegistrationForm(models.Model):
    conf = models.OneToOneField(ConferenceRun, on_delete=models.CASCADE)
    registration_form = JSONField(default=DEFAULT_FORM)

    def __str__(self):
        return "Registration Form ("+self.conf.run+")"


class Participant(models.Model):
    STATUS_CHOICES = (
        ('received', 'Received Registration'),
        ('accepted', 'Registration Accepted'),
        ('conditional', 'Registration Conditionally Accepted'),
        ('canceled', 'Registration Canceled'),
        ('spam', 'Spam'),
    )
    TYPE_CHOICES = (
        ('regular', 'Regular Participant'),
        ('invited', 'Invited Participant'),
        ('organizer', 'Organizer'),
        ('accompaying', 'Accompanying person')
    )
    profile = models.ForeignKey(Profile, blank=True, null=True, on_delete=models.SET_NULL)

    # User provided fields
    USER_PROVIDED_FIELDS = ['firstname', 'lastname', 'avatar', 'affiliation', 'web', 'gender']

    avatar = models.ImageField(blank=True, null=True)
    affiliation = models.ForeignKey(Institution, blank=True, null=True, on_delete=models.SET_NULL)
    web = models.URLField(blank=True, null=True)
    gender = models.CharField(choices=Profile.GENDER_CHOICES, max_length=2, blank=True, null=True)
    meta_data = JSONField(default=dict)

    conference = models.OneToOneField(ConferenceRun, on_delete=models.CASCADE)
    status = models.CharField(choices=STATUS_CHOICES, max_length=20, default='received')
    registration_type = models.CharField(choices=TYPE_CHOICES, max_length=20, default='regular')
    received = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return self.firstname+" "+self.lastname+" ("+self.status+","+self.type+")"
