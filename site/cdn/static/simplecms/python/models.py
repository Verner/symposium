import re
import brdj.django.db.models as models

from brdj.django.contrib.auth.models import User
from brdj.django.utils.translation import ugettext_lazy as _
from brdj.django.core.exceptions import ValidationError
from brdj.django.conf import settings


class PageTemplate(models.Model):
    title = models.CharField(max_length=255, blank=True)
    src = models.TextField(blank=True)
    path = models.CharField(max_length=255, blank=True)

    def clean(self):
        if '..' in self.path:
            raise ValidationError(_("Template paths may not contain '..'"))
        if (len(self.path) > 0 and len(self.src) > 0) or (len(self.path) == 0 and len(self.path) == 0):
            raise ValidationError(_("Template may specify ONLY ONE of 'path' or 'src'."))

    def __str__(self):
        ret = self.title
        if len(self.path) > 0:
            ret += ' (static)'
        else:
            ret += ' (dynamic)'
        return ret

models.register(PageTemplate)

PAGE_FORMATS = [
    ('html', 'HTML')
]

SLUG_RE = re.compile('[a-z-_/]', re.IGNORECASE)


class Page(models.Model):
    title = models.CharField(max_length=255, blank=True)
    slug = models.CharField(max_length=255, blank=True)
    author = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, blank=True)
    language = models.CharField(max_length=7, choices=settings.LANGUAGES)
    content = models.TextField(blank=True)
    overview = models.TextField(blank=True)
    translation_of = models.ForeignKey('Page', null=True, on_delete=models.SET_NULL, blank=True)
    template = models.ForeignKey(PageTemplate, null=True, on_delete=models.SET_NULL, blank=True)
    content_format = models.CharField(default='html', choices=PAGE_FORMATS, max_length=20)

    def clean(self):
        if not SLUG_RE.match(self.slug):
            raise ValidationError(_("Slug may only contain characters from the english alphabet, '-' and '_'."))
        self.slug = '/' + self.slug.strip('/') + '/'

    def __str__(self):
        return self.title
        pass

models.register(Page)
