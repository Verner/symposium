/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./tmpzh7aq7at.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "../../../../../../../.npm-packages/lib/node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var g;\r\n\r\n// This works in non-strict mode\r\ng = (function() {\r\n\treturn this;\r\n})();\r\n\r\ntry {\r\n\t// This works if eval is allowed (see CSP)\r\n\tg = g || Function(\"return this\")() || (1, eval)(\"this\");\r\n} catch (e) {\r\n\t// This works if the window reference is available\r\n\tif (typeof window === \"object\") g = window;\r\n}\r\n\r\n// g can still be undefined, but nothing to do about it...\r\n// We return undefined, instead of nothing here, so it's\r\n// easier to handle this case. if(!global) { ...}\r\n\r\nmodule.exports = g;\r\n\n\n//# sourceURL=webpack:///(webpack)/buildin/global.js?");

/***/ }),

/***/ "./lib/event.js":
/*!**********************!*\
  !*** ./lib/event.js ***!
  \**********************/
/*! exports provided: Publisher */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Publisher\", function() { return Publisher; });\nclass Publisher extends EventTarget {\n    constructor() {\n        super()\n        this._subscribers = {}\n        this._event_types = {}\n        this._pub_proxy = this\n    }\n\n    _reAddListeners() {\n        for(const [sub, subscriptions] of Object.entries(this._subscribers)) {\n            for(const [ev_type, handlers] of Object.entries(subscriptions)) {\n                for(const h of handlers)\n                    this._pub_proxy.addEventListener(ev_type, h)\n            }\n        }\n    }\n\n    registerEvent(event_type, opts) {this._event_types[event_type] = opts}\n\n    setPubProxy(src){this._pub_proxy = src}\n\n    pub(event_type, data) {\n        let opts = this._event_types[event_type]\n        let ev = new Event(event_type, opts);\n        ev.data = data\n        this._pub_proxy.dispatchEvent(ev)\n    }\n\n    sub(owner, event_type, handler) {\n        if (this._subscribers[owner] === undefined) this._subscribers[owner] = {}\n        let subs = this._subscribers[owner]\n        if (subs[event_type] === undefined) subs[event_type] = [];\n        subs[event_type].push(handler)\n        this._pub_proxy.addEventListener(event_type, handler)\n    }\n\n    unsub(owner, event_type='*', handler='*') {\n        let subs = this._subscribers[owner]\n\n        if (event_type === '*') {\n            for(let ev in subs) this.unsub(owner, ev, handler)\n        } else if ( handler === '*' ) {\n            for(let h of subs[event_type]) this._pub_proxy.removeEventListener(event_type, h)\n            subs[event_type] = []\n        } else {\n            this._pub_proxy.removeEventListener(event_type, handler)\n            let index = subs[event_type].indexOf(handler)\n            delete subs[event_type][index]\n        }\n\n    }\n}\n\n\n\n\n//# sourceURL=webpack:///./lib/event.js?");

/***/ }),

/***/ "./lib/models.js":
/*!***********************!*\
  !*** ./lib/models.js ***!
  \***********************/
/*! exports provided: evModelChanged, evChildAppended, evChildReplaced, Model, TreeModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"evModelChanged\", function() { return evModelChanged; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"evChildAppended\", function() { return evChildAppended; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"evChildReplaced\", function() { return evChildReplaced; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Model\", function() { return Model; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"TreeModel\", function() { return TreeModel; });\n/* harmony import */ var _event__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./event */ \"./lib/event.js\");\n\n\nconst evModelChanged = 'modelChanged';\nconst evChildAppended = 'childAppended';\nconst evChildReplaced = 'childReplaced';\n\n\nclass Model extends _event__WEBPACK_IMPORTED_MODULE_0__[\"Publisher\"] {\n\n    static fromObj(obj) {\n        let ret;\n        if (obj instanceof Array) {\n            ret = new Model([])\n            for(const item of obj) {\n                ret.data.push(Model.fromObj(item))\n            }\n        } else if (obj instanceof Object) {\n            ret = new Model({})\n            for(const [key, val] in Object.entries(obj)) {\n                ret.data[key] = Model.fromObj(val)\n            }\n        } else ret = new Model(obj)\n        return ret\n    }\n\n    constructor(data = {}) {\n        super()\n        this.data = data\n    }\n\n    get data() {return this._data}\n    set data(val) {\n        if (this._data instanceof _event__WEBPACK_IMPORTED_MODULE_0__[\"Publisher\"]) this._data.unsub(this)\n        this._data = val\n        if (this._data instanceof _event__WEBPACK_IMPORTED_MODULE_0__[\"Publisher\"]) this._data.sub(this, 'change', () => this.pub(evModelChanged, {'new': this}))\n        this.notifyChange()\n    }\n\n    notifyChange(){this.pub(evModelChanged, {'new': this})}\n\n}\n\nclass TreeModel extends Model {\n    static fromObj(obj, id_key='id', children_key='children') {\n        let data = {}\n        for(let [key, val] in Object.entries(obj)) if (key !== children_key) data[key] = val\n\n        let root = new TreeModel(obj[id_key], data)\n        for(let ch of obj[children_key]) root.appendChild(TreeModel.fromObj(ch, id_key, children_key))\n\n        return root\n    }\n\n    constructor(id, data = {}) {\n        super(data)\n        this._id = id\n        this._children = []\n        this._childrenById = {}\n    }\n\n    get id() {return this._id}\n    get parent() {return this._parent}\n    get root() {\n        let ret = this;\n        while (ret.parent) ret = ret.parent;\n        return ret\n    }\n\n    get children() {return this._children}\n\n    childById(id){return this._childrenById[id]}\n\n    appendChild(ch) {\n        this._childrenById[ch.id] = {ch:ch, index:this._children.length}\n        this._children.push(ch)\n        ch._parent = this\n        this.pub(evChildAppended, {'new': ch})\n        this.pub(evModelChanged, {'new': this})\n    }\n\n    replaceChild(old_child, new_child) {\n        let old = this._childrenById[old_child]\n        this._childrenById[new_child.id] = {ch:new_child, index:old.index}\n        this._children[old.index] = new_child\n        delete this._childrenById[old_child.id]\n        this.pub(evChildReplaced, {'new': new_child, 'old': old_child, 'index': old.index})\n        this.pub(evModelChanged, {'new': this})\n    }\n\n    subTreeAt(path) {\n        let subtree = this\n        for(let id of path.slice(1)) subtree = subtree._childrenById[id].ch\n        return subtree\n    }\n\n    get path() {\n        let ret = [this.id], node = this\n        while(node.parent) {\n            node = node.parent\n            ret.unshift(node.id)\n        }\n        return ret\n    }\n\n}\n\n\n//# sourceURL=webpack:///./lib/models.js?");

/***/ }),

/***/ "./lib/utils.js":
/*!**********************!*\
  !*** ./lib/utils.js ***!
  \**********************/
/*! exports provided: clearChildren, update */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"clearChildren\", function() { return clearChildren; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"update\", function() { return update; });\nfunction clearChildren(elt) {\n    while (elt.firstChild) elt.firstChild.remove();\n}\n\nfunction update(objWhat, objWith) {\n    if (objWith instanceof Object)  for(const [k, v] of Object.entries(objWith)) {\n        if (objWhat[k] && objWhat[k] instanceof Object && v instanceof Object) update(objWhat[k], v)\n        else objWhat[k] = v\n    }\n}\n\n\n//# sourceURL=webpack:///./lib/utils.js?");

/***/ }),

/***/ "./lib/views.js":
/*!**********************!*\
  !*** ./lib/views.js ***!
  \**********************/
/*! exports provided: ModelView, ListView, GridView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ModelView\", function() { return ModelView; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ListView\", function() { return ListView; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"GridView\", function() { return GridView; });\n/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utils */ \"./lib/utils.js\");\n/* harmony import */ var _widget__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./widget */ \"./lib/widget.js\");\n/* harmony import */ var _models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./models */ \"./lib/models.js\");\n\n\n\n\nclass ModelView extends _widget__WEBPACK_IMPORTED_MODULE_1__[\"Widget\"] {\n    constructor(model, options) {\n        super(options)\n        this.setModel(model, false)\n    }\n\n    _update() {}\n\n    get model() {return this._model}\n\n    setModel(model, update=true){\n        if (this._model) this._model.unsub(this)\n        this._model = model\n        this._model.sub(this, _models__WEBPACK_IMPORTED_MODULE_2__[\"evModelChanged\"], () => this._update())\n        if (update) this._update()\n    }\n}\n\nclass ListView extends ModelView {\n    constructor(model, ItemViewClass, options) {\n        let opts = {\n            tag: \"ul\",\n            cssClasses: [\"w-listview\"]\n        }\n        if (options) {\n            if (options.cssClasses) options.cssClasses = opts.cssClasses.concat(options.cssClasses)\n            Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"update\"])(opts, options)\n        }\n\n        super(model, opts)\n\n        this.ItemViewClass = ItemViewClass\n        this._update()\n    }\n\n    _update() {\n        this.clearChildren()\n        for(let ch of this.model.data) {\n            let item = new this.ItemViewClass(ch)\n            this.appendChild(item)\n        }\n    }\n}\n\nclass GridView extends ModelView {\n    constructor(model, ItemViewClass, options) {\n        let opts = {\n            tag: \"ul\",\n            cssClasses: [\"w-listview\"],\n            style: {\n                flexWrap:\"wrap\"\n            }\n        }\n        if (options) {\n            if (options.cssClasses) options.cssClasses = opts.cssClasses.concat(options.cssClasses)\n            Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"update\"])(opts, options)\n        }\n\n        super(model, options)\n        this.ItemViewClass = ItemViewClass\n        this._update()\n        this.sub(this, 'activated', (ev) => this.navigateTo(ev.data))\n    }\n\n    _update() {\n        this.clearChildren()\n        for(let ch of this.model.children) {\n            let item = new this.ItemViewClass(ch)\n            this.appendChild(item)\n        }\n    }\n\n    navigateUp() {\n        if (this.model.parent) {\n            this.pub('navigate', this.model.parent)\n            this.setModel(this.model.parent)\n        }\n    }\n\n    navigateToRoot() {\n        this.pub('navigate', this.model.root)\n        this.setModel(this.model.root)\n    }\n\n    navigateTo(ch) {\n        this.pub('navigate', ch)\n        this.setModel(ch)\n        this._update()\n    }\n\n}\n\n\n//# sourceURL=webpack:///./lib/views.js?");

/***/ }),

/***/ "./lib/widget.js":
/*!***********************!*\
  !*** ./lib/widget.js ***!
  \***********************/
/*! exports provided: Widget */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Widget\", function() { return Widget; });\n/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utils */ \"./lib/utils.js\");\n/* harmony import */ var _event__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./event */ \"./lib/event.js\");\n\n\n\nclass Widget extends _event__WEBPACK_IMPORTED_MODULE_1__[\"Publisher\"] {\n    constructor(options = {}) {\n        let opts = {\n            tag: \"div\",\n            cssClasses: [],\n            style: {}\n        }\n        if (options) {\n            if (options.cssClasses) options.cssClasses = opts.cssClasses.concat(options.cssClasses)\n            Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"update\"])(opts, options)\n\n        }\n\n\n        super()\n\n        this._options = opts\n\n        this.elt = this._options.elt ? this._options.elt : document.createElement(this._options.tag)\n        for(const cls of this._options.cssClasses) this.addCssClass(cls)\n        this.setStyle(this._options.style)\n        this.setPubProxy(this.elt)\n\n        this.child_widgets = []\n\n    }\n\n    renderInto(elt) {\n        Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"clearChildren\"])(elt)\n        this.appendTo(elt)\n    }\n\n    appendTo(elt) {\n        elt.appendChild(this.elt)\n    }\n\n    addCssClass(cls) {\n        this.elt.classList.add(cls)\n    }\n    removeCssClass(cls) {\n        this.elt.classList.remove(cls)\n    }\n\n    setStyle(style) {\n        for(const [prop, val] of Object.entries(style)) {\n            this.elt.style[prop] = val\n        }\n    }\n\n    indexOfChild(w) {\n        for(let i=0; i< this.child_widgets.length; i++) if (w === this.child_widgets[i]) return i\n        return -1\n    }\n\n    appendChild(w) {\n        this.elt.appendChild(w.elt)\n        this.child_widgets.push(w)\n        w.parent = this\n    }\n\n    appendChildren(widgets) {\n        for(const w of widgets) {\n            this.elt.appendChild(w.elt)\n            this.child_widgets.push(w)\n            w.parent = this\n        }\n    }\n\n    clearChildren() {\n        Object(_utils__WEBPACK_IMPORTED_MODULE_0__[\"clearChildren\"])(this.elt)\n        this.child_widgets = []\n    }\n\n}\n\n\n//# sourceURL=webpack:///./lib/widget.js?");

/***/ }),

/***/ "./tmpzh7aq7at.js":
/*!************************!*\
  !*** ./tmpzh7aq7at.js ***!
  \************************/
/*! exports provided: FSItemView, FavItemView, BreadCrumbItemView, LayoutElt, Row, Col, Button, Dialog, FileDialog, FileBrowser */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* WEBPACK VAR INJECTION */(function(global) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"FSItemView\", function() { return FSItemView; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"FavItemView\", function() { return FavItemView; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"BreadCrumbItemView\", function() { return BreadCrumbItemView; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"LayoutElt\", function() { return LayoutElt; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Row\", function() { return Row; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Col\", function() { return Col; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Button\", function() { return Button; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Dialog\", function() { return Dialog; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"FileDialog\", function() { return FileDialog; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"FileBrowser\", function() { return FileBrowser; });\n/* harmony import */ var _lib_widget__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./lib/widget */ \"./lib/widget.js\");\n/* harmony import */ var _lib_models_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./lib/models.js */ \"./lib/models.js\");\n/* harmony import */ var _lib_views__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./lib/views */ \"./lib/views.js\");\n/* harmony import */ var _lib_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./lib/utils */ \"./lib/utils.js\");\nglobal.fs = {\n    'name': '/',\n    'children':[\n        {'name':'dirA','children':[ {'name': 'fileA', children:[]}, {'name': 'fileB', children:[]}] },\n        {'name':'dirB','children':[ {'name': 'fileC', children:[]}, {'name': 'fileD', children:[]}] },\n        {'name':'fileE','children':[]},\n        {'name':'fileF','children':[]},\n    ]\n}\n\n\n\n\n\n\nclass FSItemView extends _lib_views__WEBPACK_IMPORTED_MODULE_2__[\"ModelView\"] {\n    constructor(item) {\n        super(item)\n\n        this._icon_elt = document.createElement('span')\n        this._title_elt = document.createElement('span')\n        this._icon_elt.classList.add('item-icon')\n        this._title_elt.classList.add('item-title')\n\n        this.elt.appendChild(this._icon_elt)\n        this.elt.appendChild(this._title_elt)\n\n        this.registerEvent('activated', {bubbles:true})\n        this.sub(this, 'click', () => this.pub('activated', this.model))\n\n        this._update()\n\n    }\n\n    _update() {\n\n        var self = this\n\n        if ( this.isDir() ) {\n            this.addCssClass('directory')\n            this.removeCssClass('file')\n        } else {\n            this.addCssClass('file')\n            this.removeCssClass('directory')\n        }\n\n        if (this.model.data.preview) {\n            this._icon_elt.style.backgroundImage = item.data.preview\n            this._icon_elt.classList.add('preview')\n        } else {\n            this._icon_elt.classList.remove('preview')\n            this._icon_elt.style.backgroundImage = null;\n        }\n\n        this._title_elt.innerText = this.model.id\n\n    }\n\n    isDir() { return this.model.children.length > 0 }\n}\n\nclass FavItemView extends _lib_views__WEBPACK_IMPORTED_MODULE_2__[\"ModelView\"] {\n    constructor(item) {\n        super(item, {tag:\"li\"})\n        this.registerEvent('activated', {bubbles:true})\n        this._update()\n    }\n    _update() {\n        this.elt.innerText = this.model.data.title\n        if (this.model.data.target) {\n            this.addCssClass('w-link')\n            this.sub(this, 'click', (evt) => this.pub('activated', this.model.data))\n        } else {\n            this.unsub(this)\n            this.removeCssClass('w-link')\n        }\n    }\n}\n\n\nclass BreadCrumbItemView extends _lib_views__WEBPACK_IMPORTED_MODULE_2__[\"ModelView\"] {\n    constructor(item) {\n        super(item, {tag:\"a\", cssClasses:[\"breadcrumb\"]})\n        this.registerEvent('activated', {bubbles:true})\n        this._update()\n    }\n    _update() {\n//         this.elt.innerText = this.model.data\n        this.elt.innerHTML = '<span class=\"w-separator\"></span>'+this.model.data\n        this.addCssClass('w-link')\n        this.sub(this, 'click', (evt) => this.pub('activated', this._path()))\n    }\n    _path() {\n        let pos = this.parent.indexOfChild(this)\n        return this.parent.model.data.slice(0,pos+1).map((md) => md.data)\n    }\n}\n\n\n// var dlg = new Layout('horizontal', [favs, Layout('vertical', controls, breadcrumbs, grid)])\n//\n//\n//\n\n\nclass LayoutElt extends _lib_widget__WEBPACK_IMPORTED_MODULE_0__[\"Widget\"] {\n    constructor(direction, widgets, options) {\n        let opts = {\n            cssClasses: ['w-layout'],\n            style: {\n                display: 'flex',\n                flexDirection: direction\n            }\n        }\n        if (options) {\n            if (options.cssClasses) options.cssClasses = opts.cssClasses.concat(options.cssClasses)\n            Object(_lib_utils__WEBPACK_IMPORTED_MODULE_3__[\"update\"])(opts, options)\n        }\n        console.log(\"OPTIONS\", options)\n        console.log(\"OPTS:\", opts)\n\n        super(opts)\n\n        this._direction = direction\n\n        for(let ch of widgets) {\n            this.appendChild(ch)\n        }\n    }\n\n    get direction() {return this._direction}\n    set direction(dir) {this._direction=dir; this.elt.style.flexDirection=this._direction}\n}\n\nclass Row extends LayoutElt {\n    constructor(widgets, options) {super('row', widgets, options)}\n}\n\nclass Col extends LayoutElt {\n    constructor(widgets, options) {super('col', widgets, options)}\n}\n\nclass Button extends _lib_widget__WEBPACK_IMPORTED_MODULE_0__[\"Widget\"] {\n    constructor(clickHandler) {\n        super({tag:'button'})\n        this.addEventListener('click', clickHandler)\n    }\n}\n\nclass Dialog extends _lib_widget__WEBPACK_IMPORTED_MODULE_0__[\"Widget\"] {\n    constructor() {\n        super()\n\n        this.setStyle({\n            position:'absolute',\n            top:'50%',\n            margin:'auto'\n        })\n        this.addCssClass('w-dialog')\n        document.addEventListener('keyup', this.onKeyUp)\n\n    }\n\n    onKeyUp(evt) {\n        switch( evt.keyCode) {\n            case 27: this.reject('cancel'); break;\n        }\n    }\n\n    resolve(val) {\n        this.hide()\n        this._resolve(val)\n        this._resolve = undefined\n        this._reject = undefined\n    }\n\n    reject(val) {\n        this.hide()\n        this._reject(val)\n        this._reject = undefined\n        this._resolve = undefined\n    }\n\n    exec() {\n        var self = this;\n        return new Promise(function (resolve, reject) {\n            self.show();\n            self._resolve = resolve;\n            self._reject = reject;\n        })\n    }\n\n    show() {this.setStyle({display:''})}\n    hide() {this.setStyle({display:'none'})}\n}\n\nclass FileDialog extends Dialog {\n    constructor(favs, fsModel) {\n        super()\n        var self = this;\n        this._history = {}\n        this._favs = favs\n\n\n        var upBtn = new Button(),\n            uploadBtn = new Button(),\n            createDirBtn = new Button()\n\n        this.favs = new _lib_views__WEBPACK_IMPORTED_MODULE_2__[\"ListView\"](favs, FavItem, {cssClasses:['favs']});\n        this.content = new _lib_views__WEBPACK_IMPORTED_MODULE_2__[\"GridView\"](fsModel, FSItemView, {cssClasses:['content']});\n        this.toolbar = new Row([upBtn, uploadBtn, createDirBtn], {cssClasses:['toolbar']})\n        this.layout = new Row([this.favs, new Col([\n                                                    this.toolbar,\n                                                    this.locationbar,\n                                                    this.content\n        ])])\n\n        upBtn.addEventListener('click', ()=>{self.content.navigateUp()})\n        this.content.addEventListener('itemSelect', function(ev){self.resolve(ev.data)})\n    }\n}\n\n\nclass FileBrowser extends LayoutElt {\n    constructor(favModel, fsModel) {\n        super('row', [], {cssClasses:['w-file-browser']})\n        this._history = []\n        this._pathModel = new _lib_models_js__WEBPACK_IMPORTED_MODULE_1__[\"Model\"](fsModel.path.map(id => new _lib_models_js__WEBPACK_IMPORTED_MODULE_1__[\"Model\"](id)))\n\n        this._breadCrumb = new _lib_views__WEBPACK_IMPORTED_MODULE_2__[\"ListView\"](this._pathModel, BreadCrumbItemView, {cssClasses:['w-breadcrumb']})\n        this._favView = new _lib_views__WEBPACK_IMPORTED_MODULE_2__[\"ListView\"](favModel, FavItemView, {cssClasses:['w-favs'], style:{flexGrow:1}})\n        this._content = new _lib_views__WEBPACK_IMPORTED_MODULE_2__[\"GridView\"](fsModel, FSItemView, {cssClasses:['w-content']})\n\n        this._favView.sub(this, 'activated', (ev) => this._content.navigateTo(fsModel.subTreeAt(ev.data.target)))\n        this._content.sub(this, 'navigate', (ev) => this._history.push(ev.data.path))\n        this._content.sub(this, 'navigate', (ev) => this._pathModel.data = ev.data.path.map(id => new _lib_models_js__WEBPACK_IMPORTED_MODULE_1__[\"Model\"](id)) || this._pathModel.notifyChange())\n        this._breadCrumb.sub(this, 'activated', (ev) => this._content.navigateTo(fsModel.subTreeAt(ev.data)))\n        this.appendChildren([this._favView, new LayoutElt('row',[this._breadCrumb, this._content], {style:{flexGrow:5, flexWrap:\"wrap\"}})])\n    }\n\n    back() {\n        this._content.navigateTo(fsModel.subTreeAt(this._history.pop()))\n    }\n}\n\n\nvar test_elt = document.getElementById('test')\n\n\nglobal.fsModel = _lib_models_js__WEBPACK_IMPORTED_MODULE_1__[\"TreeModel\"].fromObj(fs, 'name', 'children')\nglobal.favModel = new _lib_models_js__WEBPACK_IMPORTED_MODULE_1__[\"Model\"]([new _lib_models_js__WEBPACK_IMPORTED_MODULE_1__[\"Model\"]({title:'Directory A', target:['/', 'dirA']})])\nglobal.fb = new FileBrowser(global.favModel, global.fsModel);\nglobal.fg = new _lib_views__WEBPACK_IMPORTED_MODULE_2__[\"GridView\"](global.fsModel, FSItemView)\nglobal.fg.addCssClass('w-file-browser')\n// global.fg.renderInto(test_elt)\n// global.TreeModel = TreeModel\nglobal.fb.renderInto(test_elt)\n\n\n\n\n\n/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../../.npm-packages/lib/node_modules/webpack/buildin/global.js */ \"../../../../../../../.npm-packages/lib/node_modules/webpack/buildin/global.js\")))\n\n//# sourceURL=webpack:///./tmpzh7aq7at.js?");

/***/ })

/******/ });