window.liveedit.module('api',['util'],function(util, _export) {

var self = this;

var url_for_model = function(model_name, pk) {
    if (pk===undefined) return this.cfg.base_url+'liveedit/rest-api/'+model_name+'/';
    else return self.cfg.base_url+'liveedit/rest-api/'+model_name+'/'+pk+'/';
}

var save = function(model_name, model_pk, field_name, field_value, options) {
    var data = {}
    var contentType = undefined;
    util.log("Saving", field_value, typeof(field_value));
    if (options == undefined) options = {};
    if ( field_value instanceof File) {
        data = new FormData();
        data.append(field_name, field_value)
        data.append("options", JSON.stringify(options));
    } else {
        data[field_name] = field_value;
        data['options'] = options;
        data = JSON.stringify(data);
        contentType = "application/json";
    }
    return new util.Ajax({
        url:url_for_model(model_name, model_pk),
        data: data,
        contentType: contentType,
        method:'PATCH'
    })
}

_export('save',save);

});
