window.liveedit.module('ui.widgets',['util', 'ui'],function(util, ui, _export) {

/**
 * An abstract widget to edit some value.
 * @constructor
 * @param {string} value   - The content to be edited.
 * @param {object} options - Options (e.g. datetime format, etc)
 *                           These options are specified as the optional
 *                           options parameter to the live_edit template tag.
 *
 * The widget should communicate with the outside world
 * by emitting events on its DOMElement:
 *
 *   {CustomEvent} cancel - the widget should emit this event when the
 *                          user cancels editing (discarding current edits)
 *   {CustomEvent} save   - the widget should emit this event when the
 *                          user cancels wants to save his edits. The event's
 *                          detail should be the value that is to be saved.
 *
 * The outside communicates with the widget by calling its methods.
 */
var Widget = function(value, options) {
    var self = this;

    self.options = options;
    self.original_value = value;


    /**
     * The DOMElement of the widget
     *
     * Shouldn't be inserted or removed from the document.
     * This is done by the system.
     */
    self.elt = undefined;

    /**
     * This method is called when the user wants to save
     * the contents (e.g. when he clicks an associated save button)
     */
    self.save = undefined;

    /**
     * This method is called after everything has been setup
     * (e.g. the widget's DOMElement is now inserted into the document)
     *
     * Use it to e.g., to open a popup, etc.
     */
    self.onStartEdit = undefined;
    /**
     * This method is called when the user canceled their edits
     * (via some means external to the widget)
     */
    self.onCancelEdit = function() {},

    /**
     * This method is called whenever saving has progressed .
     *
     * @param {percent}  The approximate percentage of the saving action which is done;
     */
    self.onProgress = function(percent) {},

    /**
     * This method is called when there was an error saving
     * the data to the server.
     *
     * @param {jXHR} response - A response object having at least a data & status property
     */
    self.onSaveError = function(response) {
        self.elt.style.color='red';
        self.elt.disabled=false;
    },
    /**
     * This method is called when the edits were safely saved
     * to the server
     *
     * @param {jXHR} response - A response object having at least a data & status property
     */
    self.onSaveOk = function(response) {
        self.elt.disabled=false;
    },
    /**
     * This method is called just before the widget is destroyed.
     * (note that its element is still in the DOM at self.point)
     * @destructor
     */
    self.onDestroy = function() {},

    /**
     * This method is called with a dom element, a value, and optional parameters.
     * It should render the value into the dom element.
     * @function displayValue
     * @static
     * @param {DOMElement} display_elt - The element to display the value in
     * @param {string}     value       - The value to display
     * @param {object}     response    - The response with which the value came
     */
    self.displayValue = function(display_elt, value, response) {
        display_elt.innerHTML = value;
    }
    /**
     * A helper method to emit an event on the widget's dom element.
     * @function emit
     * @param {string} event_name - the name of the event
     * @param {any}    data       - the data to associate with the event
     */
    self.emit = function(event_name, data) {
        var event = new CustomEvent(event_name, {'detail':data});
        self.elt.dispatchEvent(event);
    }
}

/**
 * A general widget to edit text via a textinput form field.
 * @constructor
 * @extends Widget
 * @param {string} value   - The content to be edited.
 * @param {object} options - The options passed to the live_edit tag
 */
var TextWidget = util.extend(Widget, function(value, options) {

    var self = this;
    var original_value = value;
    var w = document.createElement('input');
    w.class = 'lve-text-widget';
    w.type = 'text';
    w.value = original_value;
    self.save = function() {
        self.elt.disabled=true;
        self.emit('save',self.elt.value);
    };
    if ( ! options.have_controls ) {
        w.onblur = self.save;
    }
    this.elt = w;
});

/**
 * A widget to edit ArrayFields of chars via a textinput form field.
 * Elements are comma-separated.
 * @constructor
 * @extends Widget
 * @param {string} value   - The content to be edited.
 * @param {object} options - The options passed to the live_edit tag
 */
var StrArrayWidget = util.extend(Widget, function(value, options) {
    var value_to_array = function(val) {
        if (val == 'None') return [];
        if (val instanceof Array) return val;
        if (typeof val == "string") {
            if (val.indexOf('[') > -1) {
                return (JSON.parse(val.replace(/'/g,'"')));
            } else {
                return val.split(',');
            }
        }
        return [];
    }
    var self = this;
    var original_value = value_to_array(value);
    var w = document.createElement('input');
    w.class = 'lve-array-widget';
    w.type = 'text';
    w.value = original_value.join(', ');
    self.save = function() {
        self.elt.disabled=true;
        console.log("Saving", self.elt.value.split(','));
        self.emit('save',self.elt.value.split(','));
    };
    self.displayValue = function(display_elt, value, response) {
        console.log("Displaying", value);
        display_elt.innerHTML = value_to_array(value).join(', ');
    }
    if ( ! options.have_controls ) {
        w.onblur = self.save;
    }
    this.elt = w;
});


/**
 * A general widget to upload files.
 * @constructor
 * @extends Widget
 * @param {string} value   - The content to be edited.
 * @param {object} options - The options passed to the live_edit tag
 */
var FileWidget = util.extend(Widget, function(value, options) {
    var self = this;
    var original_value = value;
    var accept = options.mimeTypePattern;
    var w = document.createElement('div'),
        progress_indicator = document.createElement('div');
    var files = [];
    w.class = 'lve-file-widget';
    progress_indicator.style.display='none';
    progress_indicator.class = 'lve-file-widget progress-indicator';
    console.log("Initing FileWidget");

    this.elt = w;
    self.onStartEdit = function() {
        console.log("Choose a file");
        var el = document.createElement('input');
        el.type='file';
        el.addEventListener("change", function (e) {
            e.stopPropagation();
            e.preventDefault();
            files = []
            var i
            if (accept) {
                for(i=0;i<el.files.length;i++) {
                    console.log("Checking type "+el.files[i].type);
                    if ( el.files[i].type.match(accept) ) files.push(el.files[i]);
                }
            } else files.push(el.files[0]);
            self.save();
        });
        el.click();
    }
    self.save = function() {
        console.log("Saving file...")
        w.appendChild(progress_indicator);
        self.emit('save',files[0]);
    };
    self.onProgress = function(percent) {
        console.log("Progress:", percent)
        progress_indicator.style.display='';
        progress_indicator.style.width = percent+'%';
    }
    self.displayValue = function(display_elt, value, response) {
        console.log("Displaying", response);
        console.log("Options:", self.options)
        display_elt.innerHTML = "<a href='"+value+"'>"+self.options.link_text+"</a>";
    }
});


/**
 * An widget to edit a datetime value using the flatpickr
 * widget.
 * @constructor
 * @extends Widget
 * @param {string} value   - The datetime to be edited.
 * @param {object} options - The options passed to the live_edit tag
 *                           The options should contain a format field
 *                           which will be used to display the date
 *
 */
var DateTimeWidget = util.extend(Widget, function(value, options) {

    var self = this;
    var w = document.createElement('input');
    w.class = 'lve-widget lve-datetime-widget';
    w.type = 'text';
    var django_to_picker = {
        'G':'H',
        'g':'h',
        's':'S',
    }
    var picker_fmt = self.options.format;
    for( k in django_to_picker ) {
        picker_fmt=picker_fmt.replace(k, django_to_picker[k]);
    }
    var pickr = $(w).flatpickr({
        onClose:function() {self.save();},
        dateFormat:picker_fmt,
        enableTime:true,
        defaultDate:value,
        minDate:'today'
    });
    self.save = function() {
        self.elt.disabled=true;
        self.emit('save', pickr.selectedDates[0]);
    }
    self.elt = w;
    self.onStartEdit = pickr.open;
    self.displayValue = function(display_elt, value, response) {
        display_elt.innerHTML = util.format_date(self.options.format, new Date(value));
    }
    self.onDestroy = function() {
        pickr.input.remove();
    }
});

_export('Widget', Widget);
_export('InputWidget', TextWidget)
_export('DateTimeWidget', DateTimeWidget)
_export('FileWidget', FileWidget)

ui.register_widget(TextWidget,'CharField');
ui.register_widget(DateTimeWidget,'DateTimeField');
ui.register_widget(FileWidget,'FileField');
ui.register_widget(StrArrayWidget,'ArrayField');
ui.register_default_widget(TextWidget);
});
