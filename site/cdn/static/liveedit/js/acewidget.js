window.liveedit.module('ui.widgets.ace',['util', 'ui', 'ui.widgets'],function(util, ui, widgets, _export) {
/**
 * A widget to edit ArrayFields of chars via a textinput form field.
 * Elements are comma-separated.
 * @constructor
 * @extends Widget
 * @param {string} value   - The content to be edited.
 * @param {object} options - The options passed to the live_edit tag
 */
var ACEWidget = util.extend(widgets.Widget, function(value, options) {
    var self = this;

    var original_value = value;
    var w = document.createElement('pre');
    var editor = ace.edit(w);
    editor.getSession().setValue(original_value);
    w.style.height='100vh';
    w.classList.add("lve-ace-widget")

    editor.setTheme("ace/theme/solarized_light");
    editor.session.setMode("ace/mode/django");
    editor.setOptions({
        showGutter: false,
        enableEmmet: true,
        enableBasicAutocompletion: true,
        enableLiveAutocompletion: true,
        enableSnippets: true,
        minLines: 80,
        printMargin: false,
    })

    self.save = function() {
        self.elt.disabled=true;
        console.log("Saving", editor.getSession().getValue());
        self.emit('save', editor.getSession().getValue());
    };

    if ( ! options.have_controls ) {
        w.onblur = self.save;
    }
    this.elt = w;
});

_export('ACEWidget', ACEWidget);
ui.register_widget(ACEWidget,'TextField');

});
