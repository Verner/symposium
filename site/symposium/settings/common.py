""" Misc settings common to dev & prod """
#pylint: disable=wildcard-import,unused-wildcard-import
import os

from .assets import *
from .auth import *
from .apps import *
from .cms import *
from .i18n import *
from .liveedit import *
from .logging import *
from .secrets import *
from .templates import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'f!e2ricsa9nfly8o0vf)uby%em@s#f(q-*#oz8a2j#hs4whlq$'
ALLOWED_HOSTS = []
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


# Application definition
ROOT_URLCONF = 'symposium.urls'

SITE_ID = 1

MIGRATION_MODULES = {

}

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ]
}

FIXTURE_DIRS = (
    os.path.join(BASE_DIR, 'fixtures'),
)

SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'
