""" Internationalization settings """

import os

LANGUAGE_CODE = 'en'

TIME_ZONE = 'Europe/Prague'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LANGUAGES = (
    ('en', 'English'),
    ('cz', 'Czech'),
)

LOCALE_PATHS = (os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))), 'locale/'),)

DATETIME_INPUT_FORMATS = (
    '%Y-%m-%dT%H:%M', '%Y-%m-%dT%H:%M'
)
