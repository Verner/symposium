from .common import *
import os
import random;

#SECRET_KEY = "".join([random.choice("abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)") for i in range(50)])
SECRET_KEY = 'f!e2ricsa9nfly8o0vf)uby%em@s#f(q-*#oz8a2j#hs4whlq$'
DEBUG = False
ALLOWED_HOSTS = ["localhost"]

# The following needs to be disabled since we are not serving
# over https
#CSRF_COOKIE_SECURE = True
#SESSION_COOKIE_SECURE = True


_PROF = os.environ.get('ENABLE_PROFILING','NO')
ENABLE_PROFILING_MIDDLEWARE = False
ENABLE_PROFILING_DECORATOR = False
if _PROF == 'MIDDLEWARE':
    ENABLE_PROFILING_MIDDLEWARE=True
elif _PROF == 'DECORATOR':
    ENABLE_PROFILING_DECORATOR=True
PROFILE_BASE_DIR = os.path.join(BASE_DIR, "../../logs/profiler/")

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = '/symposium/code/site/cdn/media/'
STATIC_ROOT = '/symposium/code/site/cdn/static/'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'symposium_dev',
        'USER': '',
        'HOST': '',
        'PORT': '',
    }
}

CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': 'unix:///var/run/redis/redis.sock',
    },
}
CACHE_MIDDLEWARE_ALIAS = 'default'
CACHE_MIDDLEWARE_SECONDS = 60
CACHE_MIDDLEWARE_KEY_PREFIX = ''


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(BASE_DIR, "../../logs/lc2019.log"),
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'propagate': True,
            'level': 'DEBUG',
        },
        'logika': {
            'handlers': ['file'],
            'level': 'DEBUG',
        },
    }
}

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
DEBUG_PROPAGATE_EXCEPTIONS = True

#tpl_loaders=TEMPLATES[0]['OPTIONS']['loaders']
#TEMPLATES[0]['OPTIONS']['loaders']=[('django.template.loaders.cached.Loader',tpl_loaders)]
LIVEEDIT['config']['base_static_url'] = STATIC_URL+'liveedit/'


