"""Authentication settings """

AUTHENTICATION_BACKENDS = ['django.contrib.auth.backends.ModelBackend', 'simpleauth.backends.pam.PAMBackend']

OAUTH = {
    'dummy': {
        'client_id':'',
        'client_secret':'',
        'auth_url':'',
        'token_url':'',
        'oauth_id_attr':'',
    }
}
