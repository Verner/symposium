""" Static files & assets settings """

import os

DATA_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

STATICFILES_FINDERS = (
    'assets.finders.ManifestFinder',
    'assets.finders.ManifestAppDirsFinder',
)


