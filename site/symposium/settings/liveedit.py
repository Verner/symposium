try:
    from django.urls import reverse_lazy
except:
    from django.core.urlresolvers import reverse_lazy


LIVEEDIT = {
    'config': {
        'media_upload_url': reverse_lazy('simplecms:mediaelement_create', kwargs={'site_prefix':None}),
        'styles': [
            {'name': 'Normal', 'element': 'p', 'attributes': {'class': 'normal'}},
            {'name': 'Heading 1', 'element': 'h1'},
            {'name': 'Heading 2', 'element': 'h2'},
            {'name': 'Page', 'element': 'div', 'attributes': {'class': 'page-block'}},
            {'name': 'Half Block', 'element': 'div', 'attributes': {'class': 'half-width-block'}},
        ],
        'base_url': '/_internal/liveedit/',
    }
}
