"""
WSGI config for symposium project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
from wdb.ext import WdbMiddleware

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "symposium.settings")

application = WdbMiddleware(get_wsgi_application())
