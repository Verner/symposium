""" Url routing for our webapp """
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.urls import include, path
from django.views.i18n import JavaScriptCatalog


import core.urls
import liveedit.urls
#import ..simpleauth.urls
import simplecms.urls

urlpatterns = [
    # REST API
    #path('api/', include(simpleauth.urls.rest_urls)),
    path('api/', include(core.urls.rest_urls)),
    path('_internal/rf-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('_internal/liveedit/', include(liveedit.urls)),

    # Django provided urls
    path('_internal/admin/', admin.site.urls, name='django-admin'),
    path('_internal/jsi18n/', JavaScriptCatalog.as_view(packages=['django.conf']), name='javascript-catalog'),
    path('sitemap.xml', sitemap, {'sitemaps': {}}),

    # Regular APP urls
    #path('', include(simpleauth.urls, namespace='AUTH')),
    path('<slug:conference_run>/', include(core.urls, namespace='core')),
    path('<slug:site_prefix>/', include(simplecms.urls, namespace='simplecms')),
    path('', include(simplecms.urls, namespace='simplecms:default')),
]

# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + \
                  static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + \
                  urlpatterns
