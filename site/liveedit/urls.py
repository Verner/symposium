from django.conf.urls import url, include

from .models import ExposedModel

urlpatterns = [url(r'^liveedit/rest-api/', include(ExposedModel._router.urls))]
