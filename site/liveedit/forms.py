import json

from django.contrib.postgres.fields import ArrayField, JSONField, HStoreField
from django.db import models
from django.template import Template, loader, Context, engines
from django.utils.safestring import mark_safe
from django.template import engines

from . import settings

jinja2_engine = engines.all()[0]


DEFAULT_WIDGET_TEMPLATE = Template("""
    <span class='lve-editable'
          onDblClick='liveedit.ui.startEdit(this)'
          data-model-name='{{model_name}}'
          data-model-pk='{{pk}}'
          data-field-name='{{field_name}}'
          data-field-type='{{field_type}}'
          data-field-value='{{field_value|escape}}'
          data-options='{{ options|escape }}'
          {% if id %}id='{{ id }}' {% endif %}>
      {{ display_value }}
    </span>
""")

WIDGET_TEMPLATES = {
    models.fields.TextField: Template("""
                 <div class='lve-editable'
                     onDblClick='liveedit.ui.startEdit(this)'
                     data-model-name='{{model_name}}'
                     data-model-pk='{{pk}}'
                     data-field-name='{{field_name}}'
                     data-field-type='{{field_type}}'
                     data-field-value='{{field_value|escape}}'
                     data-options='{{ options|escape }}'
                     {% if id %}id='{{ id }}' {% endif %}>
                   {{ display_value }}
                 </div>
                """),
}

CONTROLS_TEMPLATE = Template("""
    {% for name, display in actions %}
        <button class='lve-controls lve-control-{{ name }}' data-target='{{ id }}' data-target-fields='{{ fields }}' data-action='{{ name }}' onClick="liveedit.ui.controlClick(this)"
                {% if name == 'save' or name == 'cancel' %}disabled{% endif %}>
            {{ display | safe }}
        </button>
    {% endfor %}
    <span class='lve-controls lve-control-progress' data-target='{{ id }}' data-target-fields='{{ fields }}' data-action='progress' style='display:none'>{{ progress | safe }}</span>
""")


def render_controls(context, id, show_actions=['edit', 'save', 'cancel'], fields=[]):
    new_ctx = {
        'actions': [(name, settings.CONTROL_DISPLAY_ELEMENTS[name]) for name in show_actions],
        'id': id,
        'progress': settings.CONTROL_DISPLAY_ELEMENTS['progress'],
        'fields': json.dumps(fields),
    }
    try:
        new_ctx.update(context)
    except:
        pass
    return CONTROLS_TEMPLATE.render(Context(new_ctx))


def render_value(context, model_instance, field_name, options, editable=False):
    field_type = type(model_instance._meta.get_field(field_name))
    safe = options.get('safe', False)
    display_tpl = options.get('display_tpl', None)

    if display_tpl is None:
        if field_type == ArrayField:
            display_tpl = '{% for v in value %}{{ v }}{% if not forloop.last %},{% endif %} {% endfor %}'
        else:
            if safe:
                display_tpl = '{{ value | safe }}'
            else:
                display_tpl = '{{ value | escape }}'

    if not hasattr(display_tpl, 'render'):
        display_tpl = jinja2_engine.from_string(display_tpl)
    else:
        del options['display_tpl']
    return mark_safe(display_tpl.render(Context({
        'value': getattr(model_instance, field_name),
        'model_instance': model_instance,
        'field_name': field_name,
        'editable': editable,
        'ctx': context
    })))


def render_editable_field(context, model_instance, field_name, options):
    field_type = type(model_instance._meta.get_field(field_name))
    tpl = WIDGET_TEMPLATES.get(field_type, DEFAULT_WIDGET_TEMPLATE)
    display = render_value(context, model_instance, field_name, options, editable=True)

    id = options.get('id', None)

    return mark_safe(tpl.render(Context({
        'pk': model_instance.id,
        'field_name': field_name,
        'field_value': getattr(model_instance, field_name),
        'display_value': display,
        'model_name': model_instance._meta.label_lower,
        'field_type': field_type.__name__,
        'options': json.dumps(options),
        'id': id,
    })))
