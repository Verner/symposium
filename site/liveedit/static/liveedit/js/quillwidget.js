window.liveedit.module('ui.widgets.quill',['util', 'ui', 'ui.widgets'],function(util, ui, widgets, _export) {

var Delta = Quill.import("delta");
let SmartBreak = Quill.import("blots/break");
let Embed = Quill.import("blots/embed");

function lineBreakMatcher() {
    var newDelta = new Delta();
    newDelta.insert({ break: "" });
    return newDelta;
}

function shiftEnterHandler(range) {
    let currentLeaf = this.quill.getLeaf(range.index)[0];
    let nextLeaf = this.quill.getLeaf(range.index + 1)[0];

    this.quill.insertEmbed(range.index, "break", true, "user");

    // Insert a second break if:
    // At the end of the editor, OR next leaf has a different parent (<p>)
    if (nextLeaf === null || currentLeaf.parent !== nextLeaf.parent) {
    this.quill.insertEmbed(range.index, "break", true, "user");
    }

    // Now that we've inserted a line break, move the cursor forward
    this.quill.setSelection(range.index + 1, Quill.sources.SILENT);
}

SmartBreak.prototype.length = function(){return 1;}
SmartBreak.prototype.value = function() {return "\n";}
SmartBreak.prototype.insertInto = function(parent, ref) {
    Embed.prototype.insertInto.call(this, parent, ref);
}
SmartBreak.blotName = "break";
SmartBreak.tagName = "BR";

Quill.register(SmartBreak);

var quill_toolbar_html = ""+
"<!-- Toolbar container -->                                                      "+
"  <!-- Add font size dropdown -->                                               "+
"  <select class='ql-header'>                                                      "+
"    <option selected></option>                                                  "+
"    <option value='1'></option>                                             "+
"    <option value='2'></option>                                             "+
"    <option value='3'></option>                                              "+
"  </select>                                                                     "+
"  <!-- Add a bold button -->                                                    "+
"  <button class='ql-bold'></button>                                             "+
"  <button class='ql-italic'></button>                                           "+
"  <!-- Add list buttons -->                                                     "+
"  <button class='ql-list' value='bullet'></button>                              "+
"";



/**
 * A Quill.js based RichTextField widget.
 * @constructor
 * @extends Widget
 * @param {string} value   - The content to be edited.
 * @param {object} options - The options passed to the live_edit tag
 */
var QuillWidget = util.extend(widgets.Widget, function(value, options) {

    var self = this;
    var original_value = value;
    var w = document.createElement('div');
    var quill_container = document.createElement('div');
    var quill_toolbar = document.createElement('div');
    quill_toolbar.innerHTML = quill_toolbar_html;
    quill_toolbar.style.height='40px';

    w.class = 'lve-quill-widget';
    w.appendChild(quill_toolbar);
    w.appendChild(quill_container);

    var quill_options = {
        theme: "snow",
        modules: {
            clipboard: {
                matchers: [["BR", lineBreakMatcher]]
            },
            keyboard: {
                bindings: {
                    linebreak: {
                        key: 13,
                        shiftKey: true,
                        handler: shiftEnterHandler,
                    }
                }
            },
            toolbar: quill_toolbar,
        }
    };
    var quill_editor = new Quill(quill_container, quill_options);
    quill_editor.clipboard.dangerouslyPasteHTML(original_value);
    self.save = function() {
        self.emit('save', quill_editor.root.innerHTML);
    }
    this.elt = w;
});

_export('QuillWidget', QuillWidget);
ui.register_widget(QuillWidget,'TextField');

});
