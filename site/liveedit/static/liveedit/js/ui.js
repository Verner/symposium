window.liveedit.module('ui', ['util', 'api'], function(util, api, _export) {

util.load_defaults({
    interface: {
        buttons: {
            save: "<span class='icon fa fa-save'></span>",
            cancel: "<span class='icon fa fa-times'></span>",
        }
    }
});

var widgets = {}
var DefaultWidget = null;
var register_widget = function(widget, field_type) {
    widgets[field_type]=widget;
}
var register_default_widget = function(widget) {
    DefaultWidget = widget;
}

var create_editor_widget = function(display_elt, field_type, options) {
    if ( options.widget && widgets[options.widget] !== undefined ) {
        w = new widgets[options.widget](display_elt.dataset.fieldValue, options);
    } else if(widgets[field_type]===undefined) {
        w = new DefaultWidget(display_elt.dataset.fieldValue, options);
    } else {
        w = new widgets[field_type](display_elt.dataset.fieldValue, options);
    }
    return w;
}

var editors = new WeakMap();

var callSave = function(display_elt) {
    var editor = editors.get(display_elt);
    editor.save();
}

var save = function(display_elt, model_name, model_pk, field_name, field_value) {
    var data = {}
    var editor = editors.get(display_elt);
    var contentType = undefined;

    api.save(model_name, model_pk, field_name, field_value, editor.options).then(function(response){
        updateControls(display_elt.id,'progress_hide');
        display_elt.dataset.fieldValue = response.data[field_name];
        editor.displayValue(display_elt, response.data[field_name], response);
        if (editor.onSaveOk) editor.onSaveOk(response);
        stopEdit(display_elt, editor);
    }).catch(function(response){
        updateControls(display_elt.id, 'progress_hide');
        if (editor.onSaveError) editor.onSaveError(response);
    }).progress(function(percent){
        updateControls(display_elt.id, 'progress', percent);
        if (editor.onProgress) editor.onProgress(percent);
    });
    updateControls(display_elt.id,'saving');
}

var stopEdit = function(display_elt) {
    var editor = editors.get(display_elt);
    if (editor.onDestroy) editor.onDestroy();
    try {
        display_elt.parentNode.removeChild(editor.elt);
    } catch (e) {}
    display_elt.style.display='';
    updateControls(display_elt.id, 'cancel');
}


var startEdit = function(display_elt, have_controls) {
    var options = JSON.parse(display_elt.dataset.options);
    options.have_controls = have_controls;
    var editor = create_editor_widget(display_elt, display_elt.dataset.fieldType, options);
    display_elt.insertAdjacentElement('afterend', editor.elt);
    editor.elt.addEventListener('save', function(e) {
        save(display_elt, display_elt.dataset.modelName, display_elt.dataset.modelPk, display_elt.dataset.fieldName, e.detail);
    });
    editor.elt.addEventListener('cancel', function(e) {
        stopEdit(display_elt, editor)
    });
    display_elt.style.display='none';
    if (editor.onStartEdit) {
        editor.onStartEdit();
    }
    editor.elt.focus();
    editors.set(display_elt, editor);

    updateControls(display_elt.id, 'edit');
}

var updateControls = function(target_id, action, percent_done) {
    var updateSingleControl = function(elt) {
        switch(elt.dataset.action) {
            case 'edit':
                if (action == 'cancel' || action == 'save') elt.disabled=false;
                else elt.disabled=true;
                break;
            case 'cancel':
            case 'save':
                if (action == 'edit') elt.disabled=false;
                else elt.disabled=true;
                break;
            case 'progress':
                if (action == 'saving') elt.display = '';
                else if (action == 'progress') elt.style.width = percent_done+'%';
                else elt.display = 'none';
                break;
        }
    }
    document.querySelectorAll('[data-target="'+target_id+'"]').forEach(updateSingleControl);
}

var controlClick = function(control_elt) {
    var fields = JSON.parse(control_elt.dataset.targetFields);
    var target_elts = [];
    if (fields.length > 0) {
        fields.forEach(function(field_name) {
            document.querySelectorAll("[id^='"+control_elt.dataset.target+"."+field_name+"']").forEach(function(elt) {
                target_elts.push(elt);
            });
        });
    } else {
        target_elts = document.querySelectorAll("[id^='"+control_elt.dataset.target+"']");
    }

    switch (control_elt.dataset.action) {
        case 'edit':
            target_elts.forEach(function (elt) {
                startEdit(elt, true);
            })
            break;
        case 'cancel':
            target_elts.forEach(function (elt) {
                stopEdit(elt);
            })
            break;
        case 'save':
            target_elts.forEach(function (elt) {
                callSave(elt);
            })
            break;
    }
    updateControls(control_elt.dataset.target, control_elt.dataset.action);
}

var createControlButton = function(name) {
    var button = document.createElement('button');
    button.innerHTML = this.cfg.interface.buttons[name];
    return button;
}

_export('controlClick', controlClick);
_export('startEdit', startEdit);
_export('register_widget', register_widget);
_export('register_default_widget', register_default_widget);


});
