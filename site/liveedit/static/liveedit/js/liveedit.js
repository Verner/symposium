var liveedit = (function() {
    var finished_modules = {};
    var waiting_modules = [];

    var resolve_deps = function(depends) {
        var ready = [];
        var waiting = [];
        for(i=0;i<depends.length;i++) {
            if (finished_modules[depends[i]]) {
                ready.push(finished_modules[depends[i]]);
            } else {
                waiting.push(depends[i]);
            }
        }
        return { ready:ready, waiting:waiting }
    }

    var find_ready = function() {
      var i, deps;
      for(i=0;i<waiting_modules.length;i++) {
        deps = resolve_deps(waiting_modules['depends']);
        if (deps.waiting.length == 0) {
            return {
              mod:waiting_modules[i],
              deps: deps.ready
            }
        }
      }
      return false;
    }

    var load_module = function(deps, constructor) {
        var mod = {'cfg':lve.cfg};
        var _export = function(name, method) {mod[name]=method;}
        deps.push(_export);
        constructor.apply(mod, deps);
        return mod;
    }

    var _export_module = function(name, mod) {
        finished_modules[name] = mod;
        var obj = lve;
        var path = name.split('.');
        for(i=0;i<path.length-1;i++) {
            if (! obj[path[i]]) obj[path[i]] = {};
            obj = obj[path[i]];
        }
        obj[path[path.length-1]]=mod;
    }

    var lve = {
        module: function(name, depends, constructor) {
            var deps = resolve_deps(depends);
            if (deps.waiting.length > 0) {
                waiting_modules.push({'depends':depends, 'constructor':constructor, 'name':name});
            } else _export_module(name, load_module(deps.ready, constructor));
            var item = find_ready();
            while(item) {
                _export_module(item.mod.name, load_module(item.mod.depends, item.mod.constructor));
                item = find_ready();
            }

        },
        cfg: {
          base_url:'/'
        }
    }
    return lve;
})();
