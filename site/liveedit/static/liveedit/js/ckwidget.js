window.liveedit.module('ui.widgets.ck',['util', 'ui', 'ui.widgets'],function(util, ui, widgets, _export) {

var mod = this;

/**
 * A Quill.js based RichTextField widget.
 * @constructor
 * @extends Widget
 * @param {string} value   - The content to be edited.
 * @param {object} options - The options passed to the live_edit tag
 */
var CKWidget = util.extend(widgets.Widget, function(value, options) {

    var self = this;
    var original_value = value;
    var w = document.createElement('div');
    var ck_content = document.createElement('div');
    var ck_toolbar = document.createElement('div');
    ck_toolbar.style.height='40px';
    ck_toolbar.style['margin-bottom']='1em'
    ck_content.setAttribute('contenteditable', true);
    ck_content.innerHTML = value;
    w.classList = 'lve-ck-widget';
    w.appendChild(ck_toolbar);
    w.appendChild(ck_content);

    set_CK_config();

    var ck_editor = CKEDITOR.inline(ck_content, {
        extraAllowedContent: '',
		removePlugins: 'floatingspace,maximize,resize',
		extraPlugins: 'sharedspace,sourcedialog,codemirror,image2,uploadimage,colorbutton,insertBlockPlugin',
        startupFocus: true,
        sharedSpaces: {
            top: ck_toolbar,
        },
    });

    ck_editor.on('fileUploadRequest', function( evt ) {
        util.setCSRF(evt.data.fileLoader.xhr);
    });

    self.save = function() {
        self.emit('save', ck_content.innerHTML);
    }
    this.elt = w;

    self.onDestroy = function() {
        ck_editor.destroy();
        MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
    }
});

function set_CK_config() {
    var config = CKEDITOR.config;
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
        { name: 'styles' },
        { name: 'basicstyles', groups: [ 'basicstyles'] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
		{ name: 'links' },
        { name: 'colors' },
		{ name: 'insert' },
        { name: 'document', groups: ['mode']},
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript,Format';

	config.format_tags = 'p;h1;h2;h3;pre';
    config.colorButton_colors = '#ffc600';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';
    config.mathJaxLib = '//cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS_HTML';
    config.htmlEncodeOutput = false;
    config.entities = false;
    config.uploadUrl = mod.cfg.media_upload_url;
    try {
        console.log(mod.cfg.styles)
        CKEDITOR.stylesSet.add('custom_styles', mod.cfg.styles )
        config.stylesSet = 'custom_styles';
    } catch (e) {}
    config.contentBlocks = [
        {'label': 'Container', "description": "A simple container which can hold other elements.", "html":"<div>container content</div>"},
        {'label': 'Half Sized', "description": "A container which takes up half of the width of its parent.", "html":"<div class='half-width-block'>container content</div>"},
        {'label': 'Page', "description": "A container which takes up the full height of the browser window.", "html":"<div class='page-block'>page content</div>"},
        {'label': 'Heading', "description": "Heading", "html":"<h1>Heading</h1>"}
    ];
};

function register_plugins() {
    console.log("Registering plugins");

CKEDITOR.plugins.addExternal( 'codemirror', mod.cfg.base_static_url+'bower_components/ckeditor-codemirror/codemirror/' );
CKEDITOR.plugins.add( 'insertBlockPlugin', {
	requires: 'dialog',
	icons: 'specialchar',
	hidpi: true,
	init: function( editor_obj ) {
        console.log("Insert Block Init")
		var pluginName = 'insertBlockPlugin',
			plugin = this;

        CKEDITOR.dialog.add( 'insertblockDialog', function( editor ) {
            var dialog;

            var onChoice = function( evt ) {
                    var target = evt.data ? evt.data.getTarget() : new CKEDITOR.dom.element( evt );
                    var blockId = target.$.dataset.blockId;
                    if ( blockId !== undefined ) {
                        dialog.hide();
                        var element = CKEDITOR.dom.element.createFromHtml( editor.config.contentBlocks[blockId].html );
                        editor.insertElement( element );
                    }
                };

            var onClick = CKEDITOR.tools.addFunction( onChoice );
            var onFocus = function( evt ) {};
            var onBlur = function( evt ) {};

            return {
                title: "Insert Content Block",
                minWidth: 430,
                minHeight: 280,
                buttons: [ CKEDITOR.dialog.cancelButton ],
                charColumns: 17,
                onLoad: function() {
                    var blocks = editor.config.contentBlocks;

                    var charsTableLabel = CKEDITOR.tools.getNextId() + '_specialchar_table_label';
                    var html = [ '<ul role="listbox" aria-labelledby="' + charsTableLabel + '"' +
                        ' style="width: 320px; height: 100%;">'];

                    for(const [index, block] of blocks.entries()) {
                        html.push( '<li data-block-id="'+index+'" onclick="CKEDITOR.tools.callFunction('+onClick+',this); return false;">'+block.label+' <span class="desc">'+block.description+'</li>' );
                    }

                    html.push('</ul>')

                    html.push( '<span id="' + charsTableLabel + '" class="cke_voice_label">' + "table lable" + '</span>' );

                    this.getContentElement( 'info', 'charContainer' ).getElement().setHtml( html.join( '' ) );
                },
                contents: [ {
                    id: 'info',
                    label: "General",
                    title: "General",
                    padding: 0,
                    align: 'top',
                    elements: [ {
                        type: 'hbox',
                        align: 'top',
                        widths: [ '320px', '90px' ],
                        children: [ {
                            type: 'html',
                            id: 'charContainer',
                            html: '',
                            onMouseover: onFocus,
                            onMouseout: onBlur,
                            focus: function() {},
                            onShow: function() {},
                            onLoad: function( event ) {
                                dialog = event.sender;
                            }
                        },
                        {
                            type: 'hbox',
                            align: 'top',
                            widths: [ '100%' ],
                            children: [ {
                                type: 'vbox',
                                align: 'top',
                                children: [
                                    {
                                        type: 'html',
                                        html: '<div></div>'
                                    },
                                    {
                                        type: 'html',
                                        id: 'blockInfo',
                                        className: 'cke_dark_background',
                                        style: 'border:1px solid #eeeeee;font-size:28px;height:40px;width:70px;padding-top:9px;font-family:\'Microsoft Sans Serif\',Arial,Helvetica,Verdana;text-align:center;',
                                        html: '<div>&nbsp;</div>'
                                    },
                                    {
                                        type: 'html',
                                        id: 'htmlPreview',
                                        className: 'cke_dark_background',
                                        style: 'border:1px solid #eeeeee;font-size:14px;height:20px;width:70px;padding-top:2px;font-family:\'Microsoft Sans Serif\',Arial,Helvetica,Verdana;text-align:center;',
                                        html: '<div>&nbsp;</div>'
                                    }
                                ]
                            } ]
                        } ]
                    } ]
                } ]
            };
        } );

		editor_obj.addCommand( pluginName, {
			exec: function() {
                editor_obj.openDialog('insertblockDialog');
			},
			modes: { wysiwyg: 1 },
			canUndo: false
		} );

		// Register the toolbar button.
		editor_obj.ui.addButton && editor_obj.ui.addButton( 'InsertBlock', {
			label: "Insert Block",
			command: pluginName,
			toolbar: 'insert,50'
		} );
	}
} );

}

register_plugins();

_export('CKWidget', CKWidget);
ui.register_widget(CKWidget,'TextField');

});

