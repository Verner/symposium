from django.apps import AppConfig


class LiveeditConfig(AppConfig):
    name = 'liveedit'
