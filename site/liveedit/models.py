import json

from django.template import Template, loader, Context
from rest_framework import routers, serializers, viewsets
from rest_framework.permissions import DjangoModelPermissions


def register_model(model_class, instance_templates=None, expose_fields=None):
    return ExposedModel(model_class=model_class, instance_templates=instance_templates, expose_fields=expose_fields)


def is_registered(instance):
    return type(instance).__name__ in ExposedModel._models


class ExposedModel:
    _models = {}
    _router = routers.DefaultRouter()

    def __init__(self, model_class, instance_templates=None, expose_fields=None):
        default_template = Template('{{ object }}')
        templates = {}
        if instance_templates is not None:
            for (key, tpl_path) in instance_templates.items():
                templates[key] = loader.get_template(tpl_path)
        if expose_fields is None:
            class ModelSerializer(serializers.ModelSerializer):
                html_representation = serializers.SerializerMethodField()

                def get_html_representation(self, obj):
                    request = self.context['request']
                    options = request.data.get('options', {})
                    if type(options) == str:
                        try:
                            options = json.loads(options)
                        except:
                            options = {}
                    key = options.get('template', None)
                    tpl = templates.get(key, default_template)
                    return tpl.render(Context({'object': obj}))

                class Meta:
                    model = model_class
                    fields = '__all__'
                    depth = 0
        else:
            class ModelSerializer(serializers.ModelSerializer):
                html_representation = serializers.SerializerMethodField()

                def get_html_representation(self, obj):
                    request = self.context['request']
                    options = request.data.get('options', {})
                    key = options.get('template', None)
                    tpl = templates.get(key, default_template)
                    return tpl.render(Context({'object': obj}))

                class Meta:
                    model = model_class
                    fields = expose_fields
                    depth = 0

        class ModelViewSet(viewsets.ModelViewSet):
            queryset = model_class.objects.all()
            serializer_class = ModelSerializer
            permission_classes = (DjangoModelPermissions,)

        self._view_set = ModelViewSet
        ExposedModel._router.register(model_class._meta.label_lower, ModelViewSet)
        ExposedModel._models[model_class.__name__] = self
