from django.conf import settings

CONTROL_DISPLAY_ELEMENTS = {
    'edit': '<i class="material-icons">create</i><span class="fa fa-pencil"></span> <span class="control-label">Edit</span>',
    'save': '<i class="material-icons">save</i><span class="fa fa-floppy-o"></span> <span class="control-label">Save</span>',
    'cancel': '<i class="material-icons">cancel</i><span class="fa fa-times"></span> <span class="control-label">Cancel</span>',
    'new': '<i class="material-icons">note_add</i><span class="fa file-text-o"></span> <span class="control-label">New</span>',
    'delete': '<i class="material-icons">delete</i><span class="fa fa-trash-o"></span> <span class="control-label">Delete</span>',
    'progress': '<span class="fa fa-spinner fa-pulse"></span>',
}


def update_settings_dict(provided, defaults):
    for key, val in defaults.items():
        if key not in provided:
            provided[key] = val
        elif type(val) == dict:
            update_settings_dict(provided[key], val)


_g = globals()
for key, value in [i for i in _g.items()]:
    if type(value) == dict:
        update_settings_dict(getattr(settings, key, {}), value)
    else:
        _g[key] = getattr(settings, key, value)
