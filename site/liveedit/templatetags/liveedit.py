import json

from django import template
from django.conf import settings
from django.core.serializers import serialize
from django.core.serializers.json import DjangoJSONEncoder
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe

register = template.Library()

from ..forms import render_editable_field, render_controls, render_value
from ..models import is_registered




try:
    from django_jinja import library
    import jinja2
    HAVE_JINJA = True
except:
    HAVE_JINJA = False

class Register:
    @classmethod
    def simple_tag(cls, takes_context=False):

        def decorator(f):
            if HAVE_JINJA:
                f = library.global_function(f)
                if takes_context:
                    f = jinja2.contextfunction(f)
            f = register.simple_tag(takes_context=takes_context)(f)
            return f

        return decorator

    @classmethod
    def filter(cls, name=None, takes_context=False):

        def decorator(f):
            print("Registering", f.__name__, "as", name)
            if HAVE_JINJA:
                if takes_context:
                    f = jinja2.contextfilter(f)
                f = library.filter(f)
            f = register.filter(name=name)(f)
            return f

        return decorator

    @classmethod
    def tag(cls, f):
        return register.tag(f)


def _get_html_id(model_instance, field_name):
    if field_name is None:
        return type(model_instance).__name__+'('+str(model_instance.pk)+')'
    else:
        return type(model_instance).__name__+'('+str(model_instance.pk)+').'+field_name


@Register.simple_tag(takes_context=True)
def live_edit(context, model_instance, field_name, **kwargs):
    if not is_registered(model_instance):
        raise Exception("Model class "+type(model_instance).__name__+" not registered.")
    user = context['request'].user
    permission = model_instance._meta.label_lower.replace('.', '.change_')
    if user.has_perm(permission):
        kwargs['id'] = _get_html_id(model_instance, field_name)
        return render_editable_field(context, model_instance, field_name, options=kwargs)
    else:
        return render_value(context, model_instance, field_name, options=kwargs, editable=False)
        #return mark_safe(getattr(model_instance, field_name))


@Register.simple_tag(takes_context=True)
def live_controls(context, model_instance, field_name=None, **kwargs):
    if not is_registered(model_instance):
        raise Exception("Model class "+type(model_instance).__name__+" not registered.")
    user = context['request'].user
    permission = model_instance._meta.label_lower.replace('.', '.change_')
    if user.has_perm(permission):
        kw = {}
        if 'show_actions' in kwargs:
            kw['show_actions'] = kwargs['show_actions'].split(',')
        if 'fields' in kwargs:
            kw['fields'] = kwargs['fields'].split(',')
        return render_controls(context, _get_html_id(model_instance, field_name), **kw)
    else:
        return ""


class LiveNode(template.Node):
    def __init__(self, template_nodes, model_instance, field_name, **kwargs):
        self.template_nodes = template_nodes
        self.model_instance = model_instance
        self.field_name = field_name
        self.options = kwargs

    def render(self, context):
        try:
            mi = self.model_instance.resolve(context)
            fn = self.field_name.resolve(context)
            opts = {key: val.resolve(context) for key, val in self.options.items()}

            if not is_registered(mi):
                raise Exception("Model class "+type(mi).__name__+" not registered.")
            user = context['user']
            permission = mi._meta.label_lower.replace('.', '.change_')
            if user.has_perm(permission):
                opts['id'] = _get_html_id(mi, fn)
                opts['display_tpl'] = self.template_nodes
                return render_editable_field(context, mi, fn, options=opts)
            else:
                return mark_safe(self.template_nodes.render(template.Context({
                    'value': getattr(mi, fn),
                    'field_name': fn,
                    'model_instance': mi,
                    'editable': False
                })))
        except:
            if context.template.engine.debug:
                raise
            else:
                try:
                    return mark_safe(self.template_nodes.render(template.Context({
                        'value': getattr(mi, fn),
                        'field_name': fn,
                        'model_instance': mi,
                        'editable': False
                    })))
                except:
                    return ""


@Register.tag
def live_edit_tpl(parser, token):
    """
    Usage:
        {% live_edit_tpl model_instance 'field_name' %}
            {{ value }}
            ({{ model_instance }})
        {% endlive_edit_tpl %}
    """
    args = token.contents.split()
    kwargs = {}
    if len(args) < 2:
        raise template.TemplateSyntaxError(_('%s tag requires a model instance and a field name') % args[0])

    if len(args) < 3:
        raise template.TemplateSyntaxError(_('%s tag requires a field name') % args[0])

    for arg in args[3:]:
        name, val = arg.split('=', maxsplit=1)
        kwargs[name] = template.Variable(val)

    args = map(template.Variable, args[1:3])

    template_nodes = parser.parse(('endlive_edit_tpl',))
    parser.delete_first_token()
    return LiveNode(template_nodes, *args, **kwargs)


@Register.simple_tag(takes_context=True)
def liveedit_static(context, asset_type=None):
    if HAVE_JINJA:
        ctx = {
            'asset_type': asset_type,
            'settings': settings.LIVEEDIT
        }
        return jinja2.Markup(render_to_string('liveedit/static.html', ctx))

    ctx = context.flatten()
    ctx['asset_type'] = asset_type
    ctx['settings'] = settings.LIVEEDIT
    return mark_safe(render_to_string('liveedit/static.html', ctx))


@Register.filter(name='json')
def to_json(val):
    return mark_safe(json.dumps(val, cls=DjangoJSONEncoder))



















