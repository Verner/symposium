#!/bin/sh
DOCKER_HOST=$(ip route | awk 'NR==1 {print $3}')

cd $APP_HOME

VENV=`pipenv --venv`

cd $VENV/lib/python3.6/site-packages
patch -p 1 < $APP_HOME/inotify.patch


. $VENV/bin/activate
cd $APP_HOME/code/site

echo "ARGS: $@"
if [ "z$1" == 'zdev' ]; then
    su postgres -c 'pg_ctl start'
    ./manage.py liveserver
else
    nginx
    redis-server /etc/redis.conf &
    su postgres -c 'pg_ctl restart'
    wdb.server.py &
    cat $APP_HOME/initial-fixture.sql | psql symposium_dev
    DJANGO_SETTINGS_MODULE=symposium.settings.dockerdev ./manage.py migrate
    WDB_NO_BROWSER_AUTO_OPEN=True DJANGO_SETTINGS_MODULE=symposium.settings.dockerdev gunicorn --bind unix:$APP_HOME/gunicorn.sock --pid $APP_HOME/gunicorn.pid --access-logfile - --reload --timeout=3600 symposium.wsgi_debug:application
fi;

