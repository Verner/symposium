--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5
-- Dumped by pg_dump version 10.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

ALTER TABLE IF EXISTS ONLY "public"."simplecms_pagestructure" DROP CONSTRAINT IF EXISTS "simplecms_pagestructure_page_id_2fcfddf5_fk_simplecms_page_id";
ALTER TABLE IF EXISTS ONLY "public"."simplecms_pagestructure" DROP CONSTRAINT IF EXISTS "simplecms_pagestruct_fragment_id_9a355861_fk_simplecms";
ALTER TABLE IF EXISTS ONLY "public"."simplecms_page" DROP CONSTRAINT IF EXISTS "simplecms_page_translation_of_id_9407fffc_fk_simplecms_page_id";
ALTER TABLE IF EXISTS ONLY "public"."simplecms_page" DROP CONSTRAINT IF EXISTS "simplecms_page_template_id_1a52e8a8_fk_simplecms";
ALTER TABLE IF EXISTS ONLY "public"."simplecms_page" DROP CONSTRAINT IF EXISTS "simplecms_page_author_id_aeea587b_fk_auth_user_id";
ALTER TABLE IF EXISTS ONLY "public"."simplecms_mediaelement" DROP CONSTRAINT IF EXISTS "simplecms_mediaelement_author_id_adad4022_fk_auth_user_id";
ALTER TABLE IF EXISTS ONLY "public"."registration_registrationform" DROP CONSTRAINT IF EXISTS "registration_registr_conf_id_9a27a5b8_fk_core_conf";
ALTER TABLE IF EXISTS ONLY "public"."registration_participant" DROP CONSTRAINT IF EXISTS "registration_participant_profile_id_4fd75b80_fk_core_profile_id";
ALTER TABLE IF EXISTS ONLY "public"."registration_participant" DROP CONSTRAINT IF EXISTS "registration_partici_conference_id_64d670ca_fk_core_conf";
ALTER TABLE IF EXISTS ONLY "public"."registration_participant" DROP CONSTRAINT IF EXISTS "registration_partici_affiliation_id_604682ab_fk_core_inst";
ALTER TABLE IF EXISTS ONLY "public"."django_admin_log" DROP CONSTRAINT IF EXISTS "django_admin_log_user_id_c564eba6_fk_auth_user_id";
ALTER TABLE IF EXISTS ONLY "public"."django_admin_log" DROP CONSTRAINT IF EXISTS "django_admin_log_content_type_id_c4bce8eb_fk_django_co";
ALTER TABLE IF EXISTS ONLY "public"."core_profile" DROP CONSTRAINT IF EXISTS "core_profile_user_id_bf8ada58_fk_auth_user_id";
ALTER TABLE IF EXISTS ONLY "public"."core_profile" DROP CONSTRAINT IF EXISTS "core_profile_affiliation_id_96e7d696_fk_core_institution_id";
ALTER TABLE IF EXISTS ONLY "public"."core_institution" DROP CONSTRAINT IF EXISTS "core_institution_country_id_ef45100f_fk_countries";
ALTER TABLE IF EXISTS ONLY "public"."core_conferencerun" DROP CONSTRAINT IF EXISTS "core_conferencerun_series_id_d9f99922_fk_core_conf";
ALTER TABLE IF EXISTS ONLY "public"."core_conferencerun_organizers" DROP CONSTRAINT IF EXISTS "core_conferencerun_o_profile_id_ce1815ba_fk_core_prof";
ALTER TABLE IF EXISTS ONLY "public"."core_conferencerun_organizers" DROP CONSTRAINT IF EXISTS "core_conferencerun_o_conferencerun_id_698b5257_fk_core_conf";
ALTER TABLE IF EXISTS ONLY "public"."auth_user_user_permissions" DROP CONSTRAINT IF EXISTS "auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id";
ALTER TABLE IF EXISTS ONLY "public"."auth_user_user_permissions" DROP CONSTRAINT IF EXISTS "auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm";
ALTER TABLE IF EXISTS ONLY "public"."auth_user_groups" DROP CONSTRAINT IF EXISTS "auth_user_groups_user_id_6a12ed8b_fk_auth_user_id";
ALTER TABLE IF EXISTS ONLY "public"."auth_user_groups" DROP CONSTRAINT IF EXISTS "auth_user_groups_group_id_97559544_fk_auth_group_id";
ALTER TABLE IF EXISTS ONLY "public"."auth_permission" DROP CONSTRAINT IF EXISTS "auth_permission_content_type_id_2f476e4b_fk_django_co";
ALTER TABLE IF EXISTS ONLY "public"."auth_group_permissions" DROP CONSTRAINT IF EXISTS "auth_group_permissions_group_id_b120cbf9_fk_auth_group_id";
ALTER TABLE IF EXISTS ONLY "public"."auth_group_permissions" DROP CONSTRAINT IF EXISTS "auth_group_permissio_permission_id_84c5c92e_fk_auth_perm";
DROP INDEX IF EXISTS "public"."simplecms_pagestructure_page_id_2fcfddf5";
DROP INDEX IF EXISTS "public"."simplecms_pagestructure_fragment_id_9a355861";
DROP INDEX IF EXISTS "public"."simplecms_page_translation_of_id_9407fffc";
DROP INDEX IF EXISTS "public"."simplecms_page_template_id_1a52e8a8";
DROP INDEX IF EXISTS "public"."simplecms_page_author_id_aeea587b";
DROP INDEX IF EXISTS "public"."simplecms_mediaelement_author_id_adad4022";
DROP INDEX IF EXISTS "public"."registration_participant_profile_id_4fd75b80";
DROP INDEX IF EXISTS "public"."registration_participant_affiliation_id_604682ab";
DROP INDEX IF EXISTS "public"."django_site_domain_a2e37b91_like";
DROP INDEX IF EXISTS "public"."django_session_session_key_c0390e0f_like";
DROP INDEX IF EXISTS "public"."django_session_expire_date_a5c62663";
DROP INDEX IF EXISTS "public"."django_admin_log_user_id_c564eba6";
DROP INDEX IF EXISTS "public"."django_admin_log_content_type_id_c4bce8eb";
DROP INDEX IF EXISTS "public"."countries_plus_country_name_d2a42f1f_like";
DROP INDEX IF EXISTS "public"."countries_plus_country_iso_f0e2dfde_like";
DROP INDEX IF EXISTS "public"."countries_plus_country_iso3_668d1d09_like";
DROP INDEX IF EXISTS "public"."core_profile_affiliation_id_96e7d696";
DROP INDEX IF EXISTS "public"."core_institution_country_id_ef45100f_like";
DROP INDEX IF EXISTS "public"."core_institution_country_id_ef45100f";
DROP INDEX IF EXISTS "public"."core_conferenceseries_slug_b02a13ed_like";
DROP INDEX IF EXISTS "public"."core_conferenceseries_slug_b02a13ed";
DROP INDEX IF EXISTS "public"."core_conferencerun_slug_b7d43903_like";
DROP INDEX IF EXISTS "public"."core_conferencerun_slug_b7d43903";
DROP INDEX IF EXISTS "public"."core_conferencerun_series_id_d9f99922";
DROP INDEX IF EXISTS "public"."core_conferencerun_organizers_profile_id_ce1815ba";
DROP INDEX IF EXISTS "public"."core_conferencerun_organizers_conferencerun_id_698b5257";
DROP INDEX IF EXISTS "public"."auth_user_username_6821ab7c_like";
DROP INDEX IF EXISTS "public"."auth_user_user_permissions_user_id_a95ead1b";
DROP INDEX IF EXISTS "public"."auth_user_user_permissions_permission_id_1fbb5f2c";
DROP INDEX IF EXISTS "public"."auth_user_groups_user_id_6a12ed8b";
DROP INDEX IF EXISTS "public"."auth_user_groups_group_id_97559544";
DROP INDEX IF EXISTS "public"."auth_permission_content_type_id_2f476e4b";
DROP INDEX IF EXISTS "public"."auth_group_permissions_permission_id_84c5c92e";
DROP INDEX IF EXISTS "public"."auth_group_permissions_group_id_b120cbf9";
DROP INDEX IF EXISTS "public"."auth_group_name_a6ea08ec_like";
ALTER TABLE IF EXISTS ONLY "public"."simplecms_pagetemplate" DROP CONSTRAINT IF EXISTS "simplecms_pagetemplate_pkey";
ALTER TABLE IF EXISTS ONLY "public"."simplecms_pagestructure" DROP CONSTRAINT IF EXISTS "simplecms_pagestructure_pkey";
ALTER TABLE IF EXISTS ONLY "public"."simplecms_pagestructure" DROP CONSTRAINT IF EXISTS "simplecms_pagestructure_page_id_fragment_label_f57cc5cc_uniq";
ALTER TABLE IF EXISTS ONLY "public"."simplecms_page" DROP CONSTRAINT IF EXISTS "simplecms_page_slug_language_fc313c1d_uniq";
ALTER TABLE IF EXISTS ONLY "public"."simplecms_page" DROP CONSTRAINT IF EXISTS "simplecms_page_pkey";
ALTER TABLE IF EXISTS ONLY "public"."simplecms_menuitem" DROP CONSTRAINT IF EXISTS "simplecms_menuitem_pkey";
ALTER TABLE IF EXISTS ONLY "public"."simplecms_mediaelement" DROP CONSTRAINT IF EXISTS "simplecms_mediaelement_pkey";
ALTER TABLE IF EXISTS ONLY "public"."simplecms_contentfragment" DROP CONSTRAINT IF EXISTS "simplecms_contentfragment_pkey";
ALTER TABLE IF EXISTS ONLY "public"."registration_registrationform" DROP CONSTRAINT IF EXISTS "registration_registrationdata_pkey";
ALTER TABLE IF EXISTS ONLY "public"."registration_registrationform" DROP CONSTRAINT IF EXISTS "registration_registrationdata_conf_id_key";
ALTER TABLE IF EXISTS ONLY "public"."registration_participant" DROP CONSTRAINT IF EXISTS "registration_participant_pkey";
ALTER TABLE IF EXISTS ONLY "public"."registration_participant" DROP CONSTRAINT IF EXISTS "registration_participant_conference_id_key";
ALTER TABLE IF EXISTS ONLY "public"."django_site" DROP CONSTRAINT IF EXISTS "django_site_pkey";
ALTER TABLE IF EXISTS ONLY "public"."django_site" DROP CONSTRAINT IF EXISTS "django_site_domain_a2e37b91_uniq";
ALTER TABLE IF EXISTS ONLY "public"."django_session" DROP CONSTRAINT IF EXISTS "django_session_pkey";
ALTER TABLE IF EXISTS ONLY "public"."django_migrations" DROP CONSTRAINT IF EXISTS "django_migrations_pkey";
ALTER TABLE IF EXISTS ONLY "public"."django_content_type" DROP CONSTRAINT IF EXISTS "django_content_type_pkey";
ALTER TABLE IF EXISTS ONLY "public"."django_content_type" DROP CONSTRAINT IF EXISTS "django_content_type_app_label_model_76bd3d3b_uniq";
ALTER TABLE IF EXISTS ONLY "public"."django_admin_log" DROP CONSTRAINT IF EXISTS "django_admin_log_pkey";
ALTER TABLE IF EXISTS ONLY "public"."countries_plus_country" DROP CONSTRAINT IF EXISTS "countries_plus_country_pkey";
ALTER TABLE IF EXISTS ONLY "public"."countries_plus_country" DROP CONSTRAINT IF EXISTS "countries_plus_country_name_key";
ALTER TABLE IF EXISTS ONLY "public"."countries_plus_country" DROP CONSTRAINT IF EXISTS "countries_plus_country_iso_numeric_key";
ALTER TABLE IF EXISTS ONLY "public"."countries_plus_country" DROP CONSTRAINT IF EXISTS "countries_plus_country_iso3_key";
ALTER TABLE IF EXISTS ONLY "public"."core_profile" DROP CONSTRAINT IF EXISTS "core_profile_user_id_key";
ALTER TABLE IF EXISTS ONLY "public"."core_profile" DROP CONSTRAINT IF EXISTS "core_profile_pkey";
ALTER TABLE IF EXISTS ONLY "public"."core_institution" DROP CONSTRAINT IF EXISTS "core_institution_pkey";
ALTER TABLE IF EXISTS ONLY "public"."core_conferenceseries" DROP CONSTRAINT IF EXISTS "core_conferenceseries_pkey";
ALTER TABLE IF EXISTS ONLY "public"."core_conferencerun" DROP CONSTRAINT IF EXISTS "core_conferencerun_pkey";
ALTER TABLE IF EXISTS ONLY "public"."core_conferencerun_organizers" DROP CONSTRAINT IF EXISTS "core_conferencerun_organizers_pkey";
ALTER TABLE IF EXISTS ONLY "public"."core_conferencerun_organizers" DROP CONSTRAINT IF EXISTS "core_conferencerun_organ_conferencerun_id_profile_ccbaf9f0_uniq";
ALTER TABLE IF EXISTS ONLY "public"."auth_user" DROP CONSTRAINT IF EXISTS "auth_user_username_key";
ALTER TABLE IF EXISTS ONLY "public"."auth_user_user_permissions" DROP CONSTRAINT IF EXISTS "auth_user_user_permissions_user_id_permission_id_14a6b632_uniq";
ALTER TABLE IF EXISTS ONLY "public"."auth_user_user_permissions" DROP CONSTRAINT IF EXISTS "auth_user_user_permissions_pkey";
ALTER TABLE IF EXISTS ONLY "public"."auth_user" DROP CONSTRAINT IF EXISTS "auth_user_pkey";
ALTER TABLE IF EXISTS ONLY "public"."auth_user_groups" DROP CONSTRAINT IF EXISTS "auth_user_groups_user_id_group_id_94350c0c_uniq";
ALTER TABLE IF EXISTS ONLY "public"."auth_user_groups" DROP CONSTRAINT IF EXISTS "auth_user_groups_pkey";
ALTER TABLE IF EXISTS ONLY "public"."auth_permission" DROP CONSTRAINT IF EXISTS "auth_permission_pkey";
ALTER TABLE IF EXISTS ONLY "public"."auth_permission" DROP CONSTRAINT IF EXISTS "auth_permission_content_type_id_codename_01ab375a_uniq";
ALTER TABLE IF EXISTS ONLY "public"."auth_group" DROP CONSTRAINT IF EXISTS "auth_group_pkey";
ALTER TABLE IF EXISTS ONLY "public"."auth_group_permissions" DROP CONSTRAINT IF EXISTS "auth_group_permissions_pkey";
ALTER TABLE IF EXISTS ONLY "public"."auth_group_permissions" DROP CONSTRAINT IF EXISTS "auth_group_permissions_group_id_permission_id_0cd325b0_uniq";
ALTER TABLE IF EXISTS ONLY "public"."auth_group" DROP CONSTRAINT IF EXISTS "auth_group_name_key";
ALTER TABLE IF EXISTS "public"."simplecms_pagetemplate" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."simplecms_pagestructure" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."simplecms_page" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."simplecms_menuitem" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."simplecms_mediaelement" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."simplecms_contentfragment" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."registration_registrationform" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."registration_participant" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."django_site" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."django_migrations" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."django_content_type" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."django_admin_log" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."core_profile" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."core_institution" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."core_conferenceseries" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."core_conferencerun_organizers" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."core_conferencerun" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."auth_user_user_permissions" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."auth_user_groups" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."auth_user" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."auth_permission" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."auth_group_permissions" ALTER COLUMN "id" DROP DEFAULT;
ALTER TABLE IF EXISTS "public"."auth_group" ALTER COLUMN "id" DROP DEFAULT;
DROP SEQUENCE IF EXISTS "public"."simplecms_pagetemplate_id_seq";
DROP TABLE IF EXISTS "public"."simplecms_pagetemplate";
DROP SEQUENCE IF EXISTS "public"."simplecms_pagestructure_id_seq";
DROP TABLE IF EXISTS "public"."simplecms_pagestructure";
DROP SEQUENCE IF EXISTS "public"."simplecms_page_id_seq";
DROP TABLE IF EXISTS "public"."simplecms_page";
DROP SEQUENCE IF EXISTS "public"."simplecms_menuitem_id_seq";
DROP TABLE IF EXISTS "public"."simplecms_menuitem";
DROP SEQUENCE IF EXISTS "public"."simplecms_mediaelement_id_seq";
DROP TABLE IF EXISTS "public"."simplecms_mediaelement";
DROP SEQUENCE IF EXISTS "public"."simplecms_contentfragment_id_seq";
DROP TABLE IF EXISTS "public"."simplecms_contentfragment";
DROP SEQUENCE IF EXISTS "public"."registration_registrationdata_id_seq";
DROP TABLE IF EXISTS "public"."registration_registrationform";
DROP SEQUENCE IF EXISTS "public"."registration_participant_id_seq";
DROP TABLE IF EXISTS "public"."registration_participant";
DROP SEQUENCE IF EXISTS "public"."django_site_id_seq";
DROP TABLE IF EXISTS "public"."django_site";
DROP TABLE IF EXISTS "public"."django_session";
DROP SEQUENCE IF EXISTS "public"."django_migrations_id_seq";
DROP TABLE IF EXISTS "public"."django_migrations";
DROP SEQUENCE IF EXISTS "public"."django_content_type_id_seq";
DROP TABLE IF EXISTS "public"."django_content_type";
DROP SEQUENCE IF EXISTS "public"."django_admin_log_id_seq";
DROP TABLE IF EXISTS "public"."django_admin_log";
DROP TABLE IF EXISTS "public"."countries_plus_country";
DROP SEQUENCE IF EXISTS "public"."core_profile_id_seq";
DROP TABLE IF EXISTS "public"."core_profile";
DROP SEQUENCE IF EXISTS "public"."core_institution_id_seq";
DROP TABLE IF EXISTS "public"."core_institution";
DROP SEQUENCE IF EXISTS "public"."core_conferenceseries_id_seq";
DROP TABLE IF EXISTS "public"."core_conferenceseries";
DROP SEQUENCE IF EXISTS "public"."core_conferencerun_organizers_id_seq";
DROP TABLE IF EXISTS "public"."core_conferencerun_organizers";
DROP SEQUENCE IF EXISTS "public"."core_conferencerun_id_seq";
DROP TABLE IF EXISTS "public"."core_conferencerun";
DROP SEQUENCE IF EXISTS "public"."auth_user_user_permissions_id_seq";
DROP TABLE IF EXISTS "public"."auth_user_user_permissions";
DROP SEQUENCE IF EXISTS "public"."auth_user_id_seq";
DROP SEQUENCE IF EXISTS "public"."auth_user_groups_id_seq";
DROP TABLE IF EXISTS "public"."auth_user_groups";
DROP TABLE IF EXISTS "public"."auth_user";
DROP SEQUENCE IF EXISTS "public"."auth_permission_id_seq";
DROP TABLE IF EXISTS "public"."auth_permission";
DROP SEQUENCE IF EXISTS "public"."auth_group_permissions_id_seq";
DROP TABLE IF EXISTS "public"."auth_group_permissions";
DROP SEQUENCE IF EXISTS "public"."auth_group_id_seq";
DROP TABLE IF EXISTS "public"."auth_group";
DROP EXTENSION IF EXISTS "unaccent";
DROP EXTENSION IF EXISTS "hstore";
DROP EXTENSION IF EXISTS "plpgsql";
DROP SCHEMA IF EXISTS "public";
--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "public";


ALTER SCHEMA "public" OWNER TO "postgres";

--
-- Name: SCHEMA "public"; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA "public" IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS "plpgsql" WITH SCHEMA "pg_catalog";


--
-- Name: EXTENSION "plpgsql"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "plpgsql" IS 'PL/pgSQL procedural language';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS "hstore" WITH SCHEMA "public";


--
-- Name: EXTENSION "hstore"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "hstore" IS 'data type for storing sets of (key, value) pairs';


--
-- Name: unaccent; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS "unaccent" WITH SCHEMA "public";


--
-- Name: EXTENSION "unaccent"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "unaccent" IS 'text search dictionary that removes accents';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."auth_group" (
    "id" integer NOT NULL,
    "name" character varying(80) NOT NULL
);


ALTER TABLE "public"."auth_group" OWNER TO "root";

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."auth_group_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."auth_group_id_seq" OWNER TO "root";

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."auth_group_id_seq" OWNED BY "public"."auth_group"."id";


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."auth_group_permissions" (
    "id" integer NOT NULL,
    "group_id" integer NOT NULL,
    "permission_id" integer NOT NULL
);


ALTER TABLE "public"."auth_group_permissions" OWNER TO "root";

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."auth_group_permissions_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."auth_group_permissions_id_seq" OWNER TO "root";

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."auth_group_permissions_id_seq" OWNED BY "public"."auth_group_permissions"."id";


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."auth_permission" (
    "id" integer NOT NULL,
    "name" character varying(255) NOT NULL,
    "content_type_id" integer NOT NULL,
    "codename" character varying(100) NOT NULL
);


ALTER TABLE "public"."auth_permission" OWNER TO "root";

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."auth_permission_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."auth_permission_id_seq" OWNER TO "root";

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."auth_permission_id_seq" OWNED BY "public"."auth_permission"."id";


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."auth_user" (
    "id" integer NOT NULL,
    "password" character varying(128) NOT NULL,
    "last_login" timestamp with time zone,
    "is_superuser" boolean NOT NULL,
    "username" character varying(150) NOT NULL,
    "first_name" character varying(30) NOT NULL,
    "last_name" character varying(150) NOT NULL,
    "email" character varying(254) NOT NULL,
    "is_staff" boolean NOT NULL,
    "is_active" boolean NOT NULL,
    "date_joined" timestamp with time zone NOT NULL
);


ALTER TABLE "public"."auth_user" OWNER TO "root";

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."auth_user_groups" (
    "id" integer NOT NULL,
    "user_id" integer NOT NULL,
    "group_id" integer NOT NULL
);


ALTER TABLE "public"."auth_user_groups" OWNER TO "root";

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."auth_user_groups_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."auth_user_groups_id_seq" OWNER TO "root";

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."auth_user_groups_id_seq" OWNED BY "public"."auth_user_groups"."id";


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."auth_user_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."auth_user_id_seq" OWNER TO "root";

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."auth_user_id_seq" OWNED BY "public"."auth_user"."id";


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."auth_user_user_permissions" (
    "id" integer NOT NULL,
    "user_id" integer NOT NULL,
    "permission_id" integer NOT NULL
);


ALTER TABLE "public"."auth_user_user_permissions" OWNER TO "root";

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."auth_user_user_permissions_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."auth_user_user_permissions_id_seq" OWNER TO "root";

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."auth_user_user_permissions_id_seq" OWNED BY "public"."auth_user_user_permissions"."id";


--
-- Name: core_conferencerun; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."core_conferencerun" (
    "id" integer NOT NULL,
    "start" "date" NOT NULL,
    "end" "date" NOT NULL,
    "slug" character varying(50) NOT NULL,
    "run" character varying(500),
    "about" "text",
    "series_id" integer
);


ALTER TABLE "public"."core_conferencerun" OWNER TO "root";

--
-- Name: core_conferencerun_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."core_conferencerun_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."core_conferencerun_id_seq" OWNER TO "root";

--
-- Name: core_conferencerun_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."core_conferencerun_id_seq" OWNED BY "public"."core_conferencerun"."id";


--
-- Name: core_conferencerun_organizers; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."core_conferencerun_organizers" (
    "id" integer NOT NULL,
    "conferencerun_id" integer NOT NULL,
    "profile_id" integer NOT NULL
);


ALTER TABLE "public"."core_conferencerun_organizers" OWNER TO "root";

--
-- Name: core_conferencerun_organizers_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."core_conferencerun_organizers_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."core_conferencerun_organizers_id_seq" OWNER TO "root";

--
-- Name: core_conferencerun_organizers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."core_conferencerun_organizers_id_seq" OWNED BY "public"."core_conferencerun_organizers"."id";


--
-- Name: core_conferenceseries; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."core_conferenceseries" (
    "id" integer NOT NULL,
    "name" character varying(500) NOT NULL,
    "about" "text",
    "slug" character varying(50) NOT NULL
);


ALTER TABLE "public"."core_conferenceseries" OWNER TO "root";

--
-- Name: core_conferenceseries_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."core_conferenceseries_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."core_conferenceseries_id_seq" OWNER TO "root";

--
-- Name: core_conferenceseries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."core_conferenceseries_id_seq" OWNED BY "public"."core_conferenceseries"."id";


--
-- Name: core_institution; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."core_institution" (
    "id" integer NOT NULL,
    "name" character varying(500),
    "freeform_address" character varying(500),
    "street" character varying(200),
    "street_number" character varying(30),
    "city" character varying(200),
    "post_code" character varying(20),
    "state" character varying(200),
    "meta_data" "jsonb" NOT NULL,
    "country_id" character varying(2)
);


ALTER TABLE "public"."core_institution" OWNER TO "root";

--
-- Name: core_institution_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."core_institution_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."core_institution_id_seq" OWNER TO "root";

--
-- Name: core_institution_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."core_institution_id_seq" OWNED BY "public"."core_institution"."id";


--
-- Name: core_profile; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."core_profile" (
    "id" integer NOT NULL,
    "avatar" character varying(100),
    "web" character varying(200),
    "gender" character varying(2),
    "meta_data" "jsonb" NOT NULL,
    "affiliation_id" integer,
    "user_id" integer
);


ALTER TABLE "public"."core_profile" OWNER TO "root";

--
-- Name: core_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."core_profile_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."core_profile_id_seq" OWNER TO "root";

--
-- Name: core_profile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."core_profile_id_seq" OWNED BY "public"."core_profile"."id";


--
-- Name: countries_plus_country; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."countries_plus_country" (
    "iso" character varying(2) NOT NULL,
    "iso3" character varying(3) NOT NULL,
    "iso_numeric" integer NOT NULL,
    "fips" character varying(3),
    "name" character varying(255) NOT NULL,
    "capital" character varying(255),
    "area" numeric(11,2),
    "population" integer,
    "continent" character varying(2),
    "tld" character varying(255),
    "currency_code" character varying(3),
    "currency_symbol" character varying(255),
    "currency_name" character varying(255),
    "phone" character varying(255),
    "postal_code_format" character varying(255),
    "postal_code_regex" character varying(255),
    "languages" character varying(255),
    "geonameid" integer,
    "neighbours" character varying(255),
    "equivalent_fips_code" character varying(4)
);


ALTER TABLE "public"."countries_plus_country" OWNER TO "root";

--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."django_admin_log" (
    "id" integer NOT NULL,
    "action_time" timestamp with time zone NOT NULL,
    "object_id" "text",
    "object_repr" character varying(200) NOT NULL,
    "action_flag" smallint NOT NULL,
    "change_message" "text" NOT NULL,
    "content_type_id" integer,
    "user_id" integer NOT NULL,
    CONSTRAINT "django_admin_log_action_flag_check" CHECK (("action_flag" >= 0))
);


ALTER TABLE "public"."django_admin_log" OWNER TO "root";

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."django_admin_log_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."django_admin_log_id_seq" OWNER TO "root";

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."django_admin_log_id_seq" OWNED BY "public"."django_admin_log"."id";


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."django_content_type" (
    "id" integer NOT NULL,
    "app_label" character varying(100) NOT NULL,
    "model" character varying(100) NOT NULL
);


ALTER TABLE "public"."django_content_type" OWNER TO "root";

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."django_content_type_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."django_content_type_id_seq" OWNER TO "root";

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."django_content_type_id_seq" OWNED BY "public"."django_content_type"."id";


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."django_migrations" (
    "id" integer NOT NULL,
    "app" character varying(255) NOT NULL,
    "name" character varying(255) NOT NULL,
    "applied" timestamp with time zone NOT NULL
);


ALTER TABLE "public"."django_migrations" OWNER TO "root";

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."django_migrations_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."django_migrations_id_seq" OWNER TO "root";

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."django_migrations_id_seq" OWNED BY "public"."django_migrations"."id";


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."django_session" (
    "session_key" character varying(40) NOT NULL,
    "session_data" "text" NOT NULL,
    "expire_date" timestamp with time zone NOT NULL
);


ALTER TABLE "public"."django_session" OWNER TO "root";

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."django_site" (
    "id" integer NOT NULL,
    "domain" character varying(100) NOT NULL,
    "name" character varying(50) NOT NULL
);


ALTER TABLE "public"."django_site" OWNER TO "root";

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."django_site_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."django_site_id_seq" OWNER TO "root";

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."django_site_id_seq" OWNED BY "public"."django_site"."id";


--
-- Name: registration_participant; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."registration_participant" (
    "id" integer NOT NULL,
    "avatar" character varying(100),
    "web" character varying(200),
    "gender" character varying(2),
    "meta_data" "jsonb" NOT NULL,
    "status" character varying(20) NOT NULL,
    "registration_type" character varying(20) NOT NULL,
    "received" timestamp with time zone NOT NULL,
    "affiliation_id" integer,
    "conference_id" integer NOT NULL,
    "profile_id" integer
);


ALTER TABLE "public"."registration_participant" OWNER TO "root";

--
-- Name: registration_participant_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."registration_participant_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."registration_participant_id_seq" OWNER TO "root";

--
-- Name: registration_participant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."registration_participant_id_seq" OWNED BY "public"."registration_participant"."id";


--
-- Name: registration_registrationform; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."registration_registrationform" (
    "id" integer NOT NULL,
    "registration_form" "jsonb" NOT NULL,
    "conf_id" integer NOT NULL
);


ALTER TABLE "public"."registration_registrationform" OWNER TO "root";

--
-- Name: registration_registrationdata_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."registration_registrationdata_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."registration_registrationdata_id_seq" OWNER TO "root";

--
-- Name: registration_registrationdata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."registration_registrationdata_id_seq" OWNED BY "public"."registration_registrationform"."id";


--
-- Name: simplecms_contentfragment; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."simplecms_contentfragment" (
    "id" integer NOT NULL,
    "content" "text" NOT NULL,
    "content_format" character varying(20) NOT NULL,
    "name" character varying(255) NOT NULL
);


ALTER TABLE "public"."simplecms_contentfragment" OWNER TO "root";

--
-- Name: simplecms_contentfragment_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."simplecms_contentfragment_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."simplecms_contentfragment_id_seq" OWNER TO "root";

--
-- Name: simplecms_contentfragment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."simplecms_contentfragment_id_seq" OWNED BY "public"."simplecms_contentfragment"."id";


--
-- Name: simplecms_mediaelement; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."simplecms_mediaelement" (
    "id" integer NOT NULL,
    "title" character varying(255),
    "language" character varying(7) NOT NULL,
    "content" character varying(100) NOT NULL,
    "author_id" integer
);


ALTER TABLE "public"."simplecms_mediaelement" OWNER TO "root";

--
-- Name: simplecms_mediaelement_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."simplecms_mediaelement_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."simplecms_mediaelement_id_seq" OWNER TO "root";

--
-- Name: simplecms_mediaelement_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."simplecms_mediaelement_id_seq" OWNED BY "public"."simplecms_mediaelement"."id";


--
-- Name: simplecms_menuitem; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."simplecms_menuitem" (
    "id" integer NOT NULL,
    "name" character varying(255) NOT NULL,
    "children" "text",
    "language" character varying(7) NOT NULL,
    "site_prefix" character varying(500)
);


ALTER TABLE "public"."simplecms_menuitem" OWNER TO "root";

--
-- Name: simplecms_menuitem_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."simplecms_menuitem_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."simplecms_menuitem_id_seq" OWNER TO "root";

--
-- Name: simplecms_menuitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."simplecms_menuitem_id_seq" OWNED BY "public"."simplecms_menuitem"."id";


--
-- Name: simplecms_page; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."simplecms_page" (
    "id" integer NOT NULL,
    "title" character varying(255) NOT NULL,
    "slug" character varying(255) NOT NULL,
    "language" character varying(7) NOT NULL,
    "author_id" integer,
    "template_id" integer,
    "translation_of_id" integer,
    "site_prefix" character varying(500)
);


ALTER TABLE "public"."simplecms_page" OWNER TO "root";

--
-- Name: simplecms_page_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."simplecms_page_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."simplecms_page_id_seq" OWNER TO "root";

--
-- Name: simplecms_page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."simplecms_page_id_seq" OWNED BY "public"."simplecms_page"."id";


--
-- Name: simplecms_pagestructure; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."simplecms_pagestructure" (
    "id" integer NOT NULL,
    "fragment_label" character varying(255) NOT NULL,
    "fragment_id" integer NOT NULL,
    "page_id" integer NOT NULL
);


ALTER TABLE "public"."simplecms_pagestructure" OWNER TO "root";

--
-- Name: simplecms_pagestructure_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."simplecms_pagestructure_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."simplecms_pagestructure_id_seq" OWNER TO "root";

--
-- Name: simplecms_pagestructure_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."simplecms_pagestructure_id_seq" OWNED BY "public"."simplecms_pagestructure"."id";


--
-- Name: simplecms_pagetemplate; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "public"."simplecms_pagetemplate" (
    "id" integer NOT NULL,
    "title" character varying(255) NOT NULL,
    "src" "text" NOT NULL,
    "path" character varying(255) NOT NULL
);


ALTER TABLE "public"."simplecms_pagetemplate" OWNER TO "root";

--
-- Name: simplecms_pagetemplate_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE "public"."simplecms_pagetemplate_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "public"."simplecms_pagetemplate_id_seq" OWNER TO "root";

--
-- Name: simplecms_pagetemplate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE "public"."simplecms_pagetemplate_id_seq" OWNED BY "public"."simplecms_pagetemplate"."id";


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_group" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."auth_group_id_seq"'::"regclass");


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_group_permissions" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."auth_group_permissions_id_seq"'::"regclass");


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_permission" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."auth_permission_id_seq"'::"regclass");


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_user" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."auth_user_id_seq"'::"regclass");


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_user_groups" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."auth_user_groups_id_seq"'::"regclass");


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_user_user_permissions" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."auth_user_user_permissions_id_seq"'::"regclass");


--
-- Name: core_conferencerun id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."core_conferencerun" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."core_conferencerun_id_seq"'::"regclass");


--
-- Name: core_conferencerun_organizers id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."core_conferencerun_organizers" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."core_conferencerun_organizers_id_seq"'::"regclass");


--
-- Name: core_conferenceseries id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."core_conferenceseries" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."core_conferenceseries_id_seq"'::"regclass");


--
-- Name: core_institution id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."core_institution" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."core_institution_id_seq"'::"regclass");


--
-- Name: core_profile id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."core_profile" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."core_profile_id_seq"'::"regclass");


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."django_admin_log" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."django_admin_log_id_seq"'::"regclass");


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."django_content_type" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."django_content_type_id_seq"'::"regclass");


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."django_migrations" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."django_migrations_id_seq"'::"regclass");


--
-- Name: django_site id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."django_site" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."django_site_id_seq"'::"regclass");


--
-- Name: registration_participant id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."registration_participant" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."registration_participant_id_seq"'::"regclass");


--
-- Name: registration_registrationform id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."registration_registrationform" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."registration_registrationdata_id_seq"'::"regclass");


--
-- Name: simplecms_contentfragment id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."simplecms_contentfragment" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."simplecms_contentfragment_id_seq"'::"regclass");


--
-- Name: simplecms_mediaelement id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."simplecms_mediaelement" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."simplecms_mediaelement_id_seq"'::"regclass");


--
-- Name: simplecms_menuitem id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."simplecms_menuitem" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."simplecms_menuitem_id_seq"'::"regclass");


--
-- Name: simplecms_page id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."simplecms_page" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."simplecms_page_id_seq"'::"regclass");


--
-- Name: simplecms_pagestructure id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."simplecms_pagestructure" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."simplecms_pagestructure_id_seq"'::"regclass");


--
-- Name: simplecms_pagetemplate id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."simplecms_pagetemplate" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."simplecms_pagetemplate_id_seq"'::"regclass");


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."auth_group" ("id", "name") FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."auth_group_permissions" ("id", "group_id", "permission_id") FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."auth_permission" ("id", "name", "content_type_id", "codename") FROM stdin;
1	Can add group	1	add_group
2	Can change group	1	change_group
3	Can delete group	1	delete_group
4	Can view group	1	view_group
5	Can add user	2	add_user
6	Can change user	2	change_user
7	Can delete user	2	delete_user
8	Can view user	2	view_user
9	Can add permission	3	add_permission
10	Can change permission	3	change_permission
11	Can delete permission	3	delete_permission
12	Can view permission	3	view_permission
13	Can add content type	4	add_contenttype
14	Can change content type	4	change_contenttype
15	Can delete content type	4	delete_contenttype
16	Can view content type	4	view_contenttype
17	Can add session	5	add_session
18	Can change session	5	change_session
19	Can delete session	5	delete_session
20	Can view session	5	view_session
21	Can add log entry	6	add_logentry
22	Can change log entry	6	change_logentry
23	Can delete log entry	6	delete_logentry
24	Can view log entry	6	view_logentry
25	Can add site	7	add_site
26	Can change site	7	change_site
27	Can delete site	7	delete_site
28	Can view site	7	view_site
29	Can add Country	8	add_country
30	Can change Country	8	change_country
31	Can delete Country	8	delete_country
32	Can view Country	8	view_country
33	Can add page template	9	add_pagetemplate
34	Can change page template	9	change_pagetemplate
35	Can delete page template	9	delete_pagetemplate
36	Can view page template	9	view_pagetemplate
37	Can add page structure	10	add_pagestructure
38	Can change page structure	10	change_pagestructure
39	Can delete page structure	10	delete_pagestructure
40	Can view page structure	10	view_pagestructure
41	Can add menu item	11	add_menuitem
42	Can change menu item	11	change_menuitem
43	Can delete menu item	11	delete_menuitem
44	Can view menu item	11	view_menuitem
45	Can add media element	12	add_mediaelement
46	Can change media element	12	change_mediaelement
47	Can delete media element	12	delete_mediaelement
48	Can view media element	12	view_mediaelement
49	Can add content fragment	13	add_contentfragment
50	Can change content fragment	13	change_contentfragment
51	Can delete content fragment	13	delete_contentfragment
52	Can view content fragment	13	view_contentfragment
53	Can add page	14	add_page
54	Can change page	14	change_page
55	Can delete page	14	delete_page
56	Can view page	14	view_page
57	Can add profile	15	add_profile
58	Can change profile	15	change_profile
59	Can delete profile	15	delete_profile
60	Can view profile	15	view_profile
61	Can add institution	16	add_institution
62	Can change institution	16	change_institution
63	Can delete institution	16	delete_institution
64	Can view institution	16	view_institution
65	Can add conference series	17	add_conferenceseries
66	Can change conference series	17	change_conferenceseries
67	Can delete conference series	17	delete_conferenceseries
68	Can view conference series	17	view_conferenceseries
69	Can add conference run	18	add_conferencerun
70	Can change conference run	18	change_conferencerun
71	Can delete conference run	18	delete_conferencerun
72	Can view conference run	18	view_conferencerun
73	Can add participant	19	add_participant
74	Can change participant	19	change_participant
75	Can delete participant	19	delete_participant
76	Can view participant	19	view_participant
77	Can add registration form	20	add_registrationform
78	Can change registration form	20	change_registrationform
79	Can delete registration form	20	delete_registrationform
80	Can view registration form	20	view_registrationform
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."auth_user" ("id", "password", "last_login", "is_superuser", "username", "first_name", "last_name", "email", "is_staff", "is_active", "date_joined") FROM stdin;
3	pbkdf2_sha256$120000$EtdUBho1hGnh$/WkR0JEvFWkCj8PiRsfH11Oevy8LZo/t5uHJnSx79Xk=	2018-10-12 18:32:18.629217+00	t	admin	Admin	User	admin@localhost	t	t	2018-10-12 18:30:39+00
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."auth_user_groups" ("id", "user_id", "group_id") FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."auth_user_user_permissions" ("id", "user_id", "permission_id") FROM stdin;
\.


--
-- Data for Name: core_conferencerun; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."core_conferencerun" ("id", "start", "end", "slug", "run", "about", "series_id") FROM stdin;
\.


--
-- Data for Name: core_conferencerun_organizers; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."core_conferencerun_organizers" ("id", "conferencerun_id", "profile_id") FROM stdin;
\.


--
-- Data for Name: core_conferenceseries; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."core_conferenceseries" ("id", "name", "about", "slug") FROM stdin;
\.


--
-- Data for Name: core_institution; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."core_institution" ("id", "name", "freeform_address", "street", "street_number", "city", "post_code", "state", "meta_data", "country_id") FROM stdin;
\.


--
-- Data for Name: core_profile; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."core_profile" ("id", "avatar", "web", "gender", "meta_data", "affiliation_id", "user_id") FROM stdin;
\.


--
-- Data for Name: countries_plus_country; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."countries_plus_country" ("iso", "iso3", "iso_numeric", "fips", "name", "capital", "area", "population", "continent", "tld", "currency_code", "currency_symbol", "currency_name", "phone", "postal_code_format", "postal_code_regex", "languages", "geonameid", "neighbours", "equivalent_fips_code") FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."django_admin_log" ("id", "action_time", "object_id", "object_repr", "action_flag", "change_message", "content_type_id", "user_id") FROM stdin;
15	2018-10-12 18:32:58.779309+00	2	Home Page(en)	2	[{"changed": {"fields": ["author"]}}]	14	3
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."django_content_type" ("id", "app_label", "model") FROM stdin;
1	auth	group
2	auth	user
3	auth	permission
4	contenttypes	contenttype
5	sessions	session
6	admin	logentry
7	sites	site
8	countries_plus	country
9	simplecms	pagetemplate
10	simplecms	pagestructure
11	simplecms	menuitem
12	simplecms	mediaelement
13	simplecms	contentfragment
14	simplecms	page
15	core	profile
16	core	institution
17	core	conferenceseries
18	core	conferencerun
19	registration	participant
20	registration	registrationform
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."django_migrations" ("id", "app", "name", "applied") FROM stdin;
1	contenttypes	0001_initial	2018-10-05 15:30:46.483752+00
2	auth	0001_initial	2018-10-05 15:30:46.650212+00
3	admin	0001_initial	2018-10-05 15:30:46.703225+00
4	admin	0002_logentry_remove_auto_add	2018-10-05 15:30:46.721881+00
5	admin	0003_logentry_add_action_flag_choices	2018-10-05 15:30:46.741148+00
6	contenttypes	0002_remove_content_type_name	2018-10-05 15:30:46.773698+00
7	auth	0002_alter_permission_name_max_length	2018-10-05 15:30:46.787155+00
8	auth	0003_alter_user_email_max_length	2018-10-05 15:30:46.80827+00
9	auth	0004_alter_user_username_opts	2018-10-05 15:30:46.826632+00
10	auth	0005_alter_user_last_login_null	2018-10-05 15:30:46.848108+00
11	auth	0006_require_contenttypes_0002	2018-10-05 15:30:46.854519+00
12	auth	0007_alter_validators_add_error_messages	2018-10-05 15:30:46.872078+00
13	auth	0008_alter_user_username_max_length	2018-10-05 15:30:46.899012+00
14	auth	0009_alter_user_last_name_max_length	2018-10-05 15:30:46.918053+00
15	simplecms	0001_initial	2018-10-05 15:30:47.15924+00
16	countries_plus	0001_initial	2018-10-05 15:30:47.215143+00
17	countries_plus	0002_auto_20150120_2225	2018-10-05 15:30:47.221661+00
18	countries_plus	0003_auto_20150611_1446	2018-10-05 15:30:47.307941+00
19	countries_plus	0004_auto_20150616_1242	2018-10-05 15:30:47.371971+00
20	countries_plus	0005_auto_20160224_1804	2018-10-05 15:30:47.438204+00
21	core	0001_initial	2018-10-05 15:30:47.707752+00
22	core	0002_initial_data	2018-10-05 15:30:47.740249+00
23	registration	0001_initial	2018-10-05 15:30:47.854546+00
24	registration	0002_auto_20171128_2032	2018-10-05 15:30:47.90305+00
25	registration	0003_auto_20181005_1031	2018-10-05 15:30:47.951276+00
26	sessions	0001_initial	2018-10-05 15:30:47.985541+00
27	simplecms	0002_auto_20171125_1544	2018-10-05 15:30:48.039976+00
28	simplecms	0003_auto_20171125_1948	2018-10-05 15:30:48.071354+00
29	simplecms	0004_auto_20171125_1952	2018-10-05 15:30:48.091432+00
30	simplecms	0005_auto_20171125_2212	2018-10-05 15:30:48.108824+00
31	simplecms	0006_auto_20181005_1031	2018-10-05 15:30:48.177133+00
32	simplecms	0007_remove_menuitem_children2	2018-10-05 15:30:48.192815+00
33	sites	0001_initial	2018-10-05 15:30:48.217514+00
34	sites	0002_alter_domain_unique	2018-10-05 15:30:48.240643+00
35	simplecms	0008_auto_20181008_0959	2018-10-08 09:06:54.087434+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."django_session" ("session_key", "session_data", "expire_date") FROM stdin;
9bj5zhgybxo710k001zno8z9a4byckm0	YTBlMDM2NmVmOTQ5ZDU0ZDcxMGRlMjQ5YmI3NzYzNzQ4ZGE3ZTU5ZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYjEwZDdlNmY1MTBiYmY4NDJmNTk5NDVkNzZhYzdiNGYzMzYzODhmMCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=	2018-10-23 11:33:47.381328+00
qkc4gcve7s1b053arrf8q9sxtsk9vk90	M2UzYzBkNzMzZDk2ZDIyZjRlZDdhYjMzYTEyNGE3ZjVkMTQyNDNmYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YzA1MGJhMmNlNzljMzNjMmY0YjVmMmIyN2E2NGFmYTdiMmFiOWM1In0=	2018-10-26 14:59:35.905046+00
bep60d6pu4wd388j8rofuwgzqkkkp10e	M2UzYzBkNzMzZDk2ZDIyZjRlZDdhYjMzYTEyNGE3ZjVkMTQyNDNmYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YzA1MGJhMmNlNzljMzNjMmY0YjVmMmIyN2E2NGFmYTdiMmFiOWM1In0=	2018-10-26 17:57:12.853732+00
p1g8gl7qgfdv2lfkl19w0rzrcvwlz46w	M2UzYzBkNzMzZDk2ZDIyZjRlZDdhYjMzYTEyNGE3ZjVkMTQyNDNmYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3YzA1MGJhMmNlNzljMzNjMmY0YjVmMmIyN2E2NGFmYTdiMmFiOWM1In0=	2018-10-26 18:09:15.520364+00
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."django_site" ("id", "domain", "name") FROM stdin;
1	example.com	example.com
\.


--
-- Data for Name: registration_participant; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."registration_participant" ("id", "avatar", "web", "gender", "meta_data", "status", "registration_type", "received", "affiliation_id", "conference_id", "profile_id") FROM stdin;
\.


--
-- Data for Name: registration_registrationform; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."registration_registrationform" ("id", "registration_form", "conf_id") FROM stdin;
\.


--
-- Data for Name: simplecms_contentfragment; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."simplecms_contentfragment" ("id", "content", "content_format", "name") FROM stdin;
2	<div class="page full-height flex prague-background">\n    <h2 class='centered light-fg'>Logic Colloquium<br>\n        August 11–16, 2019<br>\n        Prague\n    </h2>\n</div>	html	home:main
3	{# Not needed right now\n{% import 'core/macros/menu.html' as menus %}\n{% set menu = load_menu('top_menu') %}\n\n <div class="row center">\n    <h5>European Summer Meeting of the Association for Symbolic Logic</h5>\n</div> \n#}\n\n<div class="row">\n              \n              <div class="col l4 hide-on-med-and-down">\n                <h5 class="white-text">Logic Colloquium</h5>\n                <p class="grey-text text-lighten-4">\n                European Summer Meeting<br/>\n                of the Association for Symbolic Logic<br/>\n                Prague, August 11&ndash;16, 2019\n                </p>\n              </div>\n              <div class="col s12 m4 l2">\n                <h5>Program<br/>Committee</h5>\n                <ul>\n                    <li><b>Lev Beklemishev (Moscow)</b></li>\n                <!--     <li>&nbsp;</li>\n                </ul>\n                <ul> -->\n                    <li>Andrew Arana (Paris)</li>\n                    <li>Agata Ciabattoni (Vienna)</li>\n                    <li>Russell Miller (New York)</li>\n                    <li>Martin Otto (Darmstadt)</li>\n                    <li>Pavel Pudlák (Prague)</li>\n                    <li>Stevo Todorčević (Toronto)</li>\n                    <li>Alex Wilkie (Manchester)</li>\n                </ul>\n              </div>\n              <div class="col s12 m4 l2">\n                <h5>Organizing<br/>Committee</h5>\n                <ul>\n                    <li><b>David Chodounský</b></li>\n                    <li><b>Jonathan Verner</b></li>\n                </ul>\n                <ul>\n                    <li>Petr Cintula</li>\n                    <li>Radek Honzík</li>\n                    <li>Jan Hubička</li>\n                    <li>Pavel Pudlák</li>\n                    <li>Jan Starý</li>\n                    <li>Neil Thapen</li>\n                </ul>\n              </div>\n              <div class='col s6 m4 l2'>\n                  <h5>Organizers <br/> &nbsp;</h5>\n                  <ul>\n                      <li>Institute of Mathematics<br/>of the Czech Academy of Sciences</li>\n                      <li>&nbsp;</li>\n                      <li>Faculty of Arts,<br/>Charles University</li>\n                      <li>&nbsp;</li>\n                      <li>Faculty of Information Technology,<br/>Czech Technical University in Prague</li>\n                  </ul>\n              </div>\n              {# No links to show\n              <div class='col s6 m4 l2'>\n                <h5>Links</h5>\n                <ul>\n                 {{ menus.render_menu(menu.children,3,'horizontal', request.path) }}\n                </ul>\n              </div>\n              #}\n              <div class="col s6 m4 l2">\n                <h5>Contact <br/> &nbsp;</h5>\n                <address class='grey-text text-lighten-3'>\n                    AMCA (conference office)<br/>\n                    Vyšehradská 320/49<br/>\n                    128 00 Praha 2<br/>\n                    Czech&nbsp;Republic<br/>\n                    <br/>\n\n                    <a href='mailto:org@lc2019.cz'>{{icon('envelope')}} &nbsp;&nbsp; org@lc2019.cz</a><br/>\n                    {{icon('phone')}} &nbsp;&nbsp; +420 221 979 351\n                </address>\n              </div>\n            </div>\n   \n          <div class="footer-copyright">\n              <div>\n                  <div>\n                      <a href='https://www.math.cas.cz'><img src='{{ asset_url('core:images:mu-white.svg') }}' class='logo'/></a>\n                      <a href='https://www.cuni.cz'><img src='{{ asset_url('core:images:uk-white.svg') }}' class='logo'/></a>\n                      <a href='https://www.fit.cvut.cz'><img src='{{ asset_url('core:images:cvut-white.svg') }}' class='logo'/> </a>\n                  </div>\n                  <div>\n                      © 2018 Jonathan Verner  \n                  </div>\n                  \n              </div>\n\n            {% if request.user.is_staff %}\n                <a class="grey-text text-lighten-4 right" href='{{ url('admin:index') }}'>\n                    {{ _("Admin") }}\n                </a>\n            {% elif not request.user.is_authenticated %}\n                <a class="grey-text text-lighten-4 right" href='{{ url('admin:login') }}'>\n                    {{ _("Admin Login") }}\n                </a>\n            {% endif %}\n          </div>	jinja2	footer
\.


--
-- Data for Name: simplecms_mediaelement; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."simplecms_mediaelement" ("id", "title", "language", "content", "author_id") FROM stdin;
\.


--
-- Data for Name: simplecms_menuitem; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."simplecms_menuitem" ("id", "name", "children", "language", "site_prefix") FROM stdin;
1	top_menu	[]	en	\N
\.


--
-- Data for Name: simplecms_page; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."simplecms_page" ("id", "title", "slug", "language", "author_id", "template_id", "translation_of_id", "site_prefix") FROM stdin;
2	Home Page	//	en	3	1	\N	\N
\.


--
-- Data for Name: simplecms_pagestructure; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."simplecms_pagestructure" ("id", "fragment_label", "fragment_id", "page_id") FROM stdin;
1	main	2	2
\.


--
-- Data for Name: simplecms_pagetemplate; Type: TABLE DATA; Schema: public; Owner: root
--

COPY "public"."simplecms_pagetemplate" ("id", "title", "src", "path") FROM stdin;
1	Simple Default Template		core/cms_default_template.html
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."auth_group_id_seq"', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."auth_group_permissions_id_seq"', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."auth_permission_id_seq"', 80, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."auth_user_groups_id_seq"', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."auth_user_id_seq"', 3, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."auth_user_user_permissions_id_seq"', 1, false);


--
-- Name: core_conferencerun_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."core_conferencerun_id_seq"', 1, false);


--
-- Name: core_conferencerun_organizers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."core_conferencerun_organizers_id_seq"', 1, false);


--
-- Name: core_conferenceseries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."core_conferenceseries_id_seq"', 1, false);


--
-- Name: core_institution_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."core_institution_id_seq"', 1, false);


--
-- Name: core_profile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."core_profile_id_seq"', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."django_admin_log_id_seq"', 15, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."django_content_type_id_seq"', 20, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."django_migrations_id_seq"', 35, true);


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."django_site_id_seq"', 1, true);


--
-- Name: registration_participant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."registration_participant_id_seq"', 1, false);


--
-- Name: registration_registrationdata_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."registration_registrationdata_id_seq"', 1, false);


--
-- Name: simplecms_contentfragment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."simplecms_contentfragment_id_seq"', 3, true);


--
-- Name: simplecms_mediaelement_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."simplecms_mediaelement_id_seq"', 1, false);


--
-- Name: simplecms_menuitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."simplecms_menuitem_id_seq"', 1, true);


--
-- Name: simplecms_page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."simplecms_page_id_seq"', 2, true);


--
-- Name: simplecms_pagestructure_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."simplecms_pagestructure_id_seq"', 1, true);


--
-- Name: simplecms_pagetemplate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('"public"."simplecms_pagetemplate_id_seq"', 1, true);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_group"
    ADD CONSTRAINT "auth_group_name_key" UNIQUE ("name");


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_group_permissions"
    ADD CONSTRAINT "auth_group_permissions_group_id_permission_id_0cd325b0_uniq" UNIQUE ("group_id", "permission_id");


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_group_permissions"
    ADD CONSTRAINT "auth_group_permissions_pkey" PRIMARY KEY ("id");


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_group"
    ADD CONSTRAINT "auth_group_pkey" PRIMARY KEY ("id");


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_permission"
    ADD CONSTRAINT "auth_permission_content_type_id_codename_01ab375a_uniq" UNIQUE ("content_type_id", "codename");


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_permission"
    ADD CONSTRAINT "auth_permission_pkey" PRIMARY KEY ("id");


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_user_groups"
    ADD CONSTRAINT "auth_user_groups_pkey" PRIMARY KEY ("id");


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_user_groups"
    ADD CONSTRAINT "auth_user_groups_user_id_group_id_94350c0c_uniq" UNIQUE ("user_id", "group_id");


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_user"
    ADD CONSTRAINT "auth_user_pkey" PRIMARY KEY ("id");


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_user_user_permissions"
    ADD CONSTRAINT "auth_user_user_permissions_pkey" PRIMARY KEY ("id");


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_user_user_permissions"
    ADD CONSTRAINT "auth_user_user_permissions_user_id_permission_id_14a6b632_uniq" UNIQUE ("user_id", "permission_id");


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_user"
    ADD CONSTRAINT "auth_user_username_key" UNIQUE ("username");


--
-- Name: core_conferencerun_organizers core_conferencerun_organ_conferencerun_id_profile_ccbaf9f0_uniq; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."core_conferencerun_organizers"
    ADD CONSTRAINT "core_conferencerun_organ_conferencerun_id_profile_ccbaf9f0_uniq" UNIQUE ("conferencerun_id", "profile_id");


--
-- Name: core_conferencerun_organizers core_conferencerun_organizers_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."core_conferencerun_organizers"
    ADD CONSTRAINT "core_conferencerun_organizers_pkey" PRIMARY KEY ("id");


--
-- Name: core_conferencerun core_conferencerun_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."core_conferencerun"
    ADD CONSTRAINT "core_conferencerun_pkey" PRIMARY KEY ("id");


--
-- Name: core_conferenceseries core_conferenceseries_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."core_conferenceseries"
    ADD CONSTRAINT "core_conferenceseries_pkey" PRIMARY KEY ("id");


--
-- Name: core_institution core_institution_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."core_institution"
    ADD CONSTRAINT "core_institution_pkey" PRIMARY KEY ("id");


--
-- Name: core_profile core_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."core_profile"
    ADD CONSTRAINT "core_profile_pkey" PRIMARY KEY ("id");


--
-- Name: core_profile core_profile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."core_profile"
    ADD CONSTRAINT "core_profile_user_id_key" UNIQUE ("user_id");


--
-- Name: countries_plus_country countries_plus_country_iso3_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."countries_plus_country"
    ADD CONSTRAINT "countries_plus_country_iso3_key" UNIQUE ("iso3");


--
-- Name: countries_plus_country countries_plus_country_iso_numeric_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."countries_plus_country"
    ADD CONSTRAINT "countries_plus_country_iso_numeric_key" UNIQUE ("iso_numeric");


--
-- Name: countries_plus_country countries_plus_country_name_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."countries_plus_country"
    ADD CONSTRAINT "countries_plus_country_name_key" UNIQUE ("name");


--
-- Name: countries_plus_country countries_plus_country_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."countries_plus_country"
    ADD CONSTRAINT "countries_plus_country_pkey" PRIMARY KEY ("iso");


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."django_admin_log"
    ADD CONSTRAINT "django_admin_log_pkey" PRIMARY KEY ("id");


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."django_content_type"
    ADD CONSTRAINT "django_content_type_app_label_model_76bd3d3b_uniq" UNIQUE ("app_label", "model");


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."django_content_type"
    ADD CONSTRAINT "django_content_type_pkey" PRIMARY KEY ("id");


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."django_migrations"
    ADD CONSTRAINT "django_migrations_pkey" PRIMARY KEY ("id");


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."django_session"
    ADD CONSTRAINT "django_session_pkey" PRIMARY KEY ("session_key");


--
-- Name: django_site django_site_domain_a2e37b91_uniq; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."django_site"
    ADD CONSTRAINT "django_site_domain_a2e37b91_uniq" UNIQUE ("domain");


--
-- Name: django_site django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."django_site"
    ADD CONSTRAINT "django_site_pkey" PRIMARY KEY ("id");


--
-- Name: registration_participant registration_participant_conference_id_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."registration_participant"
    ADD CONSTRAINT "registration_participant_conference_id_key" UNIQUE ("conference_id");


--
-- Name: registration_participant registration_participant_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."registration_participant"
    ADD CONSTRAINT "registration_participant_pkey" PRIMARY KEY ("id");


--
-- Name: registration_registrationform registration_registrationdata_conf_id_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."registration_registrationform"
    ADD CONSTRAINT "registration_registrationdata_conf_id_key" UNIQUE ("conf_id");


--
-- Name: registration_registrationform registration_registrationdata_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."registration_registrationform"
    ADD CONSTRAINT "registration_registrationdata_pkey" PRIMARY KEY ("id");


--
-- Name: simplecms_contentfragment simplecms_contentfragment_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."simplecms_contentfragment"
    ADD CONSTRAINT "simplecms_contentfragment_pkey" PRIMARY KEY ("id");


--
-- Name: simplecms_mediaelement simplecms_mediaelement_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."simplecms_mediaelement"
    ADD CONSTRAINT "simplecms_mediaelement_pkey" PRIMARY KEY ("id");


--
-- Name: simplecms_menuitem simplecms_menuitem_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."simplecms_menuitem"
    ADD CONSTRAINT "simplecms_menuitem_pkey" PRIMARY KEY ("id");


--
-- Name: simplecms_page simplecms_page_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."simplecms_page"
    ADD CONSTRAINT "simplecms_page_pkey" PRIMARY KEY ("id");


--
-- Name: simplecms_page simplecms_page_slug_language_fc313c1d_uniq; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."simplecms_page"
    ADD CONSTRAINT "simplecms_page_slug_language_fc313c1d_uniq" UNIQUE ("slug", "language");


--
-- Name: simplecms_pagestructure simplecms_pagestructure_page_id_fragment_label_f57cc5cc_uniq; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."simplecms_pagestructure"
    ADD CONSTRAINT "simplecms_pagestructure_page_id_fragment_label_f57cc5cc_uniq" UNIQUE ("page_id", "fragment_label");


--
-- Name: simplecms_pagestructure simplecms_pagestructure_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."simplecms_pagestructure"
    ADD CONSTRAINT "simplecms_pagestructure_pkey" PRIMARY KEY ("id");


--
-- Name: simplecms_pagetemplate simplecms_pagetemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."simplecms_pagetemplate"
    ADD CONSTRAINT "simplecms_pagetemplate_pkey" PRIMARY KEY ("id");


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "auth_group_name_a6ea08ec_like" ON "public"."auth_group" USING "btree" ("name" "varchar_pattern_ops");


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "auth_group_permissions_group_id_b120cbf9" ON "public"."auth_group_permissions" USING "btree" ("group_id");


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "auth_group_permissions_permission_id_84c5c92e" ON "public"."auth_group_permissions" USING "btree" ("permission_id");


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "auth_permission_content_type_id_2f476e4b" ON "public"."auth_permission" USING "btree" ("content_type_id");


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "auth_user_groups_group_id_97559544" ON "public"."auth_user_groups" USING "btree" ("group_id");


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "auth_user_groups_user_id_6a12ed8b" ON "public"."auth_user_groups" USING "btree" ("user_id");


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "auth_user_user_permissions_permission_id_1fbb5f2c" ON "public"."auth_user_user_permissions" USING "btree" ("permission_id");


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "auth_user_user_permissions_user_id_a95ead1b" ON "public"."auth_user_user_permissions" USING "btree" ("user_id");


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "auth_user_username_6821ab7c_like" ON "public"."auth_user" USING "btree" ("username" "varchar_pattern_ops");


--
-- Name: core_conferencerun_organizers_conferencerun_id_698b5257; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "core_conferencerun_organizers_conferencerun_id_698b5257" ON "public"."core_conferencerun_organizers" USING "btree" ("conferencerun_id");


--
-- Name: core_conferencerun_organizers_profile_id_ce1815ba; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "core_conferencerun_organizers_profile_id_ce1815ba" ON "public"."core_conferencerun_organizers" USING "btree" ("profile_id");


--
-- Name: core_conferencerun_series_id_d9f99922; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "core_conferencerun_series_id_d9f99922" ON "public"."core_conferencerun" USING "btree" ("series_id");


--
-- Name: core_conferencerun_slug_b7d43903; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "core_conferencerun_slug_b7d43903" ON "public"."core_conferencerun" USING "btree" ("slug");


--
-- Name: core_conferencerun_slug_b7d43903_like; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "core_conferencerun_slug_b7d43903_like" ON "public"."core_conferencerun" USING "btree" ("slug" "varchar_pattern_ops");


--
-- Name: core_conferenceseries_slug_b02a13ed; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "core_conferenceseries_slug_b02a13ed" ON "public"."core_conferenceseries" USING "btree" ("slug");


--
-- Name: core_conferenceseries_slug_b02a13ed_like; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "core_conferenceseries_slug_b02a13ed_like" ON "public"."core_conferenceseries" USING "btree" ("slug" "varchar_pattern_ops");


--
-- Name: core_institution_country_id_ef45100f; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "core_institution_country_id_ef45100f" ON "public"."core_institution" USING "btree" ("country_id");


--
-- Name: core_institution_country_id_ef45100f_like; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "core_institution_country_id_ef45100f_like" ON "public"."core_institution" USING "btree" ("country_id" "varchar_pattern_ops");


--
-- Name: core_profile_affiliation_id_96e7d696; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "core_profile_affiliation_id_96e7d696" ON "public"."core_profile" USING "btree" ("affiliation_id");


--
-- Name: countries_plus_country_iso3_668d1d09_like; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "countries_plus_country_iso3_668d1d09_like" ON "public"."countries_plus_country" USING "btree" ("iso3" "varchar_pattern_ops");


--
-- Name: countries_plus_country_iso_f0e2dfde_like; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "countries_plus_country_iso_f0e2dfde_like" ON "public"."countries_plus_country" USING "btree" ("iso" "varchar_pattern_ops");


--
-- Name: countries_plus_country_name_d2a42f1f_like; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "countries_plus_country_name_d2a42f1f_like" ON "public"."countries_plus_country" USING "btree" ("name" "varchar_pattern_ops");


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "django_admin_log_content_type_id_c4bce8eb" ON "public"."django_admin_log" USING "btree" ("content_type_id");


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "django_admin_log_user_id_c564eba6" ON "public"."django_admin_log" USING "btree" ("user_id");


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "django_session_expire_date_a5c62663" ON "public"."django_session" USING "btree" ("expire_date");


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "django_session_session_key_c0390e0f_like" ON "public"."django_session" USING "btree" ("session_key" "varchar_pattern_ops");


--
-- Name: django_site_domain_a2e37b91_like; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "django_site_domain_a2e37b91_like" ON "public"."django_site" USING "btree" ("domain" "varchar_pattern_ops");


--
-- Name: registration_participant_affiliation_id_604682ab; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "registration_participant_affiliation_id_604682ab" ON "public"."registration_participant" USING "btree" ("affiliation_id");


--
-- Name: registration_participant_profile_id_4fd75b80; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "registration_participant_profile_id_4fd75b80" ON "public"."registration_participant" USING "btree" ("profile_id");


--
-- Name: simplecms_mediaelement_author_id_adad4022; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "simplecms_mediaelement_author_id_adad4022" ON "public"."simplecms_mediaelement" USING "btree" ("author_id");


--
-- Name: simplecms_page_author_id_aeea587b; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "simplecms_page_author_id_aeea587b" ON "public"."simplecms_page" USING "btree" ("author_id");


--
-- Name: simplecms_page_template_id_1a52e8a8; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "simplecms_page_template_id_1a52e8a8" ON "public"."simplecms_page" USING "btree" ("template_id");


--
-- Name: simplecms_page_translation_of_id_9407fffc; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "simplecms_page_translation_of_id_9407fffc" ON "public"."simplecms_page" USING "btree" ("translation_of_id");


--
-- Name: simplecms_pagestructure_fragment_id_9a355861; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "simplecms_pagestructure_fragment_id_9a355861" ON "public"."simplecms_pagestructure" USING "btree" ("fragment_id");


--
-- Name: simplecms_pagestructure_page_id_2fcfddf5; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "simplecms_pagestructure_page_id_2fcfddf5" ON "public"."simplecms_pagestructure" USING "btree" ("page_id");


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_group_permissions"
    ADD CONSTRAINT "auth_group_permissio_permission_id_84c5c92e_fk_auth_perm" FOREIGN KEY ("permission_id") REFERENCES "public"."auth_permission"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_group_permissions"
    ADD CONSTRAINT "auth_group_permissions_group_id_b120cbf9_fk_auth_group_id" FOREIGN KEY ("group_id") REFERENCES "public"."auth_group"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_permission"
    ADD CONSTRAINT "auth_permission_content_type_id_2f476e4b_fk_django_co" FOREIGN KEY ("content_type_id") REFERENCES "public"."django_content_type"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_user_groups"
    ADD CONSTRAINT "auth_user_groups_group_id_97559544_fk_auth_group_id" FOREIGN KEY ("group_id") REFERENCES "public"."auth_group"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_user_groups"
    ADD CONSTRAINT "auth_user_groups_user_id_6a12ed8b_fk_auth_user_id" FOREIGN KEY ("user_id") REFERENCES "public"."auth_user"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_user_user_permissions"
    ADD CONSTRAINT "auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm" FOREIGN KEY ("permission_id") REFERENCES "public"."auth_permission"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."auth_user_user_permissions"
    ADD CONSTRAINT "auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id" FOREIGN KEY ("user_id") REFERENCES "public"."auth_user"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: core_conferencerun_organizers core_conferencerun_o_conferencerun_id_698b5257_fk_core_conf; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."core_conferencerun_organizers"
    ADD CONSTRAINT "core_conferencerun_o_conferencerun_id_698b5257_fk_core_conf" FOREIGN KEY ("conferencerun_id") REFERENCES "public"."core_conferencerun"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: core_conferencerun_organizers core_conferencerun_o_profile_id_ce1815ba_fk_core_prof; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."core_conferencerun_organizers"
    ADD CONSTRAINT "core_conferencerun_o_profile_id_ce1815ba_fk_core_prof" FOREIGN KEY ("profile_id") REFERENCES "public"."core_profile"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: core_conferencerun core_conferencerun_series_id_d9f99922_fk_core_conf; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."core_conferencerun"
    ADD CONSTRAINT "core_conferencerun_series_id_d9f99922_fk_core_conf" FOREIGN KEY ("series_id") REFERENCES "public"."core_conferenceseries"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: core_institution core_institution_country_id_ef45100f_fk_countries; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."core_institution"
    ADD CONSTRAINT "core_institution_country_id_ef45100f_fk_countries" FOREIGN KEY ("country_id") REFERENCES "public"."countries_plus_country"("iso") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: core_profile core_profile_affiliation_id_96e7d696_fk_core_institution_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."core_profile"
    ADD CONSTRAINT "core_profile_affiliation_id_96e7d696_fk_core_institution_id" FOREIGN KEY ("affiliation_id") REFERENCES "public"."core_institution"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: core_profile core_profile_user_id_bf8ada58_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."core_profile"
    ADD CONSTRAINT "core_profile_user_id_bf8ada58_fk_auth_user_id" FOREIGN KEY ("user_id") REFERENCES "public"."auth_user"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."django_admin_log"
    ADD CONSTRAINT "django_admin_log_content_type_id_c4bce8eb_fk_django_co" FOREIGN KEY ("content_type_id") REFERENCES "public"."django_content_type"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."django_admin_log"
    ADD CONSTRAINT "django_admin_log_user_id_c564eba6_fk_auth_user_id" FOREIGN KEY ("user_id") REFERENCES "public"."auth_user"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: registration_participant registration_partici_affiliation_id_604682ab_fk_core_inst; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."registration_participant"
    ADD CONSTRAINT "registration_partici_affiliation_id_604682ab_fk_core_inst" FOREIGN KEY ("affiliation_id") REFERENCES "public"."core_institution"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: registration_participant registration_partici_conference_id_64d670ca_fk_core_conf; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."registration_participant"
    ADD CONSTRAINT "registration_partici_conference_id_64d670ca_fk_core_conf" FOREIGN KEY ("conference_id") REFERENCES "public"."core_conferencerun"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: registration_participant registration_participant_profile_id_4fd75b80_fk_core_profile_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."registration_participant"
    ADD CONSTRAINT "registration_participant_profile_id_4fd75b80_fk_core_profile_id" FOREIGN KEY ("profile_id") REFERENCES "public"."core_profile"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: registration_registrationform registration_registr_conf_id_9a27a5b8_fk_core_conf; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."registration_registrationform"
    ADD CONSTRAINT "registration_registr_conf_id_9a27a5b8_fk_core_conf" FOREIGN KEY ("conf_id") REFERENCES "public"."core_conferencerun"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: simplecms_mediaelement simplecms_mediaelement_author_id_adad4022_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."simplecms_mediaelement"
    ADD CONSTRAINT "simplecms_mediaelement_author_id_adad4022_fk_auth_user_id" FOREIGN KEY ("author_id") REFERENCES "public"."auth_user"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: simplecms_page simplecms_page_author_id_aeea587b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."simplecms_page"
    ADD CONSTRAINT "simplecms_page_author_id_aeea587b_fk_auth_user_id" FOREIGN KEY ("author_id") REFERENCES "public"."auth_user"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: simplecms_page simplecms_page_template_id_1a52e8a8_fk_simplecms; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."simplecms_page"
    ADD CONSTRAINT "simplecms_page_template_id_1a52e8a8_fk_simplecms" FOREIGN KEY ("template_id") REFERENCES "public"."simplecms_pagetemplate"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: simplecms_page simplecms_page_translation_of_id_9407fffc_fk_simplecms_page_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."simplecms_page"
    ADD CONSTRAINT "simplecms_page_translation_of_id_9407fffc_fk_simplecms_page_id" FOREIGN KEY ("translation_of_id") REFERENCES "public"."simplecms_page"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: simplecms_pagestructure simplecms_pagestruct_fragment_id_9a355861_fk_simplecms; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."simplecms_pagestructure"
    ADD CONSTRAINT "simplecms_pagestruct_fragment_id_9a355861_fk_simplecms" FOREIGN KEY ("fragment_id") REFERENCES "public"."simplecms_contentfragment"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- Name: simplecms_pagestructure simplecms_pagestructure_page_id_2fcfddf5_fk_simplecms_page_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "public"."simplecms_pagestructure"
    ADD CONSTRAINT "simplecms_pagestructure_page_id_2fcfddf5_fk_simplecms_page_id" FOREIGN KEY ("page_id") REFERENCES "public"."simplecms_page"("id") DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

