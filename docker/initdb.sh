#!/bin/sh
echo "Creating $PGDATA"
mkdir -p /var/run/postgresql && chown -R postgres:postgres /var/run/postgresql && chmod 2777 /var/run/postgresql
mkdir -p /var/lib/postgresql && chown -R postgres:postgres /var/lib/postgresql
mkdir -p "$PGDATA" && chown -R postgres:postgres "$PGDATA" && chmod 777 "$PGDATA"

su postgres -c initdb
su postgres -c 'pg_ctl start'
cat $APP_HOME/initdb.sql | su postgres -c psql
