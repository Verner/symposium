Symposium
================================

A web-based system for managing conferences

.. code-block:: shell

    $ git clone https://gitlab.com/Verner/symposium

    $ cd symposium

    $ docker run -p 8081:80 -p 1984:1984 -v $PWD:/symposium/code registry.gitlab.com/verner/symposium:dev

    $ # point your browser to http://localhost:8081


Documentation
-------------

Please see `docs <https://gitlab.com/Verner/symposium>`_ for full documentation, including installation, tutorials and PDF documents.


Bugs/Requests
-------------

Please use the `GitLab issue tracker <https://gitlab.com/Verner/symposium/issues>`_ to submit bugs or request features.


Changelog
---------

Consult the :doc:`Changelog <changelog>` page for fixes and enhancements of each version.


License
-------

Copyright (c) 2017 Jonathan L. Verner <jonathan@temno.eu>


Distributed under the terms of the :doc:`MIT license <LICENSE.rst>`.
